// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto;

import static com.google.common.truth.Truth.assertThat;
import static com.google.gerrit.testing.GerritJUnit.assertThrows;
import static org.junit.Assert.assertEquals;

import com.google.common.collect.ImmutableSet;
import com.google.gson.Gson;
import java.util.Set;
import java.util.UUID;
import org.junit.Test;

public class EiffelEventTest {

  private static final EiffelEventType EVENT_TYPE = EiffelEventType.SCC;
  private static final String EVENT_VERSION = "1.0.0";
  private static final ImmutableSet<EiffelLinkType> SUPPORTED_LINKS =
      ImmutableSet.of(EiffelLinkType.BASE, EiffelLinkType.PREVIOUS_VERSION);

  @Test
  public void versionAndTypeAlwaysPopulated() {
    String serialized = new Gson().toJson(EiffelTestEventInfo.builder().build());
    String expected =
        "{\"meta\":{\"type\":\"EiffelSourceChangeCreatedEvent\",\"version\":\"1.0.0\"}}";
    assertEquals("Json matches", expected, serialized);
  }

  @Test
  public void multipleLinksDisallowedWhenNotSupported() {
    IllegalStateException thrown =
        assertThrows(
            IllegalStateException.class,
            () ->
                EiffelTestEventInfo.builder()
                    .addLink(
                        EiffelLinkInfo.builder()
                            .type(EiffelLinkType.BASE)
                            .target(UUID.fromString("aaaaaaaa-bbbb-cccc-eeee-ffffffffffff")))
                    .addLink(
                        EiffelLinkInfo.builder()
                            .type(EiffelLinkType.BASE)
                            .target(UUID.fromString("baaaaaaa-bbbb-cccc-eeee-ffffffffffff")))
                    .build());
    assertThat(thrown)
        .hasMessageThat()
        .isEqualTo(
            String.format(
                "A link of type %s already exists and multiple instances are not allowed",
                EiffelLinkType.BASE.name()));
  }

  @Test
  public void multipleLinksAllowedWhenSupported() {
    EiffelTestEventInfo info =
        EiffelTestEventInfo.builder()
            .addLink(
                EiffelLinkInfo.builder()
                    .type(EiffelLinkType.PREVIOUS_VERSION)
                    .target(UUID.fromString("aaaaaaaa-bbbb-cccc-eeee-ffffffffffff")))
            .addLink(
                EiffelLinkInfo.builder()
                    .type(EiffelLinkType.PREVIOUS_VERSION)
                    .target(UUID.fromString("baaaaaaa-bbbb-cccc-eeee-ffffffffffff")))
            .build();
    assertEquals(info.links.length, 2);
  }

  @Test
  public void unsupportedLinksDisallowed() {
    IllegalStateException thrown =
        assertThrows(
            IllegalStateException.class,
            () ->
                EiffelTestEventInfo.builder()
                    .addLink(
                        EiffelLinkInfo.builder()
                            .type(EiffelLinkType.PARTIALLY_RESOLVED_ISSUE)
                            .target(UUID.fromString("baaaaaaa-bbbb-cccc-eeee-ffffffffffff")))
                    .build());
    assertThat(thrown)
        .hasMessageThat()
        .isEqualTo(
            String.format(
                "%s not supported for event-type %s",
                EiffelLinkType.PARTIALLY_RESOLVED_ISSUE.name(), EVENT_TYPE.getType()));
  }

  static class EiffelTestEventInfo extends EiffelEvent {

    static Builder builder() {
      return new Builder();
    }

    protected EiffelTestEventInfo(EiffelMetaInfo meta, EiffelLinkInfo[] links) {
      super(meta, links);
    }

    static class Builder extends EiffelEvent.Builder<EiffelTestEventInfo, Builder> {

      @Override
      protected String getEventVersion() {
        return EVENT_VERSION;
      }

      @Override
      protected EiffelEventType getEventType() {
        return EVENT_TYPE;
      }

      @Override
      protected Set<EiffelLinkType> supportedLinks() {
        return SUPPORTED_LINKS;
      }

      @Override
      public EiffelTestEventInfo build() {
        return new EiffelTestEventInfo(buildMeta(), buildLinks());
      }
    }
  }
}
