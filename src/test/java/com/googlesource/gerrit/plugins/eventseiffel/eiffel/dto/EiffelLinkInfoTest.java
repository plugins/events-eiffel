// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto;

import static com.google.common.truth.Truth.assertThat;
import static com.google.gerrit.testing.GerritJUnit.assertThrows;
import static org.junit.Assert.assertEquals;

import com.google.gson.Gson;
import java.util.UUID;
import org.junit.Test;

public class EiffelLinkInfoTest {

  @Test
  public void isSerializable() {
    String serialized =
        new Gson()
            .toJson(
                EiffelLinkInfo.builder()
                    .target(UUID.fromString("aaaaaaaa-bbbb-cccc-eeee-ffffffffffff"))
                    .type(EiffelLinkType.BASE)
                    .build());
    String expected = "{\"type\":\"BASE\",\"target\":\"aaaaaaaa-bbbb-cccc-eeee-ffffffffffff\"}";
    assertEquals("Matches json", expected, serialized);
  }

  @Test
  public void isDeSerializable() {
    String json = "{\"type\":\"BASE\",\"target\":\"aaaaaaaa-bbbb-cccc-eeee-ffffffffffff\"}";
    EiffelLinkInfo parsed = new Gson().fromJson(json, EiffelLinkInfo.class);
    assertEquals(UUID.fromString("aaaaaaaa-bbbb-cccc-eeee-ffffffffffff"), parsed.target);
    assertEquals(EiffelLinkType.BASE, parsed.type);
  }

  @Test
  public void linkTargetIsRequired() {
    IllegalStateException thrown =
        assertThrows(
            IllegalStateException.class,
            () -> EiffelLinkInfo.builder().type(EiffelLinkType.BASE).build());
    assertThat(thrown).hasMessageThat().isEqualTo("target is required");
  }

  @Test
  public void linkTypeIsRequired() {
    IllegalStateException thrown =
        assertThrows(
            IllegalStateException.class,
            () ->
                EiffelLinkInfo.builder()
                    .target(UUID.fromString("aaaaaaaa-bbbb-cccc-eeee-ffffffffffff"))
                    .build());
    assertThat(thrown).hasMessageThat().isEqualTo("type is required");
  }
}
