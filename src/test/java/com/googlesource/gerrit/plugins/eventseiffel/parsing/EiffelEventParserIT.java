// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.parsing;

import static com.google.common.truth.Truth.assertThat;
import static com.google.gerrit.testing.GerritJUnit.assertThrows;
import static com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType.SCC;
import static com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType.SCS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import com.google.common.collect.ImmutableList;
import com.google.gerrit.acceptance.PushOneCommit;
import com.google.gerrit.acceptance.TestPlugin;
import com.google.gerrit.acceptance.UseLocalDisk;
import com.google.gerrit.acceptance.config.GlobalPluginConfig;
import com.google.gerrit.entities.BranchNameKey;
import com.google.gerrit.extensions.common.AccountInfo;
import com.googlesource.gerrit.plugins.eventseiffel.TestEventHub;
import com.googlesource.gerrit.plugins.eventseiffel.TestEventStorage;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.ArtifactEventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.CompositionDefinedEventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.SourceChangeEventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEvent;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelSourceChangeSubmittedEventInfo;
import java.time.Instant;
import java.util.Arrays;
import java.util.UUID;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.revwalk.RevCommit;
import org.junit.Before;
import org.junit.Test;

@TestPlugin(
    name = "events-eiffel",
    sysModule = "com.googlesource.gerrit.plugins.eventseiffel.parsing.ParsingTestModule")
public class EiffelEventParserIT extends EiffelEventParsingTest {

  private EiffelEventParserImpl eventParser;

  @Before
  public void setUp() {
    eventParser = plugin.getSysInjector().getInstance(EiffelEventParserImpl.class);
    TestEventHub.EVENTS.clear();
    TestEventsFilterProvider.reset();
  }

  @Test
  public void sccQueuedTogetherWithParent() throws Exception {
    PushOneCommit.Result res = createChange();
    SourceChangeEventKey scc = toSccKey(res);
    eventParser.createAndScheduleMissingSccs(scc);
    assertEquals(2, TestEventHub.EVENTS.size());
    assertCorrectEvent(0, scc.copy(res.getCommit().getParent(0)));
    assertCorrectEvent(1, scc);
  }

  @Test
  public void sccQueuedButNotForHandledParent() throws Exception {
    PushOneCommit.Result res = createChange();
    SourceChangeEventKey scc = toSccKey(res);
    RevCommit parent = res.getCommit().getParent(0);
    markAsHandled(scc.copy(parent), parent);
    eventParser.createAndScheduleMissingSccs(scc);
    assertEquals(1, TestEventHub.EVENTS.size());
    assertCorrectEvent(0, scc);
  }

  @Test
  public void sccAndScsQueuedTogetherWithParents() throws Exception {
    PushOneCommit.Result res = createChange();
    SourceChangeEventKey scs = toScsKey(res);
    SourceChangeEventKey scc = scs.copy(SCC);
    eventParser.createAndScheduleMissingScss(scs, null, null, null);

    assertEquals(4, TestEventHub.EVENTS.size());
    assertCorrectEvent(0, scc.copy(res.getCommit().getParent(0)));
    assertCorrectEvent(1, scc);
    assertCorrectEvent(2, scs.copy(res.getCommit().getParent(0)));
    assertCorrectEvent(3, scs);
  }

  @Test
  public void scsQueuedTogetherWithParentsButNotHandledSccs() throws Exception {
    PushOneCommit.Result res = createChange();
    SourceChangeEventKey scs = toScsKey(res);
    markAsHandled(scs.copy(SCC), res.getCommit());

    eventParser.createAndScheduleMissingScss(scs, null, null, null);
    assertEquals(2, TestEventHub.EVENTS.size());
    assertCorrectEvent(0, scs.copy(res.getCommit().getParent(0)));
    assertCorrectEvent(1, scs);
  }

  @Test
  public void scsQueuedButNotHandledParentsAndSccs() throws Exception {
    PushOneCommit.Result res = createChange();
    SourceChangeEventKey scs = toScsKey(res);
    markAsHandled(scs, res.getCommit().getParent(0));
    markAsHandled(scs.copy(SCC), res.getCommit());

    eventParser.createAndScheduleMissingScss(scs, null, null, null);
    assertEquals(1, TestEventHub.EVENTS.size());
    assertCorrectEvent(0, scs);
  }

  @Test
  public void scsQueuedWithSubmitterAndSubmittedAt() throws Exception {
    RevCommit hasEvents = pushTo("refs/heads/master").getCommit();
    RevCommit headBeforeMerge = pushTo("refs/heads/master").getCommit();
    RevCommit mergedCommit = pushTo("refs/heads/master").getCommit();
    AccountInfo submitter = gApi.accounts().id(user.username()).get();
    long submittedAt = Instant.now().toEpochMilli();

    SourceChangeEventKey scs = SourceChangeEventKey.scsKey(project.get(), "master", hasEvents);
    SourceChangeEventKey scc = scs.copy(SCC);
    markAsHandled(scs, hasEvents);

    eventParser.createAndScheduleMissingScss(
        scs.copy(mergedCommit), headBeforeMerge.getName(), submitter, submittedAt);

    assertEquals(4, TestEventHub.EVENTS.size());
    assertCorrectEvent(0, scc.copy(headBeforeMerge));
    assertCorrectEvent(1, scc.copy(mergedCommit));

    assertCorrectEvent(2, scs.copy(headBeforeMerge));
    EiffelSourceChangeSubmittedEventInfo notInSubmitTransaction =
        (EiffelSourceChangeSubmittedEventInfo) TestEventHub.EVENTS.get(2);
    assertEquals(admin.username(), notInSubmitTransaction.data.submitter.id);
    assertEquals(commitTimeInEpochMillis(headBeforeMerge), notInSubmitTransaction.meta.time);

    assertCorrectEvent(3, scs.copy(mergedCommit));
    EiffelSourceChangeSubmittedEventInfo inSubmitTransaction =
        (EiffelSourceChangeSubmittedEventInfo) TestEventHub.EVENTS.get(3);
    assertEquals(submitter.username, inSubmitTransaction.data.submitter.id);
    assertEquals(Long.valueOf(submittedAt), inSubmitTransaction.meta.time);
  }

  @Test
  public void eventRepositoryCalledOnceForUnhandledBranch() throws Exception {
    String initialCommit = getHeadRevision();
    RevCommit middle = pushTo(getHead()).getCommit();
    RevCommit tip = pushTo(getHead()).getCommit();
    SourceChangeEventKey scs =
        SourceChangeEventKey.create(project.get(), getHead(), tip.getName(), SCS);
    eventParser.createAndScheduleMissingScssFromBranch(project.get(), getHead());
    assertNbrQueriesFor(scs.copy(initialCommit), 1);
    assertNbrQueriesFor(scs.copy(middle), 0);
    assertNbrQueriesFor(scs, 0);
  }

  @Test
  public void eventRepositoryCalledForAllCommitsWhenBranchHasEvents() throws Exception {
    String initialCommit = getHeadRevision();
    SourceChangeEventKey initial =
        SourceChangeEventKey.create(project.get(), getHead(), initialCommit, SCS);
    eventParser.createAndScheduleMissingScssFromBranch(project.get(), getHead());
    assertNbrQueriesFor(initial, 1);

    RevCommit middle = pushTo(getHead()).getCommit();
    RevCommit tip = pushTo(getHead()).getCommit();
    SourceChangeEventKey scs =
        SourceChangeEventKey.create(project.get(), getHead(), tip.getName(), SCS);
    eventParser.createAndScheduleMissingScssFromBranch(project.get(), getHead());
    assertNbrQueriesFor(scs.copy(middle), 1);
    assertNbrQueriesFor(scs, 1);
  }

  @Test
  public void eventIdIsNotCheckedForParentsOfHandledCommit() throws Exception {
    PushOneCommit.Result one = createChange();
    PushOneCommit.Result two = createChange();
    PushOneCommit.Result three = createChange();
    PushOneCommit.Result four = createChange();
    SourceChangeEventKey oneKey = toSccKey(one);
    SourceChangeEventKey twoKey = toSccKey(two);
    SourceChangeEventKey threeKey = toSccKey(three);
    SourceChangeEventKey fourKey = toSccKey(four);
    markAsHandled(threeKey, three.getCommit());
    eventParser.createAndScheduleMissingSccs(fourKey);
    assertNbrQueriesFor(oneKey, 0);
    assertNbrQueriesFor(twoKey, 0);
    assertNbrQueriesFor(threeKey, 1);
    assertNbrQueriesFor(fourKey, 1);
  }

  @Test
  public void artcNotRepublishedWithoutForce() throws Exception {
    String ref = setCdHandled();
    ArtifactEventKey artc = ArtifactEventKey.create(tagPURL(project.get(), ref));
    markAsHandled(artc);
    eventParser.createAndScheduleArtc(project.get(), ref, EPOCH_MILLIS, false);
    assertEquals(0, TestEventHub.EVENTS.size());
  }

  @Test
  public void artcRepublishedWithForce() throws Exception {
    setScsHandled();
    String ref =
        createTagRef(getHead(repo(), "HEAD").getName(), true).substring(Constants.R_TAGS.length());
    CompositionDefinedEventKey cd =
        CompositionDefinedEventKey.create(tagCompositionName(project.get()), ref);
    markAsHandled(cd);
    ArtifactEventKey artc = ArtifactEventKey.create(tagPURL(project.get(), ref));
    markAsHandled(artc);
    eventParser.createAndScheduleArtc(project.get(), ref, EPOCH_MILLIS, true);
    assertEquals(2, TestEventHub.EVENTS.size());
    assertCorrectEvent(0, cd);
    assertCorrectEvent(1, artc);
  }

  private String setCdHandled() throws Exception {
    setScsHandled();
    String ref =
        createTagRef(getHead(repo(), "HEAD").getName(), true).substring(Constants.R_TAGS.length());
    CompositionDefinedEventKey cd =
        CompositionDefinedEventKey.create(tagCompositionName(project.get()), ref);
    markAsHandled(cd);
    return ref;
  }

  @Test
  public void artcQueued() throws Exception {
    String ref =
        createTagRef(getHead(repo(), "HEAD").getName(), true).substring(Constants.R_TAGS.length());

    SourceChangeEventKey scc =
        SourceChangeEventKey.sccKey(project.get(), getHead(), getHeadRevision());
    SourceChangeEventKey scs = scc.copy(SCS);
    ArtifactEventKey artc = ArtifactEventKey.create(tagPURL(project.get(), ref, "localhost"));
    CompositionDefinedEventKey cd =
        CompositionDefinedEventKey.create(tagCompositionName(project.get(), "localhost"), ref);

    eventParser.createAndScheduleArtc(project.get(), ref, EPOCH_MILLIS, false);
    assertEquals(4, TestEventHub.EVENTS.size());
    assertCorrectEvent(0, scc);
    assertCorrectEvent(1, scs);
    assertCorrectEvent(2, cd);
    assertCorrectEvent(3, artc);
  }

  @Test
  public void artcQueuedBlockBranch() throws Exception {
    TestEventsFilterProvider.setBlockedRefPatterns("refs/heads/master");
    String ref =
        createTagRef(getHead(repo(), "HEAD").getName(), true).substring(Constants.R_TAGS.length());
    EventParsingException thrown =
        assertThrows(
            EventParsingException.class,
            () -> eventParser.createAndScheduleArtc(project.get(), ref, EPOCH_MILLIS, false));
    assertThat(thrown).hasMessageThat().startsWith("Could not find any unblocked branch for SCS");
  }

  @Test
  public void artcQueuedBlockBranchButReachableFromOtherBranch() throws Exception {
    TestEventsFilterProvider.setBlockedRefPatterns("refs/heads/master");
    createBranch(BranchNameKey.create(project, "other-branch"));
    String ref =
        createTagRef(getHead(repo(), "HEAD").getName(), true).substring(Constants.R_TAGS.length());

    SourceChangeEventKey scc =
        SourceChangeEventKey.sccKey(project.get(), "other-branch", getHeadRevision());
    SourceChangeEventKey scs = scc.copy(SCS);
    ArtifactEventKey artc = ArtifactEventKey.create(tagPURL(project.get(), ref, "localhost"));
    CompositionDefinedEventKey cd =
        CompositionDefinedEventKey.create(tagCompositionName(project.get(), "localhost"), ref);

    eventParser.createAndScheduleArtc(project.get(), ref, EPOCH_MILLIS, false);
    assertEquals(4, TestEventHub.EVENTS.size());
    assertCorrectEvent(0, scc);
    assertCorrectEvent(1, scs);
    assertCorrectEvent(2, cd);
    assertCorrectEvent(3, artc);
  }

  @Test
  public void artcQueuedNoTagCreated() throws Exception {
    EventParsingException thrown =
        assertThrows(
            EventParsingException.class,
            () -> eventParser.createAndScheduleArtc(project.get(), TAG_NAME, EPOCH_MILLIS, false));
    assertThat(thrown).hasMessageThat().startsWith("Cannot find tag");
  }

  @Test
  public void annotatedTagArtcQueuedScsHandled() throws Exception {
    assertArtcQueuedScsHandled(true);
  }

  @Test
  public void LightweightTagArtcQueuedScsHandled() throws Exception {
    assertArtcQueuedScsHandled(false);
  }

  @Test
  public void annotatedTagArtcQueuedscsHandledLightWeightBlocked() throws Exception {
    TestEventsFilterProvider.blockLightWeightTags();
    assertArtcQueuedScsHandled(true);
  }

  @Test
  public void lightWeightTagArtcQueuedscsHandledLightWeightBlocked() throws Exception {
    setScsHandled();
    TestEventsFilterProvider.blockLightWeightTags();
    String ref =
        createTagRef(getHead(repo(), "HEAD").getName(), false).substring(Constants.R_TAGS.length());

    eventParser.createAndScheduleArtc(project.get(), ref, EPOCH_MILLIS, false);
    assertEquals(0, TestEventHub.EVENTS.size());
  }

  @Test
  public void artcQueuedCdHandled() throws Exception {
    String ref = setCdHandled();
    eventParser.createAndScheduleArtc(project.get(), ref, EPOCH_MILLIS, false);
    assertEquals(0, TestEventHub.EVENTS.size());
  }

  @Test
  @UseLocalDisk
  @GlobalPluginConfig(pluginName = PLUGIN_NAME, name = "EiffelEvent.namespace", value = NAMESPACE)
  public void artcQueuedscsHandledWithCustomNameSpace() throws Exception {
    assertArtcQueuedScsHandled(false, NAMESPACE);
  }

  @Test
  public void missingSucceedingSccs() throws Exception {
    RevCommit initial = getHead(repo(), "HEAD");
    RevCommit middle1 = pushTo(getHead()).getCommit();
    RevCommit middle2 = pushTo(getHead()).getCommit();
    RevCommit tip = pushTo(getHead()).getCommit();

    SourceChangeEventKey scsInitial = SourceChangeEventKey.scsKey(project.get(), "master", initial);
    SourceChangeEventKey sccInitial = scsInitial.copy(SCC);
    SourceChangeEventKey scsMiddle1 = SourceChangeEventKey.scsKey(project.get(), "master", middle1);
    SourceChangeEventKey sccMiddle1 = scsMiddle1.copy(SCC);
    SourceChangeEventKey scsMiddle2 = SourceChangeEventKey.scsKey(project.get(), "master", middle2);
    SourceChangeEventKey sccMiddle2 = scsMiddle2.copy(SCC);
    SourceChangeEventKey scsTip = SourceChangeEventKey.scsKey(project.get(), "master", tip);
    SourceChangeEventKey sccTip = scsTip.copy(SCC);
    eventParser.createAndScheduleMissingScssFromBranch(project.get(), getHead());

    assertEquals(8, TestEventHub.EVENTS.size());
    assertCorrectEvent(0, sccInitial);
    assertCorrectEvent(1, sccMiddle1);
    assertCorrectEvent(2, sccMiddle2);
    assertCorrectEvent(3, sccTip);
    assertCorrectEvent(4, scsInitial);
    assertCorrectEvent(5, scsMiddle1);
    assertCorrectEvent(6, scsMiddle2);
    assertCorrectEvent(7, scsTip);

    EiffelEvent eventSccTip = TestEventHub.EVENTS.remove(3);
    EiffelEvent eventSccMiddle2 = TestEventHub.EVENTS.remove(2);
    EiffelEvent eventSccMiddle1 = TestEventHub.EVENTS.remove(1);
    EiffelEvent eventSccInitial = TestEventHub.EVENTS.remove(0);

    eventParser.fillGapsForSccFromCommit(project.get(), getHead(), tip.getName());

    assertEquals(8, TestEventHub.EVENTS.size());
    assertCorrectEvent(0, scsInitial);
    assertCorrectEvent(1, scsMiddle1);
    assertCorrectEvent(2, scsMiddle2);
    assertCorrectEvent(3, scsTip);
    assertCorrectEvent(4, sccInitial);
    assertCorrectEvent(5, sccMiddle1);
    assertCorrectEvent(6, sccMiddle2);
    assertCorrectEvent(7, sccTip);
    assertEquals(eventSccInitial.meta.id, TestEventHub.EVENTS.get(4).meta.id);
    assertEquals(eventSccMiddle1.meta.id, TestEventHub.EVENTS.get(5).meta.id);
    assertEquals(eventSccMiddle2.meta.id, TestEventHub.EVENTS.get(6).meta.id);
    assertEquals(eventSccTip.meta.id, TestEventHub.EVENTS.get(7).meta.id);
    assertParentReferences(TestEventHub.EVENTS.get(5), TestEventHub.EVENTS.get(4));
    assertParentReferences(TestEventHub.EVENTS.get(6), TestEventHub.EVENTS.get(5));
    assertParentReferences(TestEventHub.EVENTS.get(7), TestEventHub.EVENTS.get(6));
  }

  @Test
  public void missingSccWithNoScs() throws Exception {
    RevCommit initial = getHead(repo(), "HEAD");
    RevCommit middle = pushTo(getHead()).getCommit();
    RevCommit tip = pushTo(getHead()).getCommit();

    SourceChangeEventKey scsInitial = SourceChangeEventKey.scsKey(project.get(), "master", initial);
    SourceChangeEventKey sccInitial = scsInitial.copy(SCC);
    SourceChangeEventKey scsMiddle = SourceChangeEventKey.scsKey(project.get(), "master", middle);
    SourceChangeEventKey sccMiddle = scsMiddle.copy(SCC);
    SourceChangeEventKey scsTip = SourceChangeEventKey.scsKey(project.get(), "master", tip);
    SourceChangeEventKey sccTip = scsTip.copy(SCC);
    eventParser.createAndScheduleMissingScssFromBranch(project.get(), getHead());

    assertEquals(6, TestEventHub.EVENTS.size());
    assertCorrectEvent(0, sccInitial);
    assertCorrectEvent(1, sccMiddle);
    assertCorrectEvent(2, sccTip);
    assertCorrectEvent(3, scsInitial);
    assertCorrectEvent(4, scsMiddle);
    assertCorrectEvent(5, scsTip);

    TestEventHub.EVENTS.remove(3);
    EiffelEvent eventSccMiddle = TestEventHub.EVENTS.remove(1);
    EiffelEvent eventSccInitial = TestEventHub.EVENTS.remove(0);

    eventParser.fillGapsForSccFromCommit(project.get(), getHead(), tip.getName());

    assertEquals(5, TestEventHub.EVENTS.size());
    assertCorrectEvent(0, sccTip);
    assertCorrectEvent(1, scsMiddle);
    assertCorrectEvent(2, scsTip);
    assertCorrectEvent(3, sccInitial);
    assertCorrectEvent(4, sccMiddle);

    assertNotEquals(eventSccInitial.meta.id, TestEventHub.EVENTS.get(3).meta.id);
    assertEquals(eventSccMiddle.meta.id, TestEventHub.EVENTS.get(4).meta.id);
    assertParentReferences(TestEventHub.EVENTS.get(4), TestEventHub.EVENTS.get(3));
    assertParentReferences(TestEventHub.EVENTS.get(0), TestEventHub.EVENTS.get(4));
  }

  @Test
  public void missingAllSccs() throws Exception {
    RevCommit initial = getHead(repo(), "HEAD");
    RevCommit middle = pushTo(getHead()).getCommit();
    RevCommit tip = pushTo(getHead()).getCommit();

    SourceChangeEventKey sccInitial = SourceChangeEventKey.sccKey(project.get(), "master", initial);
    SourceChangeEventKey sccMiddle = SourceChangeEventKey.sccKey(project.get(), "master", middle);
    SourceChangeEventKey sccTip = SourceChangeEventKey.sccKey(project.get(), "master", tip);

    eventParser.fillGapsForSccFromCommit(project.get(), getHead(), tip.getName());

    assertEquals(3, TestEventHub.EVENTS.size());
    assertCorrectEvent(0, sccInitial);
    assertCorrectEvent(1, sccMiddle);
    assertCorrectEvent(2, sccTip);
    assertParentReferences(TestEventHub.EVENTS.get(1), TestEventHub.EVENTS.get(0));
    assertParentReferences(TestEventHub.EVENTS.get(2), TestEventHub.EVENTS.get(1));
  }

  @Test
  public void missingSucceedingScss() throws Exception {
    RevCommit initial = getHead(repo(), "HEAD");
    RevCommit middle1 = pushTo(getHead()).getCommit();
    RevCommit middle2 = pushTo(getHead()).getCommit();
    RevCommit tip = pushTo(getHead()).getCommit();

    SourceChangeEventKey scsInitial = SourceChangeEventKey.scsKey(project.get(), "master", initial);
    SourceChangeEventKey sccInitial = scsInitial.copy(SCC);
    SourceChangeEventKey scsMiddle1 = SourceChangeEventKey.scsKey(project.get(), "master", middle1);
    SourceChangeEventKey sccMiddle1 = scsMiddle1.copy(SCC);
    SourceChangeEventKey scsMiddle2 = SourceChangeEventKey.scsKey(project.get(), "master", middle2);
    SourceChangeEventKey sccMiddle2 = scsMiddle2.copy(SCC);
    SourceChangeEventKey scsTip = SourceChangeEventKey.scsKey(project.get(), "master", tip);
    SourceChangeEventKey sccTip = scsTip.copy(SCC);
    eventParser.createAndScheduleMissingScssFromBranch(project.get(), getHead());

    assertEquals(8, TestEventHub.EVENTS.size());
    assertCorrectEvent(0, sccInitial);
    assertCorrectEvent(1, sccMiddle1);
    assertCorrectEvent(2, sccMiddle2);
    assertCorrectEvent(3, sccTip);
    assertCorrectEvent(4, scsInitial);
    assertCorrectEvent(5, scsMiddle1);
    assertCorrectEvent(6, scsMiddle2);
    assertCorrectEvent(7, scsTip);

    EiffelEvent eventScsTip = TestEventHub.EVENTS.remove(7);
    EiffelEvent eventScsMiddle2 = TestEventHub.EVENTS.remove(6);
    EiffelEvent eventScsMiddle1 = TestEventHub.EVENTS.remove(5);
    EiffelEvent eventScsInitial = TestEventHub.EVENTS.remove(4);

    eventParser.fillGapsForScsFromBranch(project.get(), getHead());

    assertEquals(8, TestEventHub.EVENTS.size());
    assertCorrectEvent(0, sccInitial);
    assertCorrectEvent(1, sccMiddle1);
    assertCorrectEvent(2, sccMiddle2);
    assertCorrectEvent(3, sccTip);
    assertCorrectEvent(4, scsInitial);
    assertCorrectEvent(5, scsMiddle1);
    assertCorrectEvent(6, scsMiddle2);
    assertCorrectEvent(7, scsTip);
    assertNotEquals(eventScsInitial.meta.id, TestEventHub.EVENTS.get(4).meta.id);
    assertNotEquals(eventScsMiddle1.meta.id, TestEventHub.EVENTS.get(5).meta.id);
    assertNotEquals(eventScsMiddle2.meta.id, TestEventHub.EVENTS.get(6).meta.id);
    assertNotEquals(eventScsTip.meta.id, TestEventHub.EVENTS.get(7).meta.id);
    assertParentReferences(TestEventHub.EVENTS.get(5), TestEventHub.EVENTS.get(4));
    assertParentReferences(TestEventHub.EVENTS.get(6), TestEventHub.EVENTS.get(5));
    assertParentReferences(TestEventHub.EVENTS.get(7), TestEventHub.EVENTS.get(6));
  }

  @Test
  public void missingScsWithMergeCommit() throws Exception {
    RevCommit initial = getHead(repo(), "HEAD");
    RevCommit middle1 = pushTo(getHead()).getCommit();
    // reset HEAD in order to create a sibling of the first change
    testRepo.reset(initial);
    RevCommit middle2 = pushTo(getHead()).getCommit();

    PushOneCommit m = pushFactory.create(admin.newIdent(), testRepo);
    m.setParents(ImmutableList.of(middle1, middle2));
    PushOneCommit.Result result = m.to(getHead());
    result.assertOkStatus();
    RevCommit tip = result.getCommit();

    SourceChangeEventKey scsInitial = SourceChangeEventKey.scsKey(project.get(), "master", initial);
    SourceChangeEventKey sccInitial = scsInitial.copy(SCC);
    SourceChangeEventKey scsMiddle1 = SourceChangeEventKey.scsKey(project.get(), "master", middle1);
    SourceChangeEventKey sccMiddle1 = scsMiddle1.copy(SCC);
    SourceChangeEventKey scsMiddle2 = SourceChangeEventKey.scsKey(project.get(), "master", middle2);
    SourceChangeEventKey sccMiddle2 = scsMiddle2.copy(SCC);
    SourceChangeEventKey scsTip = SourceChangeEventKey.scsKey(project.get(), "master", tip);
    SourceChangeEventKey sccTip = scsTip.copy(SCC);
    eventParser.createAndScheduleMissingScssFromBranch(project.get(), getHead());

    assertEquals(8, TestEventHub.EVENTS.size());
    assertCorrectEvent(0, sccInitial);
    assertCorrectEvent(1, sccMiddle1);
    assertCorrectEvent(2, sccMiddle2);
    assertCorrectEvent(3, sccTip);
    assertCorrectEvent(4, scsInitial);
    assertCorrectEvent(5, scsMiddle1);
    assertCorrectEvent(6, scsMiddle2);
    assertCorrectEvent(7, scsTip);

    EiffelEvent eventScsTip = TestEventHub.EVENTS.remove(7);
    EiffelEvent eventScsMiddle2 = TestEventHub.EVENTS.remove(6);
    EiffelEvent eventScsMiddle1 = TestEventHub.EVENTS.remove(5);
    EiffelEvent eventScsInitial = TestEventHub.EVENTS.remove(4);

    eventParser.fillGapsForScsFromBranch(project.get(), getHead());

    assertEquals(8, TestEventHub.EVENTS.size());
    assertCorrectEvent(0, sccInitial);
    assertCorrectEvent(1, sccMiddle1);
    assertCorrectEvent(2, sccMiddle2);
    assertCorrectEvent(3, sccTip);
    assertCorrectEvent(4, scsInitial);
    assertCorrectEvent(5, scsMiddle1);
    assertCorrectEvent(6, scsMiddle2);
    assertCorrectEvent(7, scsTip);
    assertNotEquals(eventScsInitial.meta.id, TestEventHub.EVENTS.get(4).meta.id);
    assertNotEquals(eventScsMiddle1.meta.id, TestEventHub.EVENTS.get(5).meta.id);
    assertNotEquals(eventScsMiddle2.meta.id, TestEventHub.EVENTS.get(6).meta.id);
    assertNotEquals(eventScsTip.meta.id, TestEventHub.EVENTS.get(7).meta.id);
    assertParentReferences(TestEventHub.EVENTS.get(5), TestEventHub.EVENTS.get(4));
    assertParentReferences(TestEventHub.EVENTS.get(6), TestEventHub.EVENTS.get(4));
    assertParentReferences(
        TestEventHub.EVENTS.get(7), TestEventHub.EVENTS.get(5), TestEventHub.EVENTS.get(6));
  }

  @Test
  public void missingScsWithMergeCommit3Parents() throws Exception {
    RevCommit initial = getHead(repo(), "HEAD");
    RevCommit middle1 = testRepo.commit().parent(initial).create();
    RevCommit middle2 = testRepo.commit().parent(initial).create();
    RevCommit middle3 = testRepo.commit().parent(initial).create();

    PushOneCommit m = pushFactory.create(admin.newIdent(), testRepo);
    m.setParents(ImmutableList.of(middle1, middle2, middle3));
    PushOneCommit.Result result = m.to(getHead());
    result.assertOkStatus();
    RevCommit tip = result.getCommit();

    SourceChangeEventKey scsInitial = SourceChangeEventKey.scsKey(project.get(), "master", initial);
    SourceChangeEventKey sccInitial = scsInitial.copy(SCC);
    SourceChangeEventKey scsMiddle1 = SourceChangeEventKey.scsKey(project.get(), "master", middle1);
    SourceChangeEventKey sccMiddle1 = scsMiddle1.copy(SCC);
    SourceChangeEventKey scsMiddle2 = SourceChangeEventKey.scsKey(project.get(), "master", middle2);
    SourceChangeEventKey sccMiddle2 = scsMiddle2.copy(SCC);
    SourceChangeEventKey scsMiddle3 = SourceChangeEventKey.scsKey(project.get(), "master", middle3);
    SourceChangeEventKey sccMiddle3 = scsMiddle3.copy(SCC);
    SourceChangeEventKey scsTip = SourceChangeEventKey.scsKey(project.get(), "master", tip);
    SourceChangeEventKey sccTip = scsTip.copy(SCC);
    eventParser.createAndScheduleMissingScssFromBranch(project.get(), getHead());

    assertEquals(10, TestEventHub.EVENTS.size());
    assertCorrectEvent(0, sccInitial);
    assertCorrectEvent(1, sccMiddle1);
    assertCorrectEvent(2, sccMiddle2);
    assertCorrectEvent(3, sccMiddle3);
    assertCorrectEvent(4, sccTip);
    assertCorrectEvent(5, scsInitial);
    assertCorrectEvent(6, scsMiddle1);
    assertCorrectEvent(7, scsMiddle2);
    assertCorrectEvent(8, scsMiddle3);
    assertCorrectEvent(9, scsTip);

    EiffelEvent eventScsTip = TestEventHub.EVENTS.remove(9);
    EiffelEvent eventScsMiddle3 = TestEventHub.EVENTS.remove(8);
    EiffelEvent eventScsMiddle2 = TestEventHub.EVENTS.remove(7);
    EiffelEvent eventScsMiddle1 = TestEventHub.EVENTS.remove(6);
    EiffelEvent eventScsInitial = TestEventHub.EVENTS.remove(5);

    eventParser.fillGapsForScsFromBranch(project.get(), getHead());

    assertEquals(10, TestEventHub.EVENTS.size());
    assertCorrectEvent(0, sccInitial);
    assertCorrectEvent(1, sccMiddle1);
    assertCorrectEvent(2, sccMiddle2);
    assertCorrectEvent(3, sccMiddle3);
    assertCorrectEvent(4, sccTip);
    assertCorrectEvent(5, scsInitial);
    assertCorrectEvent(6, scsMiddle1);
    assertCorrectEvent(7, scsMiddle2);
    assertCorrectEvent(8, scsMiddle3);
    assertCorrectEvent(9, scsTip);
    assertNotEquals(eventScsInitial.meta.id, TestEventHub.EVENTS.get(5).meta.id);
    assertNotEquals(eventScsMiddle1.meta.id, TestEventHub.EVENTS.get(6).meta.id);
    assertNotEquals(eventScsMiddle2.meta.id, TestEventHub.EVENTS.get(7).meta.id);
    assertNotEquals(eventScsMiddle3.meta.id, TestEventHub.EVENTS.get(8).meta.id);
    assertNotEquals(eventScsTip.meta.id, TestEventHub.EVENTS.get(9).meta.id);
    assertParentReferences(TestEventHub.EVENTS.get(6), TestEventHub.EVENTS.get(5));
    assertParentReferences(TestEventHub.EVENTS.get(7), TestEventHub.EVENTS.get(5));
    assertParentReferences(TestEventHub.EVENTS.get(8), TestEventHub.EVENTS.get(5));
    assertParentReferences(
        TestEventHub.EVENTS.get(9),
        TestEventHub.EVENTS.get(6),
        TestEventHub.EVENTS.get(7),
        TestEventHub.EVENTS.get(8));
  }

  @Test
  public void missingScsWithCommonParents() throws Exception {
    RevCommit initial = getHead(repo(), "HEAD");
    RevCommit middle1 = testRepo.commit().parent(initial).create();
    RevCommit middle2 = testRepo.commit().parent(initial).create();
    RevCommit middle3 = testRepo.commit().parent(middle1).parent(middle2).create();
    RevCommit middle4 = testRepo.commit().parent(middle1).parent(middle2).create();

    PushOneCommit m = pushFactory.create(admin.newIdent(), testRepo);
    m.setParents(ImmutableList.of(middle3, middle4));
    PushOneCommit.Result result = m.to(getHead());
    result.assertOkStatus();
    RevCommit tip = result.getCommit();

    SourceChangeEventKey scsInitial = SourceChangeEventKey.scsKey(project.get(), "master", initial);
    SourceChangeEventKey sccInitial = scsInitial.copy(SCC);
    SourceChangeEventKey scsMiddle1 = SourceChangeEventKey.scsKey(project.get(), "master", middle1);
    SourceChangeEventKey sccMiddle1 = scsMiddle1.copy(SCC);
    SourceChangeEventKey scsMiddle2 = SourceChangeEventKey.scsKey(project.get(), "master", middle2);
    SourceChangeEventKey sccMiddle2 = scsMiddle2.copy(SCC);
    SourceChangeEventKey scsMiddle3 = SourceChangeEventKey.scsKey(project.get(), "master", middle3);
    SourceChangeEventKey sccMiddle3 = scsMiddle3.copy(SCC);
    SourceChangeEventKey scsMiddle4 = SourceChangeEventKey.scsKey(project.get(), "master", middle4);
    SourceChangeEventKey sccMiddle4 = scsMiddle4.copy(SCC);
    SourceChangeEventKey scsTip = SourceChangeEventKey.scsKey(project.get(), "master", tip);
    SourceChangeEventKey sccTip = scsTip.copy(SCC);
    eventParser.createAndScheduleMissingScssFromBranch(project.get(), getHead());

    assertEquals(12, TestEventHub.EVENTS.size());
    assertCorrectEvent(0, sccInitial);
    assertCorrectEvent(1, sccMiddle1);
    assertCorrectEvent(2, sccMiddle2);
    assertCorrectEvent(3, sccMiddle3);
    assertCorrectEvent(4, sccMiddle4);
    assertCorrectEvent(5, sccTip);
    assertCorrectEvent(6, scsInitial);
    assertCorrectEvent(7, scsMiddle1);
    assertCorrectEvent(8, scsMiddle2);
    assertCorrectEvent(9, scsMiddle3);
    assertCorrectEvent(10, scsMiddle4);
    assertCorrectEvent(11, scsTip);

    EiffelEvent eventScsMiddle1 = TestEventHub.EVENTS.remove(7);

    EventParsingException thrown =
        assertThrows(
            EventParsingException.class,
            () -> eventParser.fillGapsForScsFromBranch(project.get(), getHead()));
    UUID id1 = eventScsMiddle1.meta.id;
    UUID id2 = TestEventHub.EVENTS.get(7).meta.id;
    String expectedMessage =
        String.format(
            "Found several potential UUID for %s potential UUID: (%2$s, %3$s|%3$s, %2$s)$",
            Pattern.quote(scsMiddle1.toString()), id1, id2);
    assertThat(thrown).hasMessageThat().matches(expectedMessage);

    assertEquals(11, TestEventHub.EVENTS.size());
    assertCorrectEvent(0, sccInitial);
    assertCorrectEvent(1, sccMiddle1);
    assertCorrectEvent(2, sccMiddle2);
    assertCorrectEvent(3, sccMiddle3);
    assertCorrectEvent(4, sccMiddle4);
    assertCorrectEvent(5, sccTip);
    assertCorrectEvent(6, scsInitial);
    assertCorrectEvent(7, scsMiddle2);
    assertCorrectEvent(8, scsMiddle3);
    assertCorrectEvent(9, scsMiddle4);
    assertCorrectEvent(10, scsTip);
  }

  @Test
  public void missingScsWithCircularCommonParents() throws Exception {
    RevCommit initial = getHead(repo(), "HEAD");
    RevCommit middle1 = testRepo.commit().parent(initial).create();
    RevCommit middle2 = testRepo.commit().parent(initial).create();
    RevCommit middle3 = testRepo.commit().parent(initial).create();
    RevCommit middle4 = testRepo.commit().parent(initial).create();
    RevCommit middle5 = testRepo.commit().parent(middle1).parent(middle2).parent(middle3).create();
    RevCommit middle6 = testRepo.commit().parent(middle2).parent(middle3).parent(middle4).create();
    RevCommit middle7 = testRepo.commit().parent(middle3).parent(middle4).parent(middle1).create();

    PushOneCommit m = pushFactory.create(admin.newIdent(), testRepo);
    m.setParents(ImmutableList.of(middle5, middle6, middle7));
    PushOneCommit.Result result = m.to(getHead());
    result.assertOkStatus();
    RevCommit tip = result.getCommit();

    SourceChangeEventKey scsInitial = SourceChangeEventKey.scsKey(project.get(), "master", initial);
    SourceChangeEventKey sccInitial = scsInitial.copy(SCC);
    SourceChangeEventKey scsMiddle1 = SourceChangeEventKey.scsKey(project.get(), "master", middle1);
    SourceChangeEventKey sccMiddle1 = scsMiddle1.copy(SCC);
    SourceChangeEventKey scsMiddle2 = SourceChangeEventKey.scsKey(project.get(), "master", middle2);
    SourceChangeEventKey sccMiddle2 = scsMiddle2.copy(SCC);
    SourceChangeEventKey scsMiddle3 = SourceChangeEventKey.scsKey(project.get(), "master", middle3);
    SourceChangeEventKey sccMiddle3 = scsMiddle3.copy(SCC);
    SourceChangeEventKey scsMiddle4 = SourceChangeEventKey.scsKey(project.get(), "master", middle4);
    SourceChangeEventKey sccMiddle4 = scsMiddle4.copy(SCC);
    SourceChangeEventKey scsMiddle5 = SourceChangeEventKey.scsKey(project.get(), "master", middle5);
    SourceChangeEventKey sccMiddle5 = scsMiddle5.copy(SCC);
    SourceChangeEventKey scsMiddle6 = SourceChangeEventKey.scsKey(project.get(), "master", middle6);
    SourceChangeEventKey sccMiddle6 = scsMiddle6.copy(SCC);
    SourceChangeEventKey scsMiddle7 = SourceChangeEventKey.scsKey(project.get(), "master", middle7);
    SourceChangeEventKey sccMiddle7 = scsMiddle7.copy(SCC);
    SourceChangeEventKey scsTip = SourceChangeEventKey.scsKey(project.get(), "master", tip);
    SourceChangeEventKey sccTip = scsTip.copy(SCC);
    eventParser.createAndScheduleMissingScssFromBranch(project.get(), getHead());

    assertEquals(18, TestEventHub.EVENTS.size());
    assertCorrectEvent(0, sccInitial);
    assertCorrectEvent(1, sccMiddle1);
    assertCorrectEvent(2, sccMiddle2);
    assertCorrectEvent(3, sccMiddle3);
    assertCorrectEvent(4, sccMiddle4);
    assertCorrectEvent(5, sccMiddle5);
    assertCorrectEvent(6, sccMiddle6);
    assertCorrectEvent(7, sccMiddle7);
    assertCorrectEvent(8, sccTip);
    assertCorrectEvent(9, scsInitial);
    assertCorrectEvent(10, scsMiddle1);
    assertCorrectEvent(11, scsMiddle2);
    assertCorrectEvent(12, scsMiddle3);
    assertCorrectEvent(13, scsMiddle4);
    assertCorrectEvent(14, scsMiddle5);
    assertCorrectEvent(15, scsMiddle6);
    assertCorrectEvent(16, scsMiddle7);
    assertCorrectEvent(17, scsTip);

    EiffelEvent eventScsMiddle3 = TestEventHub.EVENTS.remove(12);

    eventParser.fillGapsForScsFromBranch(project.get(), getHead());

    assertEquals(18, TestEventHub.EVENTS.size());
    assertCorrectEvent(0, sccInitial);
    assertCorrectEvent(1, sccMiddle1);
    assertCorrectEvent(2, sccMiddle2);
    assertCorrectEvent(3, sccMiddle3);
    assertCorrectEvent(4, sccMiddle4);
    assertCorrectEvent(5, sccMiddle5);
    assertCorrectEvent(6, sccMiddle6);
    assertCorrectEvent(7, sccMiddle7);
    assertCorrectEvent(8, sccTip);
    assertCorrectEvent(9, scsInitial);
    assertCorrectEvent(10, scsMiddle1);
    assertCorrectEvent(11, scsMiddle2);
    assertCorrectEvent(12, scsMiddle4);
    assertCorrectEvent(13, scsMiddle5);
    assertCorrectEvent(14, scsMiddle6);
    assertCorrectEvent(15, scsMiddle7);
    assertCorrectEvent(16, scsTip);
    assertCorrectEvent(17, scsMiddle3);
    assertEquals(eventScsMiddle3.meta.id, TestEventHub.EVENTS.get(17).meta.id);
    assertParentReferences(TestEventHub.EVENTS.get(17), TestEventHub.EVENTS.get(9));
  }

  private void assertParentReferences(EiffelEvent child, EiffelEvent... parents) {
    for (EiffelEvent parent : parents) {
      assertThat(Arrays.stream(child.links).map(link -> link.target).collect(Collectors.toList()))
          .contains(parent.meta.id);
    }
  }

  private void assertArtcQueuedScsHandled(boolean annotated) throws Exception {
    assertArtcQueuedScsHandled(annotated, "localhost");
  }

  private void assertArtcQueuedScsHandled(boolean annotated, String namespace) throws Exception {
    setScsHandled();
    String ref =
        createTagRef(getHead(repo(), "HEAD").getName(), annotated)
            .substring(Constants.R_TAGS.length());
    ArtifactEventKey artc = ArtifactEventKey.create(tagPURL(project.get(), ref, namespace));
    CompositionDefinedEventKey cd =
        CompositionDefinedEventKey.create(tagCompositionName(project.get(), namespace), ref);

    eventParser.createAndScheduleArtc(project.get(), ref, EPOCH_MILLIS, false);
    assertEquals(2, TestEventHub.EVENTS.size());
    assertCorrectEvent(0, cd);
    assertCorrectEvent(1, artc);
  }

  private void assertNbrQueriesFor(SourceChangeEventKey key, int nbrQueries) {
    assertEquals(nbrQueries, TestEventStorage.INSTANCE.queriesFor(key));
  }
}
