// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.cache;

import static com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType.SCC;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.google.gerrit.acceptance.LightweightPluginDaemonTest;
import com.google.gerrit.acceptance.TestPlugin;
import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.googlesource.gerrit.plugins.eventseiffel.TestEventStorage;
import com.googlesource.gerrit.plugins.eventseiffel.config.EventIdCacheConfig;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.EventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.SourceChangeEventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.api.EventStorage;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelGitIdentifierInfo;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelMetaInfo;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelSourceChangeCreatedEventInfo;
import java.util.Optional;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;

@TestPlugin(
    name = "events-eiffel",
    sysModule =
        "com.googlesource.gerrit.plugins.eventseiffel.cache.EiffelEventIdCacheIT$TestModule")
public class EiffelEventIdCacheIT extends LightweightPluginDaemonTest {
  private static final String COMMIT_A = "ff5250a50456c8c151591bb5dcaff4e8cc70b7f3";
  private static final String COMMIT_B = "ff5250a50456c8c151591bb5dcaff4e8cc70b7f5";
  private static final String BRANCH_A = "refs/heads/branch-a";
  private static final String BRANCH_B = "refs/heads/branch-b";
  private static final String REPO_A = "tests/project/a";
  private static final String REPO_B = "tests/project/b";
  private static final int CACHE_MAX_SIZE = 10;
  private EiffelEventIdCache cache;

  @Before
  public void setUp() {
    cache = plugin.getSysInjector().getInstance(EiffelEventIdCache.class);
    TestEventStorage.INSTANCE.reset();
    TestConfig.reset();
  }

  @Test
  public void loaderNotCalledWhenPopulated() throws Exception {
    UUID uuid = UUID.randomUUID();
    SourceChangeEventKey scc = SourceChangeEventKey.sccKey(REPO_A, BRANCH_A, COMMIT_A);
    cache.putId(
        EiffelSourceChangeCreatedEventInfo.builder()
            .gitIdentifier(
                EiffelGitIdentifierInfo.builder()
                    .repoName(scc.repo())
                    .branch(scc.branch())
                    .commitId(scc.commit())
                    .repoUri(scc.repo()))
            .meta(EiffelMetaInfo.builder().id(uuid))
            .build());
    Optional<UUID> fromCache = cache.getEventId(scc);
    assertEquals(Optional.of(uuid), fromCache);
    assertEquals(0, queriesFor(scc));
  }

  @Test
  public void loaderCalledWhenNotPopulated() throws Exception {
    SourceChangeEventKey scc = SourceChangeEventKey.sccKey(REPO_A, BRANCH_A, COMMIT_A);
    UUID actual = addEvent(scc);
    Optional<UUID> fromCache = cache.getEventId(scc);
    assertEquals(Optional.of(actual), fromCache);
    assertEquals(1, queriesFor(scc));
  }

  @Test
  public void differentTypesDoesNotInterfere() throws Exception {
    SourceChangeEventKey scsA = SourceChangeEventKey.scsKey(REPO_A, BRANCH_A, COMMIT_A);
    SourceChangeEventKey sccA = scsA.copy(SCC);

    UUID actual = addEvent(scsA);
    Optional<UUID> scsId = cache.getEventId(scsA);
    assertEquals(Optional.of(actual), scsId);
    assertEquals(1, queriesFor(scsA));
    assertEquals(0, queriesFor(sccA));
    Optional<UUID> sccId = cache.getEventId(sccA);
    assertEquals(Optional.empty(), sccId);
    assertEquals(1, queriesFor(scsA));
    assertEquals(1, queriesFor(sccA));

    SourceChangeEventKey scsB = SourceChangeEventKey.scsKey(REPO_B, BRANCH_B, COMMIT_B);
    SourceChangeEventKey sccB = scsB.copy(SCC);
    actual = addEvent(sccB);
    sccId = cache.getEventId(sccB);
    assertEquals(Optional.of(actual), sccId);
    assertEquals(1, queriesFor(sccB));
    assertEquals(0, queriesFor(scsB));
    scsId = cache.getEventId(scsB);
    assertEquals(Optional.empty(), scsId);
    assertEquals(1, queriesFor(sccB));
    assertEquals(1, queriesFor(scsB));
  }

  @Test
  public void eventStorageNotCalledWhenTrustLocalCacheIsSet() throws Exception {
    SourceChangeEventKey scc = SourceChangeEventKey.sccKey(REPO_A, BRANCH_A, COMMIT_A);
    TestConfig.trustLocalCache(true);
    Optional<UUID> fromCache = cache.getEventId(scc);
    assertTrue(fromCache.isEmpty());
    assertEquals(0, queriesFor(scc));
  }

  @Test
  public void eventRediscoverableAfterEnablingEventRepository() throws Exception {
    SourceChangeEventKey scc = SourceChangeEventKey.sccKey(REPO_A, BRANCH_A, COMMIT_A);
    addEvent(scc);
    TestConfig.trustLocalCache(true);
    Optional<UUID> fromCache = cache.getEventId(scc);
    assertTrue(fromCache.isEmpty());
    assertEquals(0, queriesFor(scc));
    TestConfig.trustLocalCache(false);
    fromCache = cache.getEventId(scc);
    assertTrue(fromCache.isPresent());
    assertEquals(1, queriesFor(scc));
  }

  @Test
  public void cacheMaxSizeIsHonored() throws Exception {
    SourceChangeEventKey scc = SourceChangeEventKey.sccKey(REPO_A, BRANCH_A, COMMIT_A);
    addEvent(scc);
    Optional<UUID> fromCache = cache.getEventId(scc);
    assertTrue(fromCache.isPresent());
    assertEquals(1, queriesFor(scc));

    cache.getEventId(scc);
    assertEquals(1, queriesFor(scc));

    SourceChangeEventKey tempScc;
    for (int i = 1; i <= CACHE_MAX_SIZE; i++) {
      tempScc = scc.copy(COMMIT_A.substring(0, COMMIT_A.length() - 6) + String.format("%06d", i));
      addEvent(tempScc);
      cache.getEventId(tempScc);
    }

    /* Let the cache eviction work. */
    Thread.sleep(200);

    cache.getEventId(scc);
    assertEquals(2, queriesFor(scc));
  }

  private static UUID addEvent(SourceChangeEventKey key) {
    return TestEventStorage.INSTANCE.addId(key);
  }

  private static int queriesFor(EventKey key) {
    return TestEventStorage.INSTANCE.queriesFor(key);
  }

  public static class TestModule extends AbstractModule {
    @Override
    protected void configure() {
      bind(EventStorage.class).toProvider(TestEventStorage.Provider.class).in(Singleton.class);
      bind(EventIdCacheConfig.class).to(TestConfig.class);
      install(EiffelEventIdCacheImpl.module(CACHE_MAX_SIZE));
    }
  }

  public static class TestConfig extends EventIdCacheConfig {

    private static boolean TRUST_CACHE = false;

    static void trustLocalCache(boolean trust) {
      TRUST_CACHE = trust;
    }

    static void reset() {
      TRUST_CACHE = false;
    }

    public TestConfig() {
      super(false, CACHE_MAX_SIZE);
    }

    @Override
    public boolean trustLocalCache() {
      return TRUST_CACHE;
    }
  }
}
