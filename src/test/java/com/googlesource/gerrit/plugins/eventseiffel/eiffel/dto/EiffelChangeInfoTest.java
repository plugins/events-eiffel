// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto;

import static org.junit.Assert.assertEquals;

import com.google.gson.Gson;
import org.junit.Test;

public class EiffelChangeInfoTest {

  @Test
  public void emptyObjectAllowed() {
    String serialized = new Gson().toJson(EiffelChangeInfo.builder().build());
    String expected = "{}";
    assertEquals("Matches json", expected, serialized);
  }

  @Test
  public void isSerializable() {
    String serialized =
        new Gson()
            .toJson(
                EiffelChangeInfo.builder()
                    .deletions(3)
                    .insertions(4)
                    .files("http://git/files")
                    .details("https://git/details")
                    .tracker("test")
                    .build());
    String expected =
        "{\"insertions\":4,\"deletions\":3,\"files\":\"http://git/files\",\"details\":\"https://git/details\",\"tracker\":\"test\"}";
    assertEquals("Matches json", expected, serialized);
  }

  @Test
  public void isDeSerializable() {
    String json =
        "{\"insertions\":4,\"deletions\":3,\"files\":\"http://git/files\",\"details\":\"https://git/details\",\"tracker\":\"test\"}";

    EiffelChangeInfo parsed = new Gson().fromJson(json, EiffelChangeInfo.class);
    assertEquals(Integer.valueOf(4), parsed.insertions);
    assertEquals(Integer.valueOf(3), parsed.deletions);
    assertEquals("http://git/files", parsed.files);
    assertEquals("https://git/details", parsed.details);
    assertEquals("test", parsed.tracker);
  }
}
