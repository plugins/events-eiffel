// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.rest;

import static com.google.gerrit.entities.RefNames.patchSetRef;
import static com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType.SCC;
import static com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType.SCS;
import static org.junit.Assert.assertEquals;

import com.google.gerrit.acceptance.PushOneCommit;
import com.google.gerrit.acceptance.RestResponse;
import com.google.gerrit.acceptance.TestPlugin;
import com.google.gerrit.entities.RefNames;
import com.google.inject.AbstractModule;
import com.googlesource.gerrit.plugins.eventseiffel.EiffelEventsTest;
import com.googlesource.gerrit.plugins.eventseiffel.EiffelEventsTestModule;
import com.googlesource.gerrit.plugins.eventseiffel.TestEventPublisher;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.ArtifactEventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.EventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.SourceChangeEventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEvent;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.transport.RefSpec;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

@TestPlugin(
    name = "events-eiffel",
    sysModule = "com.googlesource.gerrit.plugins.eventseiffel.rest.EventsEiffelRestIT$TestModule")
public class EventsEiffelRestIT extends EiffelEventsTest {
  private TestEventPublisher publisher;
  private RevCommit masterTip;

  @Before
  public void before() throws Exception {
    publisher = plugin.getSysInjector().getInstance(TestEventPublisher.class);
    git().fetch().setRefSpecs(new RefSpec(RefNames.REFS_HEADS + "master:FETCH_HEAD")).call();
    masterTip = getHead(repo(), "FETCH_HEAD");
  }

  @After
  public void cleanup() {
    if (publisher != null) {
      publisher.stop();
    }
  }

  @Test
  public void createSccsAsAdmin() throws Exception {
    createSccs("master", true).assertStatus(202);
    SourceChangeEventKey scc = keyFromMaster(SCC);
    EiffelEvent event = publisher.getPublished(scc);
    assertEquals(scc, EventKey.fromEvent(event));
  }

  @Test
  public void createSccsFromChangeRef() throws Exception {
    PushOneCommit.Result res = createChange();
    createSccs(patchSetRef(res.getPatchSetId()), true).assertStatus(202);
    SourceChangeEventKey masterKey = keyFromMaster(SCC);
    EventKey psKey = masterKey.copy(res.getCommit());

    EiffelEvent patchSet = publisher.getPublished(psKey);
    assertEquals(psKey, EventKey.fromEvent(patchSet));
    EiffelEvent master = publisher.getPublished(masterKey);
    assertEquals(masterKey, EventKey.fromEvent(master));
  }

  @Test
  public void createSccsAsNonAdminForbidden() throws Exception {
    createSccs("master", false).assertStatus(403);
    publisher.assertNotPublished(
        keyFromMaster(SCC), "SCC event creation should be blocked for non admin");
  }

  @Test
  public void createSccsFromTagForbidden() throws Exception {
    createSccs(createTagRef(true), true).assertStatus(400);
  }

  @Test
  public void createScssAsAdmin() throws Exception {
    createScss("master", true).assertStatus(202);
    SourceChangeEventKey scs = keyFromMaster(SCS);
    EiffelEvent event = publisher.getPublished(scs);
    assertEquals(scs, EventKey.fromEvent(event));
  }

  @Test
  public void createScssFromChangeRefForbidden() throws Exception {
    PushOneCommit.Result res = createChange();
    createScss(patchSetRef(res.getPatchSetId()), true).assertStatus(400);
  }

  @Test
  public void createScssAsNonAdminForbidden() throws Exception {
    createScss("master", false).assertStatus(403);
    publisher.assertNotPublished(
        keyFromMaster(SCS), "SCS event creation should be blocked for non admin");
  }

  @Test
  public void createScssFromTagForbidden() throws Exception {
    createScss(createTagRef(true), true).assertStatus(400);
  }

  @Test
  public void createArtcAsAdmin() throws Exception {
    String tagName = createTagRef(true).substring(RefNames.REFS_TAGS.length());
    createArtcs(tagName, true).assertStatus(202);
    ArtifactEventKey artc = ArtifactEventKey.create(tagPURL(project.get(), tagName));
    EiffelEvent event = publisher.getPublished(artc);
    assertEquals(artc, EventKey.fromEvent(event));
  }

  @Test
  public void createArtcAsNonAdminForbidden() throws Exception {
    String tagName = createTagRef(true).substring(RefNames.REFS_TAGS.length());
    createArtcs(tagName, false).assertStatus(403);
    publisher.assertNotPublished(
        ArtifactEventKey.create(tagPURL(project.get(), tagName)),
        "ArtC event creation should be blocked for non admin");
  }

  @Test
  public void createArtcFromBranchForbidden() throws Exception {
    createScss("master", true).assertStatus(202);
    createArtcs("master", true).assertStatus(404);
  }

  private SourceChangeEventKey keyFromMaster(EiffelEventType type) {
    switch (type) {
      case SCC:
        return SourceChangeEventKey.sccKey(project.get(), "master", masterTip);
      case SCS:
        return SourceChangeEventKey.scsKey(project.get(), "master", masterTip);

      default:
      case ARTC:
      case CD:
        return null;
    }
  }

  private RestResponse createSccs(String branch, boolean admin) throws Exception {
    return run("createSccs", branch, admin);
  }

  private RestResponse createScss(String branch, boolean admin) throws Exception {
    return run("createScss", branch, admin);
  }

  private RestResponse createArtcs(String tag, boolean admin) throws Exception {
    String endPoint =
        String.format(
            "/projects/%s/tags/%s/%s~createArtcs",
            project.get(), tag.replaceAll("/", "%2F"), PLUGIN_NAME);
    return (admin ? adminRestSession : userRestSession).post(endPoint, new CreateArtcs.Input());
  }

  private RestResponse run(String restCommand, String branch, boolean admin) throws Exception {
    String endPoint =
        String.format(
            "/projects/%s/branches/%s/%s~%s",
            project.get(), branch.replaceAll("/", "%2F"), PLUGIN_NAME, restCommand);
    return (admin ? adminRestSession : userRestSession).post(endPoint, new CreateSccs.Input());
  }

  public static class TestModule extends AbstractModule {
    @Override
    protected void configure() {
      install(new EiffelEventsTestModule());
      install(new RestModule());
    }
  }
}
