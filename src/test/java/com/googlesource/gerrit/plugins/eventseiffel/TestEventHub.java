// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel;

import com.google.common.collect.Lists;
import com.google.gerrit.extensions.restapi.NotImplementedException;
import com.google.inject.Inject;
import com.googlesource.gerrit.plugins.eventseiffel.cache.EiffelEventIdCache;
import com.googlesource.gerrit.plugins.eventseiffel.cache.EiffelEventIdLookupException;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.EventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.SourceChangeEventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEvent;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelLinkType;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import org.junit.Ignore;

@Ignore
public class TestEventHub implements EiffelEventHub {
  public static List<EiffelEvent> EVENTS = Lists.newArrayList();
  private final EiffelEventIdCache idCache;

  @Inject
  public TestEventHub(EiffelEventIdCache idCache) {
    this.idCache = idCache;
  }

  @Override
  public void push(EiffelEvent event, boolean force) {
    Optional<EiffelEvent> eventPushed = findEvent(EventKey.fromEvent(event));
    if (eventPushed.isEmpty()) EVENTS.add(event);
    else if (force) {
      EVENTS.remove(eventPushed.get());
      EVENTS.add(event);
    }
  }

  @Override
  public Optional<UUID> getExistingId(EventKey key) throws EiffelEventIdLookupException {
    Optional<UUID> eventId = findId(key);
    if (eventId.isPresent()) {
      return eventId;
    }
    return idCache.getEventId(key);
  }

  @Override
  public Optional<UUID> getScsForCommit(String repo, String commit, List<String> branches)
      throws EiffelEventIdLookupException {
    List<SourceChangeEventKey> keys =
        branches.stream()
            .map(branch -> SourceChangeEventKey.scsKey(repo, branch, commit))
            .collect(Collectors.toList());
    Optional<UUID> id = keys.stream().map(key -> findId(key)).flatMap(Optional::stream).findAny();
    if (id.isPresent()) return id;
    return idCache.getScsForCommit(repo, commit, branches);
  }

  @Override
  public Optional<List<UUID>> getParentLinks(EventKey key) throws EiffelEventIdLookupException {
    return findEvent(key)
        .map(
            event ->
                Arrays.stream(event.links)
                    .filter(link -> link.type.equals(EiffelLinkType.PREVIOUS_VERSION))
                    .map(link -> link.target)
                    .collect(Collectors.toList()));
  }

  @Override
  public Optional<UUID> getSccEventLink(SourceChangeEventKey key)
      throws EiffelEventIdLookupException {
    return findEvent(key)
        .map(
            event ->
                Arrays.stream(event.links)
                    .filter(link -> link.type.equals(EiffelLinkType.CHANGE))
                    .map(link -> link.target)
                    .findAny()
                    .get());
  }

  private Optional<EiffelEvent> findEvent(EventKey key) {
    for (EiffelEvent event : EVENTS) {
      if (key.equals(EventKey.fromEvent(event))) {
        return Optional.of(event);
      }
    }
    return Optional.empty();
  }

  private Optional<UUID> findId(EventKey key) {
    return findEvent(key).map(event -> event.meta.id);
  }

  @Override
  public Set<EiffelEvent> take(int max) throws InterruptedException {
    return null;
  }

  @Override
  public void ack(Set<EiffelEvent> events) {
    throw new NotImplementedException();
  }

  @Override
  public void nak(Set<EiffelEvent> events) {
    throw new NotImplementedException();
  }

  @Override
  public void startPublishing() {
    throw new NotImplementedException();
  }

  @Override
  public void stopPublishing() {
    throw new NotImplementedException();
  }

  @Override
  public boolean isOpen() {
    throw new NotImplementedException();
  }
}
