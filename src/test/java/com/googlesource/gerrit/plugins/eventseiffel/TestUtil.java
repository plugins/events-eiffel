// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel;

import com.google.common.collect.Lists;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEvent;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelGitIdentifierInfo;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelMetaInfo;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelSourceChangeCreatedEventInfo;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelSourceChangeSubmittedEventInfo;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import java.util.stream.IntStream;
import org.junit.Ignore;

@Ignore
public abstract class TestUtil {
  private static final String REPO_NAME = "my/project";
  private static final String BRANCH = "master";
  private static final String SHA1 = "%040x";
  private static int sha1Incr = 0;

  public static EiffelEvent copyGitIdentifier(
      EiffelSourceChangeCreatedEventInfo event, EiffelEventType type) {
    EiffelGitIdentifierInfo from = event.data.gitIdentifier;
    EiffelGitIdentifierInfo.Builder gitIdCopy =
        EiffelGitIdentifierInfo.builder()
            .repoName(from.repoName)
            .branch(from.branch)
            .commitId(from.commitId)
            .repoUri(from.repoUri);
    switch (type) {
      case SCC:
        return EiffelSourceChangeCreatedEventInfo.builder()
            .meta(EiffelMetaInfo.builder().id(UUID.randomUUID()).time(Instant.now().toEpochMilli()))
            .gitIdentifier(gitIdCopy)
            .build();
      case SCS:
        return EiffelSourceChangeSubmittedEventInfo.builder()
            .meta(EiffelMetaInfo.builder().id(UUID.randomUUID()).time(Instant.now().toEpochMilli()))
            .gitIdentifier(gitIdCopy)
            .build();

      default:
      case ARTC:
      case CD:
        return null;
    }
  }

  public static EiffelSourceChangeCreatedEventInfo newScc() {
    return newSccs(1).get(0);
  }

  public static List<EiffelSourceChangeCreatedEventInfo> newSccs(int nbrOfEvents) {
    List<EiffelSourceChangeCreatedEventInfo> events = Lists.newArrayList();
    for (UUID uuid :
        IntStream.range(0, nbrOfEvents).mapToObj(i -> UUID.randomUUID()).toArray(UUID[]::new)) {
      events.add(
          EiffelSourceChangeCreatedEventInfo.builder()
              .meta(EiffelMetaInfo.builder().id(uuid).time(Instant.now().toEpochMilli()))
              .gitIdentifier(
                  EiffelGitIdentifierInfo.builder()
                      .repoName(REPO_NAME)
                      .commitId(String.format(SHA1, sha1Incr))
                      .branch(BRANCH)
                      .repoUri("ssh://git-server/repo"))
              .build());
      sha1Incr++;
    }
    return events;
  }
}
