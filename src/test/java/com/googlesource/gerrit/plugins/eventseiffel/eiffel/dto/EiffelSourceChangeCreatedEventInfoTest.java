// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto;

import static org.junit.Assert.assertEquals;

import com.google.gson.Gson;
import java.util.UUID;
import org.junit.Test;

public class EiffelSourceChangeCreatedEventInfoTest {

  @Test
  public void isSerializable() {
    String serialized =
        new Gson()
            .toJson(
                EiffelSourceChangeCreatedEventInfo.builder()
                    .change(EiffelChangeInfo.builder().id("change-id").tracker("tracker"))
                    .author(
                        EiffelPersonInfo.builder().name("Author Name").email("author@email.com"))
                    .gitIdentifier(
                        EiffelGitIdentifierInfo.builder()
                            .commitId("1103c14f6a25e124cde1bc00abc0aaf86c223aad")
                            .repoUri("my/repo"))
                    .meta(
                        EiffelMetaInfo.builder()
                            .addTag("a-tag")
                            .id(UUID.fromString("ffffffff-aaaa-cccc-eeee-dddddddddddd"))
                            .time(123456))
                    .addLink(
                        EiffelLinkInfo.builder()
                            .type(EiffelLinkType.PREVIOUS_VERSION)
                            .target(UUID.fromString("aaaaaaaa-bbbb-cccc-eeee-ffffffffffff")))
                    .build());
    String expected =
        "{\"data\":{\"gitIdentifier\":{\"commitId\":\"1103c14f6a25e124cde1bc00abc0aaf86c223aad\",\"repoUri\":\"my/repo\"},\"author\":{\"name\":\"Author Name\",\"email\":\"author@email.com\"},\"change\":{\"tracker\":\"tracker\",\"id\":\"change-id\"}},\"meta\":{\"type\":\"EiffelSourceChangeCreatedEvent\",\"version\":\"4.0.0\",\"tags\":[\"a-tag\"],\"time\":123456,\"id\":\"ffffffff-aaaa-cccc-eeee-dddddddddddd\"},\"links\":[{\"type\":\"PREVIOUS_VERSION\",\"target\":\"aaaaaaaa-bbbb-cccc-eeee-ffffffffffff\"}]}";
    assertEquals("Json matches", expected, serialized);
  }

  @Test
  public void isDeSerializable() {
    String json =
        "{\"data\":{\"gitIdentifier\":{\"commitId\":\"1103c14f6a25e124cde1bc00abc0aaf86c223aad\",\"repoUri\":\"my/repo\"},\"author\":{\"name\":\"Author Name\",\"email\":\"author@email.com\"},\"change\":{\"tracker\":\"tracker\",\"id\":\"change-id\"}},\"meta\":{\"id\":\"ffffffff-aaaa-cccc-eeee-dddddddddddd\",\"time\":123456,\"type\":\"EiffelSourceChangeCreatedEvent\",\"version\":\"4.0.0\",\"tags\":[\"a-tag\"]},\"links\":[{\"type\":\"PREVIOUS_VERSION\",\"target\":\"aaaaaaaa-bbbb-cccc-eeee-ffffffffffff\"}]}";
    EiffelSourceChangeCreatedEventInfo parsed =
        new Gson().fromJson(json, EiffelSourceChangeCreatedEventInfo.class);
    assertEquals("change-id", parsed.data.change.id);
    assertEquals("tracker", parsed.data.change.tracker);
    assertEquals("Author Name", parsed.data.author.name);
    assertEquals("author@email.com", parsed.data.author.email);
    assertEquals("1103c14f6a25e124cde1bc00abc0aaf86c223aad", parsed.data.gitIdentifier.commitId);
    assertEquals("my/repo", parsed.data.gitIdentifier.repoUri);
    assertEquals("a-tag", parsed.meta.tags[0]);
    assertEquals(EiffelLinkType.PREVIOUS_VERSION, parsed.links[0].type);
    assertEquals(UUID.fromString("aaaaaaaa-bbbb-cccc-eeee-ffffffffffff"), parsed.links[0].target);
    assertEquals("4.0.0", parsed.meta.version);
    assertEquals(EiffelEventType.SCC, parsed.meta.type);
    assertEquals(UUID.fromString("ffffffff-aaaa-cccc-eeee-dddddddddddd"), parsed.meta.id);
    assertEquals(Long.valueOf(123456), parsed.meta.time);
  }
}
