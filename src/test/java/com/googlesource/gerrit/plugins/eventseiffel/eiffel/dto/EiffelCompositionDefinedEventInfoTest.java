// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto;

import static com.google.common.truth.Truth.assertThat;
import static com.google.gerrit.testing.GerritJUnit.assertThrows;
import static org.junit.Assert.assertEquals;

import com.google.gson.Gson;
import java.util.UUID;
import org.junit.Test;

public class EiffelCompositionDefinedEventInfoTest {

  @Test
  public void nameIsRequired() {
    IllegalStateException thrown =
        assertThrows(
            IllegalStateException.class, () -> EiffelCompositionDefinedEventInfo.builder().build());
    assertThat(thrown).hasMessageThat().isEqualTo("name is required");
  }

  @Test
  public void isSerializable() {
    String serialized =
        new Gson()
            .toJson(
                EiffelCompositionDefinedEventInfo.builder()
                    .name("https://gerrit.googlesource.com/gerrit")
                    .version("artifact-tag")
                    .meta(
                        EiffelMetaInfo.builder()
                            .addTag("a-tag")
                            .id(UUID.fromString("ffffffff-aaaa-cccc-eeee-dddddddddddd"))
                            .time(123456))
                    .addLink(
                        EiffelLinkInfo.builder()
                            .type(EiffelLinkType.PREVIOUS_VERSION)
                            .target(UUID.fromString("aaaaaaaa-bbbb-cccc-eeee-ffffffffffff")))
                    .build());
    String expected =
        "{\"data\":{\"name\":\"https://gerrit.googlesource.com/gerrit\",\"version\":\"artifact-tag\"},\"meta\":{\"type\":\"EiffelCompositionDefinedEvent\",\"version\":\"3.0.0\",\"tags\":[\"a-tag\"],\"time\":123456,\"id\":\"ffffffff-aaaa-cccc-eeee-dddddddddddd\"},\"links\":[{\"type\":\"PREVIOUS_VERSION\",\"target\":\"aaaaaaaa-bbbb-cccc-eeee-ffffffffffff\"}]}";
    assertEquals("Matches json", expected, serialized);
  }

  @Test
  public void testSimpleJson() {
    String json =
        "{\"meta\":{\"type\":\"EiffelCompositionDefinedEvent\",\"version\":\"3.0.0\",\"time\":1234567890,\"id\":\"aaaaaaaa-bbbb-5ccc-8ddd-eeeeeeeeeee0\"},\"data\":{\"name\":\"https://gerrit.googlesource.com/gerrit\",\"version\":\"artifact-tag\"},\"links\":[{\"type\":\"CHANGE\",\"target\":\"aaaaaaaa-bbbb-5ccc-8ddd-eeeeeeeeeee1\"},{\"type\":\"PREVIOUS_VERSION\",\"target\":\"aaaaaaaa-bbbb-5ccc-8ddd-eeeeeeeeeee2\"}]}";
    EiffelCompositionDefinedEventInfo parsed =
        new Gson().fromJson(json, EiffelCompositionDefinedEventInfo.class);
    assertEquals(EiffelEventType.CD, parsed.meta.type);
    assertEquals("3.0.0", parsed.meta.version);
    assertEquals(Long.valueOf(1234567890), parsed.meta.time);
    assertEquals(UUID.fromString("aaaaaaaa-bbbb-5ccc-8ddd-eeeeeeeeeee0"), parsed.meta.id);
    assertEquals("https://gerrit.googlesource.com/gerrit", parsed.data.name);
    assertEquals("artifact-tag", parsed.data.version);
    assertEquals(EiffelLinkType.CHANGE, parsed.links[0].type);
    assertEquals(UUID.fromString("aaaaaaaa-bbbb-5ccc-8ddd-eeeeeeeeeee1"), parsed.links[0].target);
    assertEquals(EiffelLinkType.PREVIOUS_VERSION, parsed.links[1].type);
    assertEquals(UUID.fromString("aaaaaaaa-bbbb-5ccc-8ddd-eeeeeeeeeee2"), parsed.links[1].target);
  }
}
