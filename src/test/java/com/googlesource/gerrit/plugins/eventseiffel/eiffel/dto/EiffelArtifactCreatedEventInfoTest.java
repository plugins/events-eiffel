// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto;

import static com.google.common.truth.Truth.assertThat;
import static com.google.gerrit.testing.GerritJUnit.assertThrows;
import static org.junit.Assert.assertEquals;

import com.google.gson.Gson;
import java.util.UUID;
import org.junit.Test;

public class EiffelArtifactCreatedEventInfoTest {

  @Test
  public void identityIsRequired() {
    IllegalStateException thrown =
        assertThrows(
            IllegalStateException.class, () -> EiffelArtifactCreatedEventInfo.builder().build());
    assertThat(thrown).hasMessageThat().isEqualTo("identity is required");
  }

  @Test
  public void isSerializable() {
    String serialized =
        new Gson()
            .toJson(
                EiffelArtifactCreatedEventInfo.builder()
                    .identity("scheme:type/namespace/name@version?qualifiers#subpath")
                    .fileInformation(
                        EiffelFileInfo.builder()
                            .name("package/artifacts/file")
                            .addTag("artifact-tag"))
                    .buildCommand("bazelisk")
                    .requiresImplementation("NONE")
                    .addImplements(
                        "pkg:golang/google.golang.org/genproto#googleapis/api/annotation")
                    .addDependancy("pkg:rpm/fedora/curl@7.50.3-1.fc25?arch=i386&distro=fedora-25")
                    .name("colloquial-name")
                    .meta(
                        EiffelMetaInfo.builder()
                            .addTag("a-tag")
                            .id(UUID.fromString("ffffffff-aaaa-cccc-eeee-dddddddddddd"))
                            .time(123456))
                    .addLink(
                        EiffelLinkInfo.builder()
                            .type(EiffelLinkType.PREVIOUS_VERSION)
                            .target(UUID.fromString("aaaaaaaa-bbbb-cccc-eeee-ffffffffffff")))
                    .build());
    String expected =
        "{\"data\":{\"identity\":\"scheme:type/namespace/name@version?qualifiers#subpath\",\"fileInformation\":[{\"name\":\"package/artifacts/file\",\"tags\":[\"artifact-tag\"]}],\"buildCommand\":\"bazelisk\",\"requiresImplementation\":\"NONE\",\"implements\":[\"pkg:golang/google.golang.org/genproto#googleapis/api/annotation\"],\"dependsOn\":[\"pkg:rpm/fedora/curl@7.50.3-1.fc25?arch\\u003di386\\u0026distro\\u003dfedora-25\"],\"name\":\"colloquial-name\"},\"meta\":{\"type\":\"EiffelArtifactCreatedEvent\",\"version\":\"3.0.0\",\"tags\":[\"a-tag\"],\"time\":123456,\"id\":\"ffffffff-aaaa-cccc-eeee-dddddddddddd\"},\"links\":[{\"type\":\"PREVIOUS_VERSION\",\"target\":\"aaaaaaaa-bbbb-cccc-eeee-ffffffffffff\"}]}";
    assertEquals("Matches json", expected, serialized);
  }

  @Test
  public void testSimpleJson() {
    String json =
        "{\"meta\":{\"type\":\"EiffelArtifactCreatedEvent\",\"version\":\"3.0.0\",\"time\":1234567890,\"id\":\"aaaaaaaa-bbbb-5ccc-8ddd-eeeeeeeeeee0\"},\"data\":{\"identity\":\"scheme:type/namespace/name@version?qualifiers#subpath\",\"fileInformation\":[{\"name\":\"package/artifacts/file\",\"tags\":[\"artifact-tag\"]}],\"buildCommand\":\"bazelisk\",\"requiresImplementation\":\"NONE\",\"implements\":[\"pkg:golang/google.golang.org/genproto#googleapis/api/annotation\"],\"dependsOn\":[\"pkg:rpm/fedora/curl@7.50.3-1.fc25?arch=i386&distro=fedora-25\"],\"name\":\"colloquial-name\"},\"links\":[{\"type\":\"CHANGE\",\"target\":\"aaaaaaaa-bbbb-5ccc-8ddd-eeeeeeeeeee1\"},{\"type\":\"PREVIOUS_VERSION\",\"target\":\"aaaaaaaa-bbbb-5ccc-8ddd-eeeeeeeeeee2\"}]}";
    EiffelArtifactCreatedEventInfo parsed =
        new Gson().fromJson(json, EiffelArtifactCreatedEventInfo.class);
    assertEquals(EiffelEventType.ARTC, parsed.meta.type);
    assertEquals("3.0.0", parsed.meta.version);
    assertEquals(Long.valueOf(1234567890), parsed.meta.time);
    assertEquals(UUID.fromString("aaaaaaaa-bbbb-5ccc-8ddd-eeeeeeeeeee0"), parsed.meta.id);
    assertEquals("scheme:type/namespace/name@version?qualifiers#subpath", parsed.data.identity);
    assertEquals(1, parsed.data.fileInformation.length);
    assertEquals("package/artifacts/file", parsed.data.fileInformation[0].name);
    assertEquals(1, parsed.data.fileInformation[0].tags.length);
    assertEquals("artifact-tag", parsed.data.fileInformation[0].tags[0]);
    assertEquals("bazelisk", parsed.data.buildCommand);
    assertEquals("NONE", parsed.data.requiresImplementation);
    assertEquals(1, parsed.data.implementsField.length);
    assertEquals(
        "pkg:golang/google.golang.org/genproto#googleapis/api/annotation",
        parsed.data.implementsField[0]);
    assertEquals(1, parsed.data.dependsOn.length);
    assertEquals(
        "pkg:rpm/fedora/curl@7.50.3-1.fc25?arch=i386&distro=fedora-25", parsed.data.dependsOn[0]);
    assertEquals("colloquial-name", parsed.data.name);
    assertEquals(EiffelLinkType.CHANGE, parsed.links[0].type);
    assertEquals(UUID.fromString("aaaaaaaa-bbbb-5ccc-8ddd-eeeeeeeeeee1"), parsed.links[0].target);
    assertEquals(EiffelLinkType.PREVIOUS_VERSION, parsed.links[1].type);
    assertEquals(UUID.fromString("aaaaaaaa-bbbb-5ccc-8ddd-eeeeeeeeeee2"), parsed.links[1].target);
  }
}
