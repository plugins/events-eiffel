// Copyright (C) 2022 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package com.googlesource.gerrit.plugins.eventseiffel.parsing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import com.google.gerrit.extensions.common.AccountInfo;
import com.google.gson.Gson;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType;
import org.junit.Test;

public class ParsingQueueTaskTest {

  @Test
  public void testParsingQueueTaskSerializeable() {
    AccountInfo accountInfo = new AccountInfo(1);
    ParsingQueueTask task =
        ParsingQueueTask.builder(EiffelEventType.SCC, "repo", "branch")
            .commit("commit-sha1")
            .updater(accountInfo)
            .updateTime(0L)
            .previousTip("previous")
            .force(true)
            .fillGaps(true)
            .build();
    String json = new Gson().toJson(task);
    String expected =
        "{\"repoName\":\"repo\",\"branchRefOrTag\":\"branch\",\"commitId\":\"commit-sha1\",\"type\":\"EiffelSourceChangeCreatedEvent\",\"updater\":{\"_accountId\":1},\"updateTime\":0,\"force\":true,\"previousTip\":\"previous\",\"fillGaps\":true}";
    assertEquals(json, expected);
  }

  @Test
  public void testParsingQueueTaskDeserializeable() {
    String json =
        "{\"repoName\":\"repo\",\"branchRefOrTag\":\"branch\",\"commitId\":\"commit-sha1\",\"type\":\"EiffelSourceChangeCreatedEvent\",\"updater\":{\"_accountId\":1},\"updateTime\":0,\"force\":true,\"previousTip\":\"previous\",\"fillGaps\":true}";
    ParsingQueueTask deSerialized = new Gson().fromJson(json, ParsingQueueTask.class);
    assertEquals(deSerialized.repoName, "repo");
    assertEquals(deSerialized.branchRefOrTag, "branch");
    assertEquals(deSerialized.commitId, "commit-sha1");
    assertEquals(deSerialized.type, EiffelEventType.SCC);
    assertEquals(deSerialized.updater._accountId, Integer.valueOf(1));
    assertEquals(deSerialized.updateTime, Long.valueOf(0));
    assertEquals(deSerialized.force, Boolean.TRUE);
    assertEquals(deSerialized.fillGaps, Boolean.TRUE);
    assertEquals(deSerialized.previousTip, "previous");
  }

  @Test
  public void testEqualsAndHashCode() {
    ParsingQueueTask dis = ParsingQueueTask.builder(EiffelEventType.SCC, "repo", "branch").build();
    ParsingQueueTask dat = ParsingQueueTask.builder(EiffelEventType.SCC, "repo", "branch").build();
    assertEquals(dis, dat);
    assertEquals(dis.hashCode(), dat.hashCode());
    ParsingQueueTask theOther =
        ParsingQueueTask.builder(EiffelEventType.SCC, "repo", "branch").commit("commit").build();
    assertNotEquals(dis, theOther);
    assertNotEquals(dis.hashCode(), theOther.hashCode());
  }
}
