// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.config;

import static com.googlesource.gerrit.plugins.eventseiffel.config.EventsFilter.Provider.filterMatches;
import static com.googlesource.gerrit.plugins.eventseiffel.config.EventsFilter.Provider.fromConfig;
import static com.googlesource.gerrit.plugins.eventseiffel.config.EventsFilter.Provider.toCombinedMatcher;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.function.Function;
import org.eclipse.jgit.lib.Config;
import org.junit.Test;

public class EventsFilterTest {

  @Test
  public void regexBlockRefPattern() throws Exception {
    EventsFilter filter = refsPatterns("^refs/.*/blocked");
    assertTrue(filter.refIsBlocked("refs/that/are/blocked"));
    assertFalse(filter.refIsBlocked("refs/allowed"));

    filter = refsPatterns("^refs/(heads|tags)");
    assertTrue(filter.refIsBlocked("refs/heads"));
    assertTrue(filter.refIsBlocked("refs/tags"));
    assertFalse(filter.refIsBlocked("refs/meta"));
  }

  @Test
  public void wildcardBlockRefPattern() throws Exception {
    EventsFilter filter = refsPatterns("");
    assertFalse(filter.refIsBlocked("refs/blocked/always"));

    filter = refsPatterns("refs/blocked/*");
    assertTrue(filter.refIsBlocked("refs/blocked/always"));
    assertFalse(filter.refIsBlocked("refs/allowed/always"));
  }

  @Test
  public void exactBlockRefPattern() throws Exception {
    EventsFilter filter = refsPatterns("");
    assertFalse(filter.refIsBlocked("refs/blocked/exact"));

    filter = refsPatterns("refs/blocked/exact");
    assertTrue(filter.refIsBlocked("refs/blocked/exact"));
    assertFalse(filter.refIsBlocked("refs/blocked/exact2"));
  }

  @Test
  public void combinedBlockRefPattern() throws Exception {
    EventsFilter filter = refsPatterns("");
    assertFalse(filter.refIsBlocked("refs/certainly/blocked"));
    assertFalse(filter.refIsBlocked("refs/blocked/always"));
    assertFalse(filter.refIsBlocked("refs/not/allowed/at/all"));

    filter = refsPatterns("refs/blocked/*", "^refs/.*/blocked", "refs/not/allowed/at/all");
    assertTrue(filter.refIsBlocked("refs/certainly/blocked"));
    assertTrue(filter.refIsBlocked("refs/blocked/always"));
    assertTrue(filter.refIsBlocked("refs/not/allowed/at/all"));
  }

  @Test
  public void regexBlockProjectPattern() throws Exception {
    assertProjectBlockedByPatterns("blocked/project", "^blocked/.*");
  }

  @Test
  public void wildcardBlockProjectPattern() throws Exception {
    assertProjectBlockedByPatterns("blocked/project", "blocked*");
  }

  @Test
  public void exactBlockProjectPattern() throws Exception {
    assertProjectBlockedByPatterns("blocked/project", "blocked/project");
  }

  private void assertProjectBlockedByPatterns(String project, String... patterns) {
    assertProjectNotBlockedWithoutConfiguration(project);
    assertTrue(projectsPatterns(patterns).projectIsBlocked(project));
  }

  private void assertProjectNotBlockedWithoutConfiguration(String project) {
    assertFalse(projectsPatterns().projectIsBlocked(project));
  }

  @Test
  public void exactProjectBlockerParsedCorrectly() {
    assertTrue(filterMatches("project", "project"));
    assertFalse(filterMatches("project", "projec"));
    assertFalse(filterMatches("project", "projects"));
  }

  @Test
  public void wildCardProjectBlockerParsedCorrectly() {
    assertTrue(filterMatches("blocked/project/*", "blocked/project/something"));
    assertFalse(filterMatches("blocked/project/*", "project/something"));
    assertFalse(filterMatches("blocked/project/*", "other/project/something"));
  }

  @Test
  public void regexProjectBlockerParsedCorrectly() {
    assertFalse(filterMatches("^project/\\d{3}", "project/12"));
    assertTrue(filterMatches("^project/\\d{3}", "project/123"));
    assertFalse(filterMatches("^project/\\d{3}", "project/1234"));
  }

  @Test
  public void blocksUnionOfAllFilters() {
    Function<String, Boolean> unionMatcher =
        toCombinedMatcher("project", "blocked/project/*", "^project/\\d{3}$");
    assertTrue(unionMatcher.apply("project"));
    assertTrue(unionMatcher.apply("project/123"));
    assertTrue(unionMatcher.apply("blocked/project/something"));
    assertFalse(unionMatcher.apply("projects"));
    assertFalse(unionMatcher.apply("project/1234"));
    assertFalse(unionMatcher.apply("project/something/blocked"));
  }

  @Test
  public void configParsedCorrectly() throws Exception {
    Config cfg = new Config();
    cfg.fromText(
        "[EventsFilter]\n"
            + "blockedRef = refs/heads/blocked\n"
            + "blockedRef = refs/heads/also/blocked\n"
            + "blockedProject = blocked/project\n"
            + "blockedProject = blocked/projects/*\n"
            + "enabled = False\n");
    EventsFilter fromText = fromConfig(cfg);

    assertTrue(fromText.refIsBlocked("refs/heads/blocked"));
    assertTrue(fromText.refIsBlocked("refs/heads/also/blocked"));
    assertTrue(fromText.projectIsBlocked("blocked/project"));
    assertTrue(fromText.projectIsBlocked("blocked/projects/something"));
  }

  private EventsFilter refsPatterns(String... patterns) {
    return new EventsFilter(toCombinedMatcher(), toCombinedMatcher(patterns), false);
  }

  private EventsFilter projectsPatterns(String... patterns) {
    return new EventsFilter(toCombinedMatcher(patterns), toCombinedMatcher(), false);
  }
}
