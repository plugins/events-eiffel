// Copyright (C) 2022 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package com.googlesource.gerrit.plugins.eventseiffel.parsing;

import static com.google.common.truth.Truth.assertThat;

import com.google.common.collect.Lists;
import com.google.gerrit.acceptance.LightweightPluginDaemonTest;
import com.google.gerrit.acceptance.TestPlugin;
import com.google.gerrit.acceptance.UseLocalDisk;
import com.google.gerrit.entities.Project;
import com.google.gerrit.extensions.common.AccountInfo;
import com.google.gerrit.server.git.ProjectRunnable;
import com.google.inject.AbstractModule;
import com.google.inject.Provider;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.SourceChangeEventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Delayed;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.junit.Before;
import org.junit.Test;

@UseLocalDisk
@TestPlugin(
    name = "events-eiffel",
    sysModule =
        "com.googlesource.gerrit.plugins.eventseiffel.parsing.ParsingQueuePersistenceIT$TestModule")
public class ParsingQueuePersistenceIT extends LightweightPluginDaemonTest {

  private ParsingQueuePersistence queuePersistence;
  private EiffelEventParsingQueue parsingQueue;

  @Before
  public void setUp() throws Exception {
    queuePersistence = plugin.getSysInjector().getInstance(ParsingQueuePersistence.class);
    /* Purge persistence from previous runs. */
    queuePersistence.persistFailed(Set.of());
    queuePersistence.persistQueue(Set.of());

    parsingQueue = plugin.getSysInjector().getInstance(EiffelEventParsingQueue.class);
  }

  @Test
  public void queueIsPersisted() throws Exception {
    Set<ParsingQueueTask> original = createTasks(1, 5);
    queuePersistence.persistQueue(original);
    assertThat(original).containsExactlyElementsIn(queuePersistence.getPersistedQueue());
  }

  @Test
  public void failedTasksArePersisted() throws Exception {
    Set<ParsingQueueTask> original = createTasks(1, 5);
    queuePersistence.persistFailed(original);
    assertThat(original).containsExactlyElementsIn(queuePersistence.getFailedTasks());
  }

  @Test
  public void multipleQueuePersistenceRuns() throws Exception {
    Set<ParsingQueueTask> original = createTasks(1, 6);
    queuePersistence.persistQueue(original);
    assertThat(original).containsExactlyElementsIn(queuePersistence.getPersistedQueue());

    original = createTasks(6, 10);
    queuePersistence.persistQueue(original);
    assertThat(original).containsExactlyElementsIn(queuePersistence.getPersistedQueue());
  }

  @Test
  public void persistedQueuedTasksAreScheduledOnInit() throws Exception {
    Set<ParsingQueueTask> tasks = createTasks(1, 6);
    queuePersistence.persistQueue(tasks);
    parsingQueue.init();
    assertThat(
            TestExecutor.scheduledItems().stream()
                .map(p -> p.getProjectNameKey())
                .collect(Collectors.toList()))
        .containsExactlyElementsIn(
            tasks.stream().map(t -> Project.nameKey(t.repoName)).collect(Collectors.toList()));
  }

  @Test
  public void persistedFailedTasksAreScheduledOnInit() throws Exception {
    Set<ParsingQueueTask> tasks = createTasks(1, 6);
    queuePersistence.persistFailed(tasks);
    parsingQueue.init();
    assertThat(
            TestExecutor.scheduledItems().stream()
                .map(p -> p.getProjectNameKey())
                .collect(Collectors.toList()))
        .containsExactlyElementsIn(
            tasks.stream().map(t -> Project.nameKey(t.repoName)).collect(Collectors.toList()));
  }

  private Set<ParsingQueueTask> createTasks(int start, int end) {
    return IntStream.range(start, end)
        .mapToObj(ParsingQueuePersistenceIT::createTask)
        .collect(Collectors.toSet());
  }

  public static ParsingQueueTask createTask(int id) {
    return ParsingQueueTask.builder(EiffelEventType.SCC, "repo" + id, "branch" + id)
        .commit("commit-sha1" + id)
        .build();
  }

  public static class TestExecutor
      implements EiffelEventParsingExecutor, Provider<EiffelEventParsingExecutor> {
    private static TestExecutor INSTANCE;

    public static List<ProjectRunnable> scheduledItems() {
      return INSTANCE.scheduledItems;
    }

    private final List<ProjectRunnable> scheduledItems = Lists.newArrayList();

    @Override
    public ScheduledFuture<?> schedule(ProjectRunnable runnable) {
      scheduledItems.add(runnable);
      return new ScheduledFuture<ProjectRunnable>() {

        @Override
        public long getDelay(TimeUnit arg0) {
          return 0;
        }

        @Override
        public int compareTo(Delayed arg0) {
          return 0;
        }

        @Override
        public boolean cancel(boolean arg0) {
          return false;
        }

        @Override
        public ProjectRunnable get() throws InterruptedException, ExecutionException {
          return null;
        }

        @Override
        public ProjectRunnable get(long arg0, TimeUnit arg1)
            throws InterruptedException, ExecutionException, TimeoutException {
          return null;
        }

        @Override
        public boolean isCancelled() {
          return false;
        }

        @Override
        public boolean isDone() {
          return false;
        }
      };
    }

    @Override
    public void shutDown() {}

    @Override
    public EiffelEventParsingExecutor get() {
      INSTANCE = new TestExecutor();
      return INSTANCE;
    }
  }

  public static class NoOpEventParser implements EiffelEventParser {

    public NoOpEventParser() {}

    @Override
    public void createAndScheduleSccFromPatchsetCreation(PatchsetCreationData data) {}

    @Override
    public void createAndScheduleSccFromBranch(String repoName, String branchRef) {}

    @Override
    public void createAndScheduleSccFromCommit(String repoName, String branchRef, String commit) {}

    @Override
    public void fillGapsForSccFromBranch(String repoName, String branchRef) {}

    @Override
    public void fillGapsForSccFromCommit(String repoName, String branchRef, String commit) {}

    @Override
    public void fillGapsForScsFromBranch(String repoName, String branchRef) {}

    @Override
    public void createAndScheduleMissingScssFromBranch(String repoName, String branchRef) {}

    @Override
    public void createAndScheduleMissingScss(
        SourceChangeEventKey scs,
        String commitSha1TransactionEnd,
        AccountInfo submitter,
        Long submittedAt) {}

    @Override
    public void createAndScheduleArtc(
        String repoName, String tagName, Long creationTime, boolean force) {}
  }

  public static class TestModule extends AbstractModule {
    @Override
    protected void configure() {
      bind(ParsingQueuePersistence.class);
      bind(EiffelEventParser.class).to(NoOpEventParser.class);
      bind(EiffelEventParsingExecutor.class).toProvider(TestExecutor.class);
    }
  }
}
