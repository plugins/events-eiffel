// Copyright (C) 2022 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package com.googlesource.gerrit.plugins.eventseiffel.parsing;

import static org.junit.Assert.assertEquals;

import com.google.gerrit.acceptance.PushOneCommit;
import com.google.gerrit.acceptance.TestPlugin;
import com.google.gerrit.acceptance.UseLocalDisk;
import com.googlesource.gerrit.plugins.eventseiffel.TestEventHub;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.ArtifactEventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.CompositionDefinedEventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType;
import java.util.Set;
import org.eclipse.jgit.lib.Constants;
import org.junit.Before;
import org.junit.Test;

@UseLocalDisk
@TestPlugin(
    name = "events-eiffel",
    sysModule =
        "com.googlesource.gerrit.plugins.eventseiffel.parsing.EiffelEventParsingQueueIT$TestModule")
public class EiffelEventParsingQueueIT extends EiffelEventParsingTest {

  private ParsingQueuePersistence queuePersistence;
  private EiffelEventParsingQueue parsingQueue;

  @Before
  public void setUp() throws Exception {
    queuePersistence = plugin.getSysInjector().getInstance(ParsingQueuePersistence.class);
    parsingQueue = plugin.getSysInjector().getInstance(EiffelEventParsingQueue.class);
    TestEventHub.EVENTS.clear();
  }

  @Test
  public void persistedScsFromBranchIsScheduledAndPushed() throws Exception {
    queuePersistence.persistQueue(
        Set.of(ParsingQueueTask.builder(EiffelEventType.SCS, project.get(), getHead()).build()));
    parsingQueue.init();
    assertCorrectEvent(0, eventForHead(EiffelEventType.SCC));
    assertCorrectEvent(1, eventForHead(EiffelEventType.SCS));
  }

  @Test
  public void persistedScsFromCommitIsScheduledAndPushed() throws Exception {
    setScsHandled();
    PushOneCommit.Result res = createChange();
    merge(res);
    queuePersistence.persistQueue(
        Set.of(
            ParsingQueueTask.builder(EiffelEventType.SCS, project.get(), "refs/heads/master")
                .commit(res.getCommit().name())
                .build()));
    parsingQueue.init();
    assertCorrectEvent(0, toSccKey(res));
    assertCorrectEvent(1, toScsKey(res));
  }

  @Test
  public void persistedSccFromCommitIsScheduledAndPushed() throws Exception {
    setScsHandled();
    PushOneCommit.Result res = createChange();
    queuePersistence.persistQueue(
        Set.of(
            ParsingQueueTask.builder(EiffelEventType.SCC, project.get(), "refs/heads/master")
                .commit(res.getCommit().name())
                .build()));
    parsingQueue.init();
    assertCorrectEvent(0, toSccKey(res));
  }

  @Test
  public void persistedSccFromBranchIsScheduledAndPushed() throws Exception {
    setScsHandled();
    PushOneCommit.Result res = createChange();
    merge(res);
    queuePersistence.persistQueue(
        Set.of(
            ParsingQueueTask.builder(EiffelEventType.SCS, project.get(), "refs/heads/master")
                .build()));
    parsingQueue.init();
    assertCorrectEvent(0, toSccKey(res));
  }

  @Test
  public void persistedArtcIsScheduledAndPushed() throws Exception {
    setScsHandled();
    String tag =
        createTagRef(getHead(repo(), "HEAD").getName(), true).substring(Constants.R_TAGS.length());
    ArtifactEventKey artc = ArtifactEventKey.create(tagPURL(project.get(), tag, "localhost"));
    CompositionDefinedEventKey cd =
        CompositionDefinedEventKey.create(tagCompositionName(project.get(), "localhost"), tag);
    queuePersistence.persistQueue(
        Set.of(
            ParsingQueueTask.builder(EiffelEventType.ARTC, project.get(), tag)
                .updateTime(EPOCH_MILLIS)
                .force(false)
                .build()));
    parsingQueue.init();
    assertEquals(2, TestEventHub.EVENTS.size());
    assertCorrectEvent(0, cd);
    assertCorrectEvent(1, artc);
  }

  @Test
  public void failedTaskIsRescheduled() throws Exception {
    setScsHandled();
    String tagName = "a-tag";
    ArtifactEventKey artc = ArtifactEventKey.create(tagPURL(project.get(), tagName, "localhost"));
    CompositionDefinedEventKey cd =
        CompositionDefinedEventKey.create(tagCompositionName(project.get(), "localhost"), tagName);

    /* Scheduling ArtC creation for a non-existing tag fails. */
    parsingQueue.schedule(
        ParsingQueueTask.builder(EiffelEventType.ARTC, project.get(), tagName)
            .updateTime(EPOCH_MILLIS)
            .force(false));
    assertEquals(0, TestEventHub.EVENTS.size());

    /* Creating the tag should mean that rescheduling the failed task should work. */
    createTagRef(tagName, getHead(repo(), "HEAD").getName(), true);
    parsingQueue.rescheduleFailed();
    assertEquals(2, TestEventHub.EVENTS.size());
    assertCorrectEvent(0, cd);
    assertCorrectEvent(1, artc);
  }

  public static class TestModule extends ParsingTestModule {

    @Override
    protected void configure() {
      bind(EiffelEventParsingExecutor.class).to(EiffelEventParsingExecutor.Direct.class);
      super.configure();
    }
  }
}
