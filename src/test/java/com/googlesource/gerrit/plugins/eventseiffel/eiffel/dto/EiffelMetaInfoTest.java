// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto;

import static com.google.common.truth.Truth.assertThat;
import static com.google.gerrit.testing.GerritJUnit.assertThrows;
import static org.junit.Assert.assertEquals;

import com.google.gson.Gson;
import java.util.UUID;
import org.junit.Test;

public class EiffelMetaInfoTest {

  @Test
  public void isSerializable() {
    String serialized =
        new Gson()
            .toJson(
                EiffelMetaInfo.builder()
                    .addTag("a-tag")
                    .version("3.0.0")
                    .id(UUID.fromString("ffffffff-aaaa-cccc-eeee-dddddddddddd"))
                    .time(123456)
                    .type(EiffelEventType.SCC)
                    .source(EiffelSourceInfo.builder().domainId("domain").host("host"))
                    .security(
                        EiffelSecurityInfo.builder()
                            .authorIdentity("ident")
                            .integrityProtection(EiffelIntegrityProtectionInfo.builder().alg("alg"))
                            .addSequenceProtection(
                                EiffelSequenceProtectionInfo.builder()
                                    .sequenceName("a-sequence")
                                    .position(4)))
                    .build());

    String expected =
        "{\"type\":\"EiffelSourceChangeCreatedEvent\",\"version\":\"3.0.0\",\"tags\":[\"a-tag\"],\"source\":{\"domainId\":\"domain\",\"host\":\"host\"},\"security\":{\"authorIdentity\":\"ident\",\"integrityProtection\":{\"alg\":\"alg\",\"signature\":\"\"},\"sequenceProtection\":[{\"sequenceName\":\"a-sequence\",\"position\":4}]},\"time\":123456,\"id\":\"ffffffff-aaaa-cccc-eeee-dddddddddddd\"}";
    assertEquals("Matches json", expected, serialized);
  }

  @Test
  public void isDeSerializable() {
    String json =
        "{\"id\":\"ffffffff-aaaa-cccc-eeee-dddddddddddd\",\"time\":123456,\"type\":\"EiffelSourceChangeCreatedEvent\",\"version\":\"3.0.0\",\"tags\":[\"a-tag\"],\"source\":{\"domainId\":\"domain-id\",\"host\":\"host\"},\"security\":{\"authorIdentity\":\"ident\",\"integrityProtection\":{\"alg\":\"alg\",\"signature\":\"signature\"},\"sequenceProtection\":[{\"sequenceName\":\"a-sequence\",\"position\":4}]}}";
    EiffelMetaInfo parsed = new Gson().fromJson(json, EiffelMetaInfo.class);

    assertEquals(UUID.fromString("ffffffff-aaaa-cccc-eeee-dddddddddddd"), parsed.id);
    assertEquals(Long.valueOf(123456), parsed.time);
    assertEquals(EiffelEventType.SCC, parsed.type);
    assertEquals("3.0.0", parsed.version);
    assertEquals("a-tag", parsed.tags[0]);
    assertEquals("domain-id", parsed.source.domainId);
    assertEquals("host", parsed.source.host);
    assertEquals("ident", parsed.security.authorIdentity);
    assertEquals("alg", parsed.security.integrityProtection.alg);
    assertEquals("signature", parsed.security.integrityProtection.signature);
    assertEquals("a-sequence", parsed.security.sequenceProtection[0].sequenceName);
    assertEquals(Integer.valueOf(4), parsed.security.sequenceProtection[0].position);
  }

  @Test
  public void typeIsRequired() {
    IllegalStateException thrown =
        assertThrows(
            IllegalStateException.class,
            () ->
                EiffelMetaInfo.builder()
                    .addTag("a-tag")
                    .version("3.0.0")
                    .source(EiffelSourceInfo.builder().domainId("domain").host("host"))
                    .build());
    assertThat(thrown).hasMessageThat().isEqualTo("type is required");
  }

  @Test
  public void versionIsRequired() {
    IllegalStateException thrown =
        assertThrows(
            IllegalStateException.class,
            () ->
                EiffelMetaInfo.builder()
                    .addTag("a-tag")
                    .type(EiffelEventType.SCC)
                    .source(EiffelSourceInfo.builder().domainId("domain").host("host"))
                    .build());
    assertThat(thrown).hasMessageThat().isEqualTo("version is required");
  }
}
