// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto;

import static com.google.common.truth.Truth.assertThat;
import static com.google.gerrit.testing.GerritJUnit.assertThrows;
import static org.junit.Assert.assertEquals;

import com.google.gson.Gson;
import org.junit.Test;

public class EiffelSecurityInfoTest {

  @Test
  public void authorIdentityIsRequired() {
    IllegalStateException thrown =
        assertThrows(
            IllegalStateException.class,
            () ->
                EiffelSecurityInfo.builder()
                    .integrityProtection(EiffelIntegrityProtectionInfo.builder().alg("alg"))
                    .addSequenceProtection(
                        EiffelSequenceProtectionInfo.builder()
                            .sequenceName("sequence-name")
                            .position(5))
                    .build());
    assertThat(thrown).hasMessageThat().isEqualTo("authorIdentity is required");
  }

  @Test
  public void isSerializable() {
    String serialized =
        new Gson()
            .toJson(
                EiffelSecurityInfo.builder()
                    .authorIdentity("author-identity")
                    .integrityProtection(EiffelIntegrityProtectionInfo.builder().alg("alg"))
                    .addSequenceProtection(
                        EiffelSequenceProtectionInfo.builder()
                            .sequenceName("sequence-name-1")
                            .position(5))
                    .addSequenceProtection(
                        EiffelSequenceProtectionInfo.builder()
                            .sequenceName("sequence-name-2")
                            .position(10))
                    .build());
    System.out.println(serialized);
    String expected =
        "{\"authorIdentity\":\"author-identity\",\"integrityProtection\":{\"alg\":\"alg\",\"signature\":\"\"},\"sequenceProtection\":[{\"sequenceName\":\"sequence-name-1\",\"position\":5},{\"sequenceName\":\"sequence-name-2\",\"position\":10}]}";
    assertEquals("Json matches", expected, serialized);
  }

  @Test
  public void isDeSerializable() {
    String json =
        "{\"authorIdentity\":\"author-identity\",\"integrityProtection\":{\"alg\":\"alg\",\"signature\":\"\"},\"sequenceProtection\":[{\"sequenceName\":\"sequence-name-1\",\"position\":5},{\"sequenceName\":\"sequence-name-2\",\"position\":10}]}";
    EiffelSecurityInfo parsed = new Gson().fromJson(json, EiffelSecurityInfo.class);
    assertEquals("author-identity", parsed.authorIdentity);
    assertEquals("alg", parsed.integrityProtection.alg);
    assertEquals("sequence-name-1", parsed.sequenceProtection[0].sequenceName);
    assertEquals(Integer.valueOf(5), parsed.sequenceProtection[0].position);
    assertEquals("sequence-name-2", parsed.sequenceProtection[1].sequenceName);
    assertEquals(Integer.valueOf(10), parsed.sequenceProtection[1].position);
  }
}
