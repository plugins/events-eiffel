package com.googlesource.gerrit.plugins.eventseiffel.parsing;

import static org.junit.Assert.assertEquals;

import com.googlesource.gerrit.plugins.eventseiffel.EiffelEventsTest;
import com.googlesource.gerrit.plugins.eventseiffel.TestEventHub;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.EventKey;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import org.junit.Ignore;

@Ignore
public class EiffelEventParsingTest extends EiffelEventsTest {
  protected static final Long EPOCH_MILLIS = 978307261000l;

  protected static String tagCompositionName(String projectName) {
    return tagCompositionName(projectName, "localhost");
  }

  protected static String tagCompositionName(String projectName, String namespace) {
    return "scmtag/git/" + namespace + "/" + URLEncoder.encode(projectName, StandardCharsets.UTF_8);
  }

  protected static String tagPURL(String projectName, String tagName) {
    return tagPURL(projectName, tagName, "localhost");
  }

  protected static String tagPURL(String projectName, String tagName, String namespace) {
    return String.format(
        TAG_PURL_TEMPLATE,
        namespace,
        projectName,
        tagName,
        String.format("ssh://%s/%s", HOST_NAME, projectName),
        tagName);
  }

  protected void assertCorrectEvent(int order, EventKey expected) {
    EventKey actual = EventKey.fromEvent(TestEventHub.EVENTS.get(order));
    assertEquals(expected, actual);
  }
}
