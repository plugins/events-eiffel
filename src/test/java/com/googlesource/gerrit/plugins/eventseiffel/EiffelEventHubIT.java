// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel;

import static com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType.SCC;
import static com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType.SCS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.google.gerrit.acceptance.LightweightPluginDaemonTest;
import com.google.gerrit.acceptance.TestPlugin;
import com.google.inject.AbstractModule;
import com.googlesource.gerrit.plugins.eventseiffel.cache.EiffelEventIdCache;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.EventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEvent;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelSourceChangeCreatedEventInfo;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelSourceChangeSubmittedEventInfo;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

@TestPlugin(
    name = "events-eiffel",
    sysModule = "com.googlesource.gerrit.plugins.eventseiffel.EiffelEventHubIT$TestModule")
public class EiffelEventHubIT extends LightweightPluginDaemonTest {

  private EiffelEventHub hub;
  private EiffelEventIdCache idCache;
  private TestEventPublisher publisher;

  @Before
  public void before() {
    hub = plugin.getSysInjector().getInstance(EiffelEventHub.class);
    idCache = plugin.getSysInjector().getInstance(EiffelEventIdCache.class);
    publisher = plugin.getSysInjector().getInstance(TestEventPublisher.class);
  }

  @After
  public void cleanUp() {
    publisher.stop();
    TestEventStorage.INSTANCE.reset();
  }

  @Test
  public void eventIsPassedThroughHub() throws Exception {
    EiffelEvent toPublish = TestUtil.newScc();
    hub.push(toPublish, false);
    EiffelEvent published = publisher.getPublished(EventKey.fromEvent(toPublish));
    assertNotNull(published);
    assertEquals(toPublish.meta.id, published.meta.id);
  }

  @Test
  public void getExisting() throws Exception {
    EiffelEvent toCheck = TestUtil.newScc();
    EventKey key = EventKey.fromEvent(toCheck);
    assertFalse(hub.getExistingId(key).isPresent());
    publisher.stopPublishing();
    hub.push(toCheck, false);
    assertTrue(hub.getExistingId(key).isPresent());
    publisher.startPublishing();
    assertTrue(hub.getExistingId(key).isPresent());
  }

  @Test
  public void getScsForCommitNoBranch() throws Exception {
    EiffelSourceChangeSubmittedEventInfo toCheck =
        (EiffelSourceChangeSubmittedEventInfo) TestUtil.copyGitIdentifier(TestUtil.newScc(), SCS);
    EventKey key = EventKey.fromEvent(toCheck);
    String repo = toCheck.data.gitIdentifier.repoName;
    String commit = toCheck.data.gitIdentifier.commitId;
    assertFalse(hub.getScsForCommit(repo, commit, List.of()).isPresent());
    assertEquals(1, TestEventStorage.INSTANCE.queriesForScsIds(repo, commit));
    publisher.stopPublishing();
    hub.push(toCheck, false);
    assertFalse(hub.getScsForCommit(repo, commit, List.of()).isPresent());
    assertEquals(2, TestEventStorage.INSTANCE.queriesForScsIds(repo, commit));
    publisher.startPublishing();
    assertTrue(hub.getScsForCommit(repo, commit, List.of()).isPresent());
    assertEquals(3, TestEventStorage.INSTANCE.queriesForScsIds(repo, commit));
  }

  @Test
  public void getScsForCommitWithBranch() throws Exception {
    EiffelSourceChangeSubmittedEventInfo toCheck =
        (EiffelSourceChangeSubmittedEventInfo) TestUtil.copyGitIdentifier(TestUtil.newScc(), SCS);
    EventKey key = EventKey.fromEvent(toCheck);
    String repo = toCheck.data.gitIdentifier.repoName;
    String commit = toCheck.data.gitIdentifier.commitId;
    String branch = toCheck.data.gitIdentifier.branch;
    assertFalse(hub.getScsForCommit(repo, commit, List.of(branch)).isPresent());
    assertEquals(1, TestEventStorage.INSTANCE.queriesForScsIds(repo, commit));
    publisher.stopPublishing();
    hub.push(toCheck, false);
    assertTrue(hub.getScsForCommit(repo, commit, List.of(branch)).isPresent());
    assertEquals(1, TestEventStorage.INSTANCE.queriesForScsIds(repo, commit));
    publisher.startPublishing();
    assertTrue(hub.getScsForCommit(repo, commit, List.of(branch)).isPresent());
    assertEquals(1, TestEventStorage.INSTANCE.queriesForScsIds(repo, commit));
  }

  @Test
  public void eventsArePassedThroughHubInOrder() throws Exception {
    List<EiffelSourceChangeCreatedEventInfo> toPublish = TestUtil.newSccs(5);
    publisher.stopPublishing();
    for (EiffelEvent event : toPublish) {
      hub.push(event, false);
    }
    publisher.startPublishing();
    publisher.assertOrder(toPublish);
  }

  @Test
  public void eventDoesNotReplaceAlreadyExistingWithSameKey() throws Exception {
    EiffelSourceChangeCreatedEventInfo event = TestUtil.newScc();
    EiffelEvent sccCopy = TestUtil.copyGitIdentifier(event, SCC);
    assertEquals(EventKey.fromEvent(event), EventKey.fromEvent(sccCopy));
    hub.push(event, false);
    hub.push(sccCopy, false);
    EiffelEvent published = publisher.getPublished(EventKey.fromEvent(event));
    assertNotEquals(sccCopy.meta.id, published.meta.id);
    assertEquals(event.meta.id, published.meta.id);
  }

  @Test
  public void eventDoesNotReplaceAlreadyExistingWithSameKey_publisherDown() throws Exception {
    EiffelSourceChangeCreatedEventInfo event = TestUtil.newScc();
    EiffelEvent sccCopy = TestUtil.copyGitIdentifier(event, SCC);
    assertEquals(EventKey.fromEvent(event), EventKey.fromEvent(sccCopy));
    publisher.stopPublishing();
    hub.push(event, false);
    hub.push(sccCopy, false);
    publisher.startPublishing();
    EiffelEvent published = publisher.getPublished(EventKey.fromEvent(event));
    assertNotEquals(sccCopy.meta.id, published.meta.id);
    assertEquals(event.meta.id, published.meta.id);
  }

  @Test
  public void eventDoesNotReplaceAlreadyExistingWithDifferentType() throws Exception {
    EiffelSourceChangeCreatedEventInfo event = TestUtil.newScc();
    EiffelEvent scsCopy = TestUtil.copyGitIdentifier(event, SCS);
    hub.push(event, false);
    hub.push(scsCopy, false);
    assertNotEquals(scsCopy.meta.id, event.meta.id);
    assertEquals(event.meta.id, publisher.getPublished(EventKey.fromEvent(event)).meta.id);
    assertEquals(scsCopy.meta.id, publisher.getPublished(EventKey.fromEvent(scsCopy)).meta.id);
  }

  @Test
  public void eventDoesNotReplaceAlreadyExistingWithDifferentType_publisherDown() throws Exception {
    EiffelSourceChangeCreatedEventInfo event = TestUtil.newScc();
    EiffelEvent scsCopy = TestUtil.copyGitIdentifier(event, SCS);
    publisher.stopPublishing();
    hub.push(event, false);
    hub.push(scsCopy, false);
    publisher.startPublishing();
    assertNotEquals(scsCopy.meta.id, event.meta.id);
    assertEquals(event.meta.id, publisher.getPublished(EventKey.fromEvent(event)).meta.id);
    assertEquals(scsCopy.meta.id, publisher.getPublished(EventKey.fromEvent(scsCopy)).meta.id);
  }

  @Test
  public void idCacheIsUpdatedUponAck() throws Exception {
    publisher.stopPublishing();
    EiffelSourceChangeCreatedEventInfo event = TestUtil.newScc();
    hub.push(event, false);
    EventKey key = EventKey.fromEvent(event);
    assertEquals(event.meta.id, hub.getExistingId(key).get());
    assertTrue(idCache.getEventId(key).isEmpty());
    publisher.startPublishing();
    EiffelEvent taken = publisher.getPublished(key);
    assertNotNull(taken);
    assertEquals(EventKey.fromEvent(taken), key);
    assertTrue(idCache.getEventId(key).isPresent());
    assertEquals(event.meta.id, idCache.getEventId(key).get());
    assertEquals(event.meta.id, hub.getExistingId(key).get());
  }

  @Test
  public void getExistingIdFallsBackToIdCache() throws Exception {
    EiffelSourceChangeCreatedEventInfo event = TestUtil.newScc();
    idCache.putId(event);
    EventKey key = EventKey.fromEvent(event);
    assertTrue(idCache.getEventId(key).isPresent());
    assertTrue(hub.getExistingId(key).isPresent());
    assertEquals(idCache.getEventId(key), hub.getExistingId(key));
  }

  public static class TestModule extends AbstractModule {
    @Override
    protected void configure() {
      install(new EiffelEventsTestModule());
    }
  }
}
