// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto;

import static org.junit.Assert.assertEquals;

import com.google.gson.Gson;
import java.util.UUID;
import org.junit.Test;

public class EiffelSourceChangeSubmittedEventInfoTest {

  @Test
  public void isSerializable() {
    String serialized =
        new Gson()
            .toJson(
                EiffelSourceChangeSubmittedEventInfo.builder()
                    .submitter(
                        EiffelPersonInfo.builder().name("Author Name").email("author@email.com"))
                    .gitIdentifier(
                        EiffelGitIdentifierInfo.builder()
                            .commitId("1103c14f6a25e124cde1bc00abc0aaf86c223aad")
                            .repoUri("my/repo"))
                    .meta(
                        EiffelMetaInfo.builder()
                            .addTag("a-tag")
                            .id(UUID.fromString("ffffffff-aaaa-cccc-eeee-dddddddddddd"))
                            .time(123456))
                    .addLink(
                        EiffelLinkInfo.builder()
                            .type(EiffelLinkType.PREVIOUS_VERSION)
                            .target(UUID.fromString("aaaaaaaa-bbbb-cccc-eeee-ffffffffffff")))
                    .build());

    String expected =
        "{\"data\":{\"gitIdentifier\":{\"commitId\":\"1103c14f6a25e124cde1bc00abc0aaf86c223aad\",\"repoUri\":\"my/repo\"},\"submitter\":{\"name\":\"Author Name\",\"email\":\"author@email.com\"}},\"meta\":{\"type\":\"EiffelSourceChangeSubmittedEvent\",\"version\":\"3.0.0\",\"tags\":[\"a-tag\"],\"time\":123456,\"id\":\"ffffffff-aaaa-cccc-eeee-dddddddddddd\"},\"links\":[{\"type\":\"PREVIOUS_VERSION\",\"target\":\"aaaaaaaa-bbbb-cccc-eeee-ffffffffffff\"}]}";
    assertEquals("Mathches json", expected, serialized);
  }

  @Test
  public void testSimpleJson() {
    String json =
        "{\"meta\":{\"type\":\"EiffelSourceChangeSubmittedEvent\",\"version\":\"3.0.0\",\"time\":1234567890,\"id\":\"aaaaaaaa-bbbb-5ccc-8ddd-eeeeeeeeeee0\"},\"data\":{\"gitIdentifier\":{\"commitId\":\"1103c14f6a25e124cde1bc00abc0aaf86c223aad\",\"repoUri\":\"my/repo\"},\"submitter\":{\"name\":\"Jane Doe\",\"email\":\"jane.doe@company.com\",\"id\":\"j_doe\",\"group\":\"Team Wombats\"}},\"links\":[{\"type\":\"CHANGE\",\"target\":\"aaaaaaaa-bbbb-5ccc-8ddd-eeeeeeeeeee1\"},{\"type\":\"PREVIOUS_VERSION\",\"target\":\"aaaaaaaa-bbbb-5ccc-8ddd-eeeeeeeeeee2\"}]}";
    EiffelSourceChangeSubmittedEventInfo parsed =
        new Gson().fromJson(json, EiffelSourceChangeSubmittedEventInfo.class);
    assertEquals(EiffelEventType.SCS, parsed.meta.type);
    assertEquals("3.0.0", parsed.meta.version);
    assertEquals(Long.valueOf(1234567890), parsed.meta.time);
    assertEquals(UUID.fromString("aaaaaaaa-bbbb-5ccc-8ddd-eeeeeeeeeee0"), parsed.meta.id);
    assertEquals("1103c14f6a25e124cde1bc00abc0aaf86c223aad", parsed.data.gitIdentifier.commitId);
    assertEquals("my/repo", parsed.data.gitIdentifier.repoUri);
    assertEquals("Jane Doe", parsed.data.submitter.name);
    assertEquals("jane.doe@company.com", parsed.data.submitter.email);
    assertEquals("j_doe", parsed.data.submitter.id);
    assertEquals("Team Wombats", parsed.data.submitter.group);
    assertEquals(EiffelLinkType.CHANGE, parsed.links[0].type);
    assertEquals(UUID.fromString("aaaaaaaa-bbbb-5ccc-8ddd-eeeeeeeeeee1"), parsed.links[0].target);
    assertEquals(EiffelLinkType.PREVIOUS_VERSION, parsed.links[1].type);
    assertEquals(UUID.fromString("aaaaaaaa-bbbb-5ccc-8ddd-eeeeeeeeeee2"), parsed.links[1].target);
  }
}
