// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto;

import static com.google.common.truth.Truth.assertThat;
import static com.google.gerrit.testing.GerritJUnit.assertThrows;
import static org.junit.Assert.assertEquals;

import com.google.gson.Gson;
import org.junit.Test;

public class EiffelGitIdentifierInfoTest {

  @Test
  public void isSerializable() {
    String serialized =
        new Gson()
            .toJson(
                EiffelGitIdentifierInfo.builder()
                    .branch("master")
                    .repoName("repo")
                    .commitId("1103c14f6a25e124cde1bc00abc0aaf86c223aad")
                    .repoUri("my/repo")
                    .build());

    String expected =
        "{\"commitId\":\"1103c14f6a25e124cde1bc00abc0aaf86c223aad\",\"branch\":\"master\",\"repoName\":\"repo\",\"repoUri\":\"my/repo\"}";

    assertEquals("Matches json", expected, serialized);
  }

  @Test
  public void isDeSerializable() {
    String json =
        "{\"commitId\":\"1103c14f6a25e124cde1bc00abc0aaf86c223aad\",\"branch\":\"master\",\"repoName\":\"repo\",\"repoUri\":\"my/repo\"}";
    EiffelGitIdentifierInfo parsed = new Gson().fromJson(json, EiffelGitIdentifierInfo.class);
    assertEquals("1103c14f6a25e124cde1bc00abc0aaf86c223aad", parsed.commitId);
    assertEquals("master", parsed.branch);
    assertEquals("repo", parsed.repoName);
    assertEquals("my/repo", parsed.repoUri);
  }

  @Test
  public void commitIdIsRequired() {
    IllegalStateException thrown =
        assertThrows(
            IllegalStateException.class,
            () ->
                EiffelGitIdentifierInfo.builder()
                    .branch("master")
                    .repoName("repo")
                    .repoUri("my/repo")
                    .build());
    assertThat(thrown).hasMessageThat().isEqualTo("commitId is required");
  }

  @Test
  public void repoUriIsRequired() {
    IllegalStateException thrown =
        assertThrows(
            IllegalStateException.class,
            () ->
                EiffelGitIdentifierInfo.builder()
                    .branch("master")
                    .repoName("repo")
                    .commitId("103c14f6a25e124cde1bc00abc0aaf86c223aadf")
                    .build());
    assertThat(thrown).hasMessageThat().isEqualTo("repoUri is required");
  }

  @Test
  public void commitIdMustBeValidSha1() {
    /* not 40 chars*/
    IllegalStateException thrown =
        assertThrows(
            IllegalStateException.class,
            () ->
                EiffelGitIdentifierInfo.builder()
                    .branch("master")
                    .repoName("repo")
                    .commitId("103c14f6a25e124cde1bc00abc0aaf86c223aad")
                    .repoUri("my/repo")
                    .build());
    assertThat(thrown).hasMessageThat().isEqualTo("commitId must be a valid SHA1");

    /* Illegal character */
    thrown =
        assertThrows(
            IllegalStateException.class,
            () ->
                EiffelGitIdentifierInfo.builder()
                    .branch("master")
                    .repoName("repo")
                    .commitId("103c14f6a25e124cde1bc00abc0aaf86c223aag")
                    .repoUri("my/repo")
                    .build());
    assertThat(thrown).hasMessageThat().isEqualTo("commitId must be a valid SHA1");
  }
}
