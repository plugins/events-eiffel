// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto;

import static com.google.common.truth.Truth.assertThat;
import static com.google.gerrit.testing.GerritJUnit.assertThrows;
import static org.junit.Assert.assertEquals;

import com.google.gson.Gson;
import org.junit.Test;

public class EiffelSequenceProtectionInfoTest {
  @Test
  public void isSerializable() {
    String serialized =
        new Gson()
            .toJson(
                EiffelSequenceProtectionInfo.builder()
                    .sequenceName("sequence-name")
                    .position(4)
                    .build());
    String expected = "{\"sequenceName\":\"sequence-name\",\"position\":4}";
    assertEquals("Json matches", expected, serialized);
  }

  @Test
  public void isDeSerializable() {
    String json = "{\"sequenceName\":\"sequence-name\",\"position\":4}";
    EiffelSequenceProtectionInfo parsed =
        new Gson().fromJson(json, EiffelSequenceProtectionInfo.class);
    assertEquals(Integer.valueOf(4), parsed.position);
    assertEquals("sequence-name", parsed.sequenceName);
  }

  @Test
  public void sequenceNameIsRequired() {
    IllegalStateException thrown =
        assertThrows(
            IllegalStateException.class,
            () -> EiffelSequenceProtectionInfo.builder().position(5).build());
    assertThat(thrown).hasMessageThat().isEqualTo("sequenceName is required");
  }

  @Test
  public void positionIsRequired() {
    IllegalStateException thrown =
        assertThrows(
            IllegalStateException.class,
            () -> EiffelSequenceProtectionInfo.builder().sequenceName("sequence-name").build());
    assertThat(thrown).hasMessageThat().isEqualTo("position is required");
  }

  @Test
  public void positionRequiredLargerThanZero() {
    IllegalStateException thrown =
        assertThrows(
            IllegalStateException.class,
            () ->
                EiffelSequenceProtectionInfo.builder()
                    .sequenceName("sequence-name")
                    .position(0)
                    .build());
    assertThat(thrown).hasMessageThat().isEqualTo("position must be larger than zero");
  }
}
