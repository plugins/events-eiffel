// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto;

import static org.junit.Assert.assertEquals;

import com.google.gson.Gson;
import org.junit.Test;

public class EiffelPersonInfoTest {
  @Test
  public void emptyObjectAllowed() {
    String serialized = new Gson().toJson(EiffelPersonInfo.builder().build());
    String expected = "{}";
    assertEquals("Matches json", expected, serialized);
  }

  @Test
  public void isSerializable() {
    String serialized =
        new Gson()
            .toJson(
                EiffelPersonInfo.builder()
                    .name("Person Name")
                    .email("name@person.com")
                    .id("person-id")
                    .group("person-group")
                    .build());
    String expected =
        "{\"name\":\"Person Name\",\"email\":\"name@person.com\",\"id\":\"person-id\",\"group\":\"person-group\"}";
    assertEquals("Matches json", expected, serialized);
  }

  @Test
  public void isDeSerializable() {
    String json =
        "{\"name\":\"Person Name\",\"email\":\"name@person.com\",\"id\":\"person-id\",\"group\":\"person-group\"}";
    EiffelPersonInfo parsed = new Gson().fromJson(json, EiffelPersonInfo.class);
    assertEquals("name@person.com", parsed.email);
    assertEquals("Person Name", parsed.name);
    assertEquals("person-id", parsed.id);
    assertEquals("person-group", parsed.group);
  }
}
