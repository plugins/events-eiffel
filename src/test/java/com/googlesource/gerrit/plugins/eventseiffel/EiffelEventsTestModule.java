// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel;

import com.google.gerrit.extensions.events.LifecycleListener;
import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import com.google.inject.Singleton;
import com.google.inject.internal.UniqueAnnotations;
import com.googlesource.gerrit.plugins.eventseiffel.EiffelEventsTest.TestEventFactoryProvider;
import com.googlesource.gerrit.plugins.eventseiffel.EiffelEventsTest.TestEventsFilterProvider;
import com.googlesource.gerrit.plugins.eventseiffel.cache.EiffelEventIdCacheImpl;
import com.googlesource.gerrit.plugins.eventseiffel.config.EventIdCacheConfig;
import com.googlesource.gerrit.plugins.eventseiffel.config.EventMappingConfig;
import com.googlesource.gerrit.plugins.eventseiffel.config.EventsFilter;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.api.EventStorage;
import com.googlesource.gerrit.plugins.eventseiffel.mapping.EiffelEventFactory;
import com.googlesource.gerrit.plugins.eventseiffel.parsing.EiffelEventParser;
import com.googlesource.gerrit.plugins.eventseiffel.parsing.EiffelEventParserImpl;
import com.googlesource.gerrit.plugins.eventseiffel.parsing.EiffelEventParsingExecutor;
import com.googlesource.gerrit.plugins.eventseiffel.parsing.EiffelEventParsingQueue;
import org.junit.Ignore;

@Ignore
public class EiffelEventsTestModule extends AbstractModule {
  @Override
  public void configure() {
    bind(EventStorage.class).toProvider(TestEventStorage.Provider.class).in(Singleton.class);
    bind(EventIdCacheConfig.class).toProvider(EventIdCacheConfig.Provider.class);
    bind(EventMappingConfig.class).toProvider(EventMappingConfig.Provider.class);
    install(EiffelEventIdCacheImpl.module());
    bind(EiffelEventHubImpl.class).in(Scopes.SINGLETON);
    bind(EiffelEventHub.class).to(EiffelEventHubImpl.class);
    bind(EiffelEventParsingExecutor.class).to(EiffelEventParsingExecutor.Direct.class);
    bind(TestEventPublisher.class).in(Scopes.SINGLETON);
    bind(EiffelEventHub.Consumer.class).to(TestEventPublisher.class);

    bind(EventsFilter.class).toProvider(TestEventsFilterProvider.class);
    bind(EiffelEventParser.class).to(EiffelEventParserImpl.class);
    bind(EiffelEventParsingQueue.class);
    bind(EiffelEventFactory.class).toProvider(TestEventFactoryProvider.class);

    bind(Manager.class).in(Scopes.SINGLETON);
    bind(LifecycleListener.class).annotatedWith(UniqueAnnotations.create()).to(Manager.class);
  }
}
