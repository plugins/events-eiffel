// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.cache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.googlesource.gerrit.plugins.eventseiffel.eiffel.ArtifactEventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.CompositionDefinedEventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.EventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.SourceChangeEventKey;
import java.util.Optional;
import java.util.UUID;
import org.junit.Test;

public class IdCacheSerializerTest {

  @Test
  public void sccEventKeyIsSerializedCorrectly() throws Exception {
    eventKeyIsSerializedCorrectly(SourceChangeEventKey.sccKey("repo-name", "branch", "commitId"));
  }

  @Test
  public void scsEventKeyIsSerializedCorrectly() throws Exception {
    eventKeyIsSerializedCorrectly(SourceChangeEventKey.scsKey("repo-name", "branch", "commitId"));
  }

  @Test
  public void artcEventKeyIsSerializedCorrectly() throws Exception {
    eventKeyIsSerializedCorrectly(ArtifactEventKey.create("identity"));
  }

  @Test
  public void cdEventKeyIsSerializedCorrectly() throws Exception {
    eventKeyIsSerializedCorrectly(CompositionDefinedEventKey.create("name", "version"));
  }

  @Test
  public void uuidIsSerializedCorrectly() throws Exception {
    UUIDSerializer uuidSerializer = new UUIDSerializer();
    Optional<UUID> actual = Optional.of(UUID.randomUUID());
    byte[] serialized = uuidSerializer.serialize(actual);
    Optional<UUID> deserialized = uuidSerializer.deserialize(serialized);
    assertEquals(actual, deserialized);
  }

  @Test
  public void missingUuidSerializedCorrectly() throws Exception {
    UUIDSerializer uuidSerializer = new UUIDSerializer();
    byte[] serialized = uuidSerializer.serialize(Optional.empty());
    Optional<UUID> deserialized = uuidSerializer.deserialize(serialized);
    assertTrue(deserialized.isEmpty());
  }

  private void eventKeyIsSerializedCorrectly(EventKey actual) throws Exception {
    EventKeySerializer keySerializer = new EventKeySerializer();
    byte[] serialized = keySerializer.serialize(actual);
    EventKey deserialized = keySerializer.deserialize(serialized);
    assertEquals(actual, deserialized);
  }
}
