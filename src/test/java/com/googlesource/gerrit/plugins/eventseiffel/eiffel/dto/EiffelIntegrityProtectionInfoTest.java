// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto;

import static org.junit.Assert.assertEquals;

import com.google.gson.Gson;
import org.junit.Test;

public class EiffelIntegrityProtectionInfoTest {
  @Test
  public void signatureAlwaysEmptyString() {
    String serializedInfo = new Gson().toJson(EiffelIntegrityProtectionInfo.builder().build());
    String expected = "{\"signature\":\"\"}";
    assertEquals("Matches json", expected, serializedInfo);
  }

  @Test
  public void isSerializable() {
    String serializedInfo =
        new Gson()
            .toJson(EiffelIntegrityProtectionInfo.builder().alg("alg").pulicKey("pub-key").build());
    String expected = "{\"alg\":\"alg\",\"signature\":\"\",\"publicKey\":\"pub-key\"}";
    assertEquals("Matches json", expected, serializedInfo);
  }

  @Test
  public void isDeSerializable() {
    String json = "{\"alg\":\"alg\",\"signature\":\"\",\"publicKey\":\"pub-key\"}";
    EiffelIntegrityProtectionInfo parsedInfo =
        new Gson().fromJson(json, EiffelIntegrityProtectionInfo.class);
    assertEquals("alg", parsedInfo.alg);
    assertEquals("pub-key", parsedInfo.publicKey);
    assertEquals("", parsedInfo.signature);
  }
}
