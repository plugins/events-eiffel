// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.listeners;

import static com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType.SCC;
import static com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType.SCS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.google.common.collect.Lists;
import com.google.gerrit.acceptance.PushOneCommit;
import com.google.gerrit.acceptance.TestPlugin;
import com.google.gerrit.entities.RefNames;
import com.google.gerrit.extensions.api.projects.BranchInfo;
import com.google.gerrit.extensions.api.projects.BranchInput;
import com.google.gerrit.extensions.events.GitReferenceUpdatedListener;
import com.google.gerrit.extensions.events.RevisionCreatedListener;
import com.google.gerrit.extensions.registration.DynamicSet;
import com.google.inject.AbstractModule;
import com.google.inject.Provider;
import com.google.inject.Scopes;
import com.googlesource.gerrit.plugins.eventseiffel.EiffelEventsTest;
import com.googlesource.gerrit.plugins.eventseiffel.EiffelEventsTestModule;
import com.googlesource.gerrit.plugins.eventseiffel.TestEventPublisher;
import com.googlesource.gerrit.plugins.eventseiffel.config.EiffelConfig;
import com.googlesource.gerrit.plugins.eventseiffel.config.EventListenersConfig;
import com.googlesource.gerrit.plugins.eventseiffel.config.EventsFilter;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.ArtifactEventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.CompositionDefinedEventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.EventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.SourceChangeEventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEvent;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType;
import java.util.UUID;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.transport.RefSpec;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

@TestPlugin(
    name = "events-eiffel",
    sysModule =
        "com.googlesource.gerrit.plugins.eventseiffel.listeners.GerritEventListenersIT$TestModule")
public class GerritEventListenersIT extends EiffelEventsTest {
  private TestEventPublisher publisher;

  @Before
  public void before() {
    publisher = plugin.getSysInjector().getInstance(TestEventPublisher.class);
    TestEventsFilterProvider.reset();
    TestEventListenersConfigProvider.reset();
  }

  @After
  public void cleanup() {
    if (publisher != null) {
      publisher.stop();
    }
  }

  @Test
  public void patchSetCreatedResultsInEvent() throws Exception {
    UUID parentEventId = markMasterAsHandled(SCC);
    EventKey sccKey = toSccKey(createChange());
    EiffelEvent event = publisher.getPublished(sccKey);
    assertNotNull("Publisher did not find SCC event", event);
    assertEquals(EventKey.fromEvent(event), sccKey);
    assertSccLinks(Lists.newArrayList(parentEventId), event.links);
  }

  @Test
  public void patchSetCreatedForMetaDoesntResultsInEvent() throws Exception {
    publisher.assertNotPublished(
        toSccKey(createMetaConfigChange()), "refs/meta/config change shouldn't result in event");
  }

  @Test
  public void patchSetSubmittedResultsInEvent_parentHandled() throws Exception {
    markMasterAsHandled(SCC);
    PushOneCommit.Result res = createChange();
    SourceChangeEventKey sccKey = toSccKey(res);
    EiffelEvent sccEvent = publisher.getPublished(sccKey);

    UUID scsParentEventId = markMasterAsHandled(SCS);
    merge(res);
    EventKey scsKey = sccKey.copy(SCS);
    EiffelEvent scsEvent = publisher.getPublished(scsKey);
    assertNotNull("Publisher did not find SCS event", scsEvent);
    assertEquals(EventKey.fromEvent(scsEvent), scsKey);
    assertScsLinks(Lists.newArrayList(scsParentEventId), sccEvent.meta.id, scsEvent.links);
  }

  @Test
  public void patchSetSubmittedResultsInEvents_parentNotHandled() throws Exception {
    UUID masterSccEventId = markMasterAsHandled(SCC);
    RevCommit previousMaster = getMaster();

    TestEventListenersConfigProvider.disable();

    PushOneCommit.Result res1 = createChange();
    SourceChangeEventKey sccKey1 = toSccKey(res1);
    merge(res1);

    TestEventListenersConfigProvider.enable();

    PushOneCommit.Result res2 = createChange();
    SourceChangeEventKey sccKey2 = toSccKey(res2);

    EiffelEvent sccEvent1 = publisher.getPublished(sccKey1);
    assertNotNull("Publisher did not find SCC event1", sccEvent1);
    assertSccLinks(Lists.newArrayList(masterSccEventId), sccEvent1.links);

    EiffelEvent sccEvent2 = publisher.getPublished(sccKey2);
    assertNotNull("Publisher did not find SCC event2", sccEvent2);
    assertSccLinks(Lists.newArrayList(sccEvent1.meta.id), sccEvent2.links);

    merge(res2);

    EiffelEvent masterScsEvent =
        publisher.getPublished(
            SourceChangeEventKey.scsKey(project.get(), "master", previousMaster));
    assertNotNull("Publisher did not find SCS event for master", masterScsEvent);

    EventKey scsKey1 = sccKey1.copy(SCS);
    EiffelEvent scsEvent1 = publisher.getPublished(scsKey1);
    assertNotNull("Publisher did not find SCS event", scsEvent1);
    assertEquals(EventKey.fromEvent(scsEvent1), scsKey1);
    assertScsLinks(Lists.newArrayList(masterScsEvent.meta.id), sccEvent1.meta.id, scsEvent1.links);

    EventKey scsKey2 = sccKey2.copy(SCS);
    EiffelEvent scsEvent2 = publisher.getPublished(scsKey2);
    assertNotNull("Publisher did not find SCS event", scsEvent2);
    assertEquals(EventKey.fromEvent(scsEvent2), scsKey2);
    assertScsLinks(Lists.newArrayList(scsEvent1.meta.id), sccEvent2.meta.id, scsEvent2.links);
  }

  @Test
  public void patchSetSubmittedForMetaDoesntResultsInEvent() throws Exception {
    PushOneCommit.Result res = createMetaConfigChange();
    publisher.assertNotPublished(
        toSccKey(res), "refs/meta/config change shouldn't result in event");

    merge(res);
    publisher.assertNotPublished(
        toScsKey(res), "refs/meta/config change shouldn't result in event");
  }

  @Test
  public void noEventsCreatedWhenDisabled() throws Exception {
    TestEventListenersConfigProvider.disable();
    PushOneCommit.Result res = createChange();
    publisher.assertNotPublished(
        toSccKey(res), "Patch-set created shouldn't result in event when disabled");
    merge(res);
    publisher.assertNotPublished(
        toScsKey(res), "Patch-set submitted shouldn't result in event when disabled");
  }

  @Test
  public void noEventsCreatedWhenRefIsBlocked() throws Exception {
    TestEventsFilterProvider.setBlockedRefPatterns("refs/heads/master");
    PushOneCommit.Result res = createChange();
    publisher.assertNotPublished(
        toSccKey(res), "Patch-set created shouldn't result in event when target ref is blocked");
    merge(res);
    publisher.assertNotPublished(
        toScsKey(res), "Patch-set submitted shouldn't result in event when target ref is blocked");
  }

  @Test
  public void noEventsCreatedWhenProjectIsBlocked() throws Exception {
    TestEventsFilterProvider.setBlockedProjectPatterns(project.get());
    PushOneCommit.Result res = createChange();
    publisher.assertNotPublished(
        toSccKey(res), "Patch-set created shouldn't result in event when project is blocked");
    merge(res);
    publisher.assertNotPublished(
        toScsKey(res), "Patch-set submitted shouldn't result in event when project is blocked");
  }

  @Test
  public void lightweightTagCreatedResultsInEvent() throws Exception {
    tagCreatedResultsInEvent(false);
  }

  @Test
  public void annotatedTagCreatedResultsInEvent() throws Exception {
    tagCreatedResultsInEvent(true);
  }

  private void tagCreatedResultsInEvent(boolean annotated) throws Exception {
    String tagName = createTagRef(annotated).substring(RefNames.REFS_TAGS.length());

    EventKey artcKey = ArtifactEventKey.create(tagPURL(project.get(), tagName));
    EiffelEvent artcEvent = publisher.getPublished(artcKey);
    assertNotNull("Publisher did not find ARTC event", artcEvent);
    assertEquals(EventKey.fromEvent(artcEvent), artcKey);

    EventKey cdKey = CompositionDefinedEventKey.create(tagCompositionName(project.get()), tagName);
    EiffelEvent cdEvent = publisher.getPublished(cdKey);
    assertNotNull("Publisher did not find CD event", cdEvent);
    assertEquals(EventKey.fromEvent(cdEvent), cdKey);

    EventKey scsKey = SourceChangeEventKey.scsKey(project.get(), "master", getMaster());
    EiffelEvent scsEvent = publisher.getPublished(scsKey);
    assertNotNull("Publisher did not find SCS event", scsEvent);
    assertEquals(EventKey.fromEvent(scsEvent), scsKey);

    assertArtcLinks(cdEvent.meta.id, artcEvent.links);
    assertCdLinks(scsEvent.meta.id, cdEvent.links);
  }

  @Test
  public void tagCreatedResultsInNoEventWhenBranchIsBlocked() throws Exception {
    TestEventsFilterProvider.setBlockedRefPatterns("refs/heads/master");
    String tagName = createTagRef(true).substring(RefNames.REFS_TAGS.length());

    EventKey artcKey = ArtifactEventKey.create(tagPURL(project.get(), tagName));
    publisher.assertNotPublished(artcKey, "Publisher found ARTC event");

    EventKey cdKey = CompositionDefinedEventKey.create(tagCompositionName(project.get()), tagName);
    publisher.assertNotPublished(cdKey, "Publisher found CD event");

    EventKey scsKey = SourceChangeEventKey.scsKey(project.get(), "master", getMaster());
    publisher.assertNotPublished(scsKey, "Publisher found SCS event");
  }

  @Test
  public void tagCreatedResultsInNoEventWhenBranchIsBlockedSCSHandled() throws Exception {
    UUID parentEventId = markMasterAsHandled(SCS);
    TestEventsFilterProvider.setBlockedRefPatterns("refs/heads/master");
    String tagName = createTagRef(true).substring(RefNames.REFS_TAGS.length());

    EventKey artcKey = ArtifactEventKey.create(tagPURL(project.get(), tagName));
    EiffelEvent artcEvent = publisher.getPublished(artcKey);
    assertNotNull("Publisher did not find ARTC event", artcEvent);
    assertEquals(EventKey.fromEvent(artcEvent), artcKey);

    EventKey cdKey = CompositionDefinedEventKey.create(tagCompositionName(project.get()), tagName);
    EiffelEvent cdEvent = publisher.getPublished(cdKey);
    assertNotNull("Publisher did not find CD event", cdEvent);
    assertEquals(EventKey.fromEvent(cdEvent), cdKey);

    assertArtcLinks(cdEvent.meta.id, artcEvent.links);
    assertCdLinks(parentEventId, cdEvent.links);
  }

  @Test
  public void eventsCreatedForNewbornBranch() throws Exception {
    BranchInfo info =
        gApi.projects().name(project.get()).branch("new-branch").create(new BranchInput()).get();
    SourceChangeEventKey scc =
        SourceChangeEventKey.sccKey(project.get(), "new-branch", info.revision);
    EiffelEvent sccEvent = publisher.getPublished(scc);
    assertNotNull("New branch should result in SCC event", sccEvent);
    EiffelEvent scsEvent = publisher.getPublished(scc.copy(SCS));
    assertNotNull("New branch should result in SCS event", scsEvent);
  }

  private PushOneCommit.Result createMetaConfigChange()
      throws GitAPIException, InvalidRemoteException, TransportException, Exception {
    RevCommit head = getHead(repo(), "HEAD");
    git()
        .fetch()
        .setRefSpecs(
            new RefSpec(String.format("%s:%s", RefNames.REFS_CONFIG, RefNames.REFS_CONFIG)))
        .call();
    testRepo.reset(RefNames.REFS_CONFIG);
    PushOneCommit.Result res = createChange("refs/for/refs/meta/config");
    testRepo.reset(head.getName());
    return res;
  }

  private UUID markMasterAsHandled(EiffelEventType as) throws Exception {
    RevCommit originMaster = getMaster();
    SourceChangeEventKey scc = SourceChangeEventKey.sccKey(project.get(), "master", originMaster);
    return markAsHandled(as.equals(SCS) ? scc.copy(SCS) : scc, originMaster);
  }

  private RevCommit getMaster()
      throws GitAPIException, InvalidRemoteException, TransportException, Exception {
    git().fetch().setRefSpecs(new RefSpec(RefNames.REFS_HEADS + "master:FETCH_HEAD")).call();
    RevCommit originMaster = getHead(repo(), "FETCH_HEAD");
    return originMaster;
  }

  public static class TestEventListenersConfigProvider implements Provider<EventListenersConfig> {
    private static boolean enabled;

    static {
      reset();
    }

    static void reset() {
      enabled = true;
    }

    static void disable() {
      enabled = false;
    }

    static void enable() {
      enabled = true;
    }

    @Override
    public EventListenersConfig get() {
      return new EventListenersConfig(enabled);
    }
  }

  public static class TestModule extends AbstractModule {

    @Override
    protected void configure() {
      install(new EiffelEventsTestModule());
      bind(EventListenersConfig.class).toProvider(TestEventListenersConfigProvider.class);
      bind(EventsFilter.class).toProvider(TestEventsFilterProvider.class);
      DynamicSet.bind(binder(), RevisionCreatedListener.class).to(PatchsetCreatedListener.class);
      DynamicSet.bind(binder(), GitReferenceUpdatedListener.class).to(RefUpdateListener.class);
      bind(EiffelConfig.class).toProvider(EiffelConfig.Provider.class).in(Scopes.SINGLETON);
    }
  }
}
