package com.googlesource.gerrit.plugins.eventseiffel.parsing;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import com.google.inject.Singleton;
import com.googlesource.gerrit.plugins.eventseiffel.EiffelEventHub;
import com.googlesource.gerrit.plugins.eventseiffel.EiffelEventsTest.TestEventsFilterProvider;
import com.googlesource.gerrit.plugins.eventseiffel.TestEventHub;
import com.googlesource.gerrit.plugins.eventseiffel.TestEventStorage;
import com.googlesource.gerrit.plugins.eventseiffel.cache.EiffelEventIdCacheImpl;
import com.googlesource.gerrit.plugins.eventseiffel.config.EiffelConfig;
import com.googlesource.gerrit.plugins.eventseiffel.config.EventIdCacheConfig;
import com.googlesource.gerrit.plugins.eventseiffel.config.EventMappingConfig;
import com.googlesource.gerrit.plugins.eventseiffel.config.EventsFilter;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.api.EventStorage;
import com.googlesource.gerrit.plugins.eventseiffel.mapping.EiffelEventFactory;
import com.googlesource.gerrit.plugins.eventseiffel.mapping.EiffelEventMapper;
import org.junit.Ignore;

@Ignore
public class ParsingTestModule extends AbstractModule {
  @Override
  protected void configure() {
    bind(EventStorage.class).toProvider(TestEventStorage.Provider.class).in(Singleton.class);
    bind(EventIdCacheConfig.class).toProvider(EventIdCacheConfig.Provider.class);
    bind(EventsFilter.class).toProvider(TestEventsFilterProvider.class);
    install(EiffelEventIdCacheImpl.module());
    bind(EiffelEventHub.class).to(TestEventHub.class);
    bind(EventMappingConfig.class).toProvider(EventMappingConfig.Provider.class);
    bind(EiffelEventMapper.class);
    bind(EiffelEventParser.class).to(EiffelEventParserImpl.class);
    bind(EiffelConfig.class).toProvider(EiffelConfig.Provider.class).in(Scopes.SINGLETON);
    bind(EiffelEventFactory.class)
        .toProvider(EiffelEventFactory.Provider.class)
        .in(Scopes.SINGLETON);
  }
}
