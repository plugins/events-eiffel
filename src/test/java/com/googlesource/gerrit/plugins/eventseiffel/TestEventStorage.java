// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel;

import static com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType.SCS;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.inject.Singleton;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.EventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.SourceChangeEventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.api.EventStorage;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.api.EventStorageException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import org.junit.Ignore;

@Ignore
@Singleton
public class TestEventStorage implements EventStorage {
  public static final TestEventStorage INSTANCE = new TestEventStorage();

  Map<EventKey, Integer> hits;
  Map<EventKey, UUID> ids;
  Map<Map.Entry<String, String>, Integer> scsIdsHits;

  public TestEventStorage() {
    reset();
  }

  @Override
  public Optional<UUID> getEventId(EventKey key) throws EventStorageException {
    if (!hits.containsKey(key)) {
      hits.put(key, 0);
    }
    hits.put(key, hits.get(key) + 1);
    return Optional.ofNullable(ids.getOrDefault(key, null));
  }

  @Override
  public List<UUID> getScsIds(String repo, String commit) throws EventStorageException {
    Map.Entry<String, String> key = Map.entry(repo, commit);
    if (!scsIdsHits.containsKey(key)) {
      scsIdsHits.put(key, 0);
    }
    scsIdsHits.put(key, scsIdsHits.get(key) + 1);
    return ids.entrySet().stream()
        .filter(e -> equalsSCSWithRepoAndCommit(e.getKey(), repo, commit))
        .map(Map.Entry::getValue)
        .collect(Collectors.toList());
  }

  @Override
  public Optional<List<UUID>> getParentLinks(EventKey key) throws EventStorageException {
    return Optional.of(Lists.newArrayList());
  }

  @Override
  public Optional<UUID> getSccEventLink(SourceChangeEventKey key) throws EventStorageException {
    return Optional.empty();
  }

  private boolean equalsSCSWithRepoAndCommit(EventKey key, String repo, String commit) {
    if (!key.type().equals(SCS)) return false;
    SourceChangeEventKey scsKey = (SourceChangeEventKey) key;
    return scsKey.repo().equals(repo) && scsKey.commit().equals(commit);
  }

  public UUID addId(EventKey key) {
    UUID newUuid = UUID.randomUUID();
    ids.put(key, newUuid);
    return newUuid;
  }

  public int queriesFor(EventKey key) {
    return hits.getOrDefault(key, 0);
  }

  public int queriesForScsIds(String repo, String commit) {
    return scsIdsHits.getOrDefault(Map.entry(repo, commit), 0);
  }

  public static class Provider implements com.google.inject.Provider<TestEventStorage> {

    @Override
    public TestEventStorage get() {
      return INSTANCE;
    }
  }

  public void reset() {
    this.hits = Maps.newHashMap();
    this.ids = Maps.newHashMap();
    this.scsIdsHits = Maps.newHashMap();
  }

  public void resetQueryCount() {
    this.hits = Maps.newHashMap();
  }
}
