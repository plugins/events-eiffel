// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto;

import static com.google.common.truth.Truth.assertThat;
import static com.google.gerrit.testing.GerritJUnit.assertThrows;
import static org.junit.Assert.assertEquals;

import com.google.gson.Gson;
import org.junit.Test;

public class EiffelFileInfoTest {
  @Test
  public void artifactNameIsRequired() {
    IllegalStateException thrown =
        assertThrows(
            IllegalStateException.class,
            () -> EiffelFileInfo.builder().addTag("artifact-tag").build());
    assertThat(thrown).hasMessageThat().isEqualTo("name is required");
  }

  @Test
  public void isSerializable() {
    String serialized =
        new Gson()
            .toJson(
                EiffelFileInfo.builder()
                    .name("package/artifacts/file")
                    .addTag("artifact-tag")
                    .build());
    String expected = "{\"name\":\"package/artifacts/file\",\"tags\":[\"artifact-tag\"]}";
    assertEquals("Matches json", expected, serialized);
  }

  @Test
  public void isDeSerializable() {
    String json = "{\"name\":\"package/artifacts/file\",\"tags\":[\"artifact-tag\"]}";
    EiffelFileInfo parsed = new Gson().fromJson(json, EiffelFileInfo.class);
    assertEquals("package/artifacts/file", parsed.name);
    assertEquals(1, parsed.tags.length);
    assertEquals("artifact-tag", parsed.tags[0]);
  }
}
