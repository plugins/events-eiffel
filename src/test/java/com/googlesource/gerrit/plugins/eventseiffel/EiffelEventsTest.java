// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel;

import static com.google.common.truth.Truth.assertThat;
import static com.googlesource.gerrit.plugins.eventseiffel.config.EventsFilter.Provider.toCombinedMatcher;
import static com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType.SCC;
import static com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType.SCS;

import com.google.common.collect.Streams;
import com.google.gerrit.acceptance.LightweightPluginDaemonTest;
import com.google.gerrit.acceptance.PushOneCommit;
import com.google.gerrit.entities.RefNames;
import com.google.gerrit.extensions.api.projects.TagInput;
import com.google.inject.Provider;
import com.googlesource.gerrit.plugins.eventseiffel.config.EventsFilter;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.EventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.SourceChangeEventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.api.EventStorageException;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelLinkInfo;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelLinkType;
import com.googlesource.gerrit.plugins.eventseiffel.mapping.EiffelEventFactory;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;
import org.eclipse.jgit.revwalk.RevCommit;
import org.junit.Ignore;

@Ignore
public class EiffelEventsTest extends LightweightPluginDaemonTest {
  protected static final String HOST_NAME = "localhost";
  protected static final String PLUGIN_NAME = "events-eiffel";
  protected static final String WEB_URL = "http://" + HOST_NAME + ":8080/gerrit";
  protected static final String VERSION = "1.2.3";
  protected static final int SSH_PORT = 29418;
  protected static final String NAMESPACE = "gerrit-review";
  protected static final String TAG_NAME = "v1.0";
  protected static final String TAG_PURL_TEMPLATE = "pkg:generic/%s/%s@%s?vcs_url=git%%2B%s%%40%s";
  private static final long SECONDS_TO_MILLIS = 1000l;
  private static int number = 0;

  protected static final Long commitTimeInEpochMillis(RevCommit commit) {
    return commit.getCommitTime() * SECONDS_TO_MILLIS;
  }

  protected void assertSccLinks(List<UUID> parentUuids, EiffelLinkInfo... links) throws Exception {
    assertThat(links)
        .isEqualTo(
            parentUuids.stream()
                .map(
                    pu ->
                        EiffelLinkInfo.builder()
                            .type(EiffelLinkType.PREVIOUS_VERSION)
                            .target(pu)
                            .build())
                .toArray());
  }

  protected void assertScsLinks(List<UUID> parentUuids, UUID sccId, EiffelLinkInfo... links) {
    assertThat(links)
        .isEqualTo(
            Streams.concat(
                    parentUuids.stream()
                        .map(
                            pu ->
                                EiffelLinkInfo.builder()
                                    .type(EiffelLinkType.PREVIOUS_VERSION)
                                    .target(pu)
                                    .build()),
                    Stream.of(
                        EiffelLinkInfo.builder().type(EiffelLinkType.CHANGE).target(sccId).build()))
                .toArray());
  }

  protected void assertArtcLinks(UUID cdId, EiffelLinkInfo... links) throws Exception {
    assertThat(links)
        .isEqualTo(
            new EiffelLinkInfo[] {
              EiffelLinkInfo.builder().type(EiffelLinkType.COMPOSITION).target(cdId).build()
            });
  }

  protected void assertCdLinks(UUID scsId, EiffelLinkInfo... links) throws Exception {
    assertThat(links)
        .isEqualTo(
            new EiffelLinkInfo[] {
              EiffelLinkInfo.builder().type(EiffelLinkType.ELEMENT).target(scsId).build()
            });
  }

  protected SourceChangeEventKey toSccKey(PushOneCommit.Result result) {
    return SourceChangeEventKey.sccKey(
        result.getChange().project().get(),
        result.getChange().change().getDest().branch().substring(RefNames.REFS_HEADS.length()),
        result.getCommit().getName());
  }

  protected SourceChangeEventKey toScsKey(PushOneCommit.Result result) {
    return SourceChangeEventKey.scsKey(
        result.getChange().project().get(),
        result.getChange().change().getDest().branch(),
        result.getCommit().getName());
  }

  protected void setScsHandled() throws Exception {
    markAsHandled(eventForHead(EiffelEventType.SCS), getHead(repo(), "HEAD"));
  }

  protected SourceChangeEventKey eventForHead(EiffelEventType type) throws Exception {
    return SourceChangeEventKey.create(project.get(), getHead(), getHeadRevision(), type);
  }

  protected String getHead() throws Exception {
    return gApi.projects().name(project.get()).head();
  }

  protected String getHeadRevision() throws Exception {
    return getHead(repo(), "HEAD").getName();
  }

  protected UUID markAsHandled(EventKey key) throws EventStorageException {
    Optional<UUID> eventId = TestEventStorage.INSTANCE.getEventId(key);
    if (eventId.isPresent()) {
      return eventId.get();
    }
    return TestEventStorage.INSTANCE.addId(key);
  }

  protected UUID markAsHandled(SourceChangeEventKey key, RevCommit commit)
      throws EventStorageException {
    Optional<UUID> eventId = TestEventStorage.INSTANCE.getEventId(key);
    if (eventId.isPresent()) {
      return eventId.get();
    }
    if (key.type().equals(SCS)) {
      /* Create SCCs */
      markAsHandledRec(key.copy(SCC), commit);
    }
    UUID res = markAsHandledRec(key, commit);
    TestEventStorage.INSTANCE.resetQueryCount();
    return res;
  }

  protected UUID markAsHandledRec(SourceChangeEventKey key, RevCommit commit)
      throws EventStorageException {
    SourceChangeEventKey current = key.copy(commit);
    Optional<UUID> fromStorage = TestEventStorage.INSTANCE.getEventId(current);
    if (fromStorage.isPresent()) {
      return fromStorage.get();
    }
    UUID created = TestEventStorage.INSTANCE.addId(current);
    for (RevCommit parent : commit.getParents()) {
      markAsHandledRec(current, parent);
    }
    return created;
  }

  protected static String tagCompositionName(String projectName) {
    return "scmtag/git/" + NAMESPACE + "/" + URLEncoder.encode(projectName, StandardCharsets.UTF_8);
  }

  protected static String tagPURL(String projectName, String tagName) {
    return String.format(
        TAG_PURL_TEMPLATE,
        NAMESPACE,
        projectName,
        tagName,
        String.format("ssh://%s:%d/%s", HOST_NAME, SSH_PORT, projectName),
        tagName);
  }

  protected String createTagRef(boolean annotated) throws Exception {
    return createTagRef(getHead(repo(), "HEAD").getName(), annotated);
  }

  protected String createTagRef(String revision, boolean annotated) throws Exception {
    return createTagRef("v." + number++, revision, annotated);
  }

  protected String createTagRef(String name, String revision, boolean annotated) throws Exception {
    TagInput input = new TagInput();
    input.ref = name;
    input.revision = revision;
    if (annotated) {
      input.message = "A message";
    }
    return gApi.projects().name(project.get()).tag(input.ref).create(input).get().ref;
  }

  public static class TestEventsFilterProvider implements Provider<EventsFilter> {
    private static String[] refPatterns;
    private static String[] projectPatterns;
    private static boolean blockLightweightTags;

    static {
      reset();
    }

    public static void reset() {
      refPatterns = projectPatterns = new String[0];
      blockLightweightTags = false;
    }

    public static void setBlockedRefPatterns(String... patterns) {
      refPatterns = patterns;
    }

    public static void setBlockedProjectPatterns(String... patterns) {
      projectPatterns = patterns;
    }

    public static void blockLightWeightTags() {
      blockLightweightTags = true;
    }

    @Override
    public EventsFilter get() {
      return new EventsFilter(
          toCombinedMatcher(projectPatterns), toCombinedMatcher(refPatterns), blockLightweightTags);
    }
  }

  public static class TestEventFactoryProvider implements Provider<EiffelEventFactory> {

    @Override
    public EiffelEventFactory get() {
      return new EiffelEventFactory(
          PLUGIN_NAME, VERSION, WEB_URL, "*:" + SSH_PORT, Optional.of(NAMESPACE));
    }
  }
}
