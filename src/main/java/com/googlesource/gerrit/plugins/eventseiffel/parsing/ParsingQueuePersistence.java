// Copyright (C) 2022 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package com.googlesource.gerrit.plugins.eventseiffel.parsing;

import static java.nio.charset.StandardCharsets.UTF_8;

import com.google.common.flogger.FluentLogger;
import com.google.common.reflect.TypeToken;
import com.google.gerrit.extensions.annotations.PluginName;
import com.google.gerrit.server.config.SitePaths;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.inject.Inject;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Set;

public class ParsingQueuePersistence {
  private static final FluentLogger logger = FluentLogger.forEnclosingClass();
  private static final Gson GSON = new Gson();
  private static final String PERSISTED_QUEUE_FILE_NAME = "persisted-queue.json";
  private static final String PERSISTED_FAILED_FILE_NAME = "persisted-failed.json";

  private static void persistTo(Set<ParsingQueueTask> tasks, Path dir, String fileName) {
    Path file = dir.resolve(fileName);
    try {
      if (Files.isRegularFile(file)) {
        Files.delete(file);
      }
      if (tasks == null || tasks.isEmpty()) {
        return;
      }
      if (!Files.isDirectory(dir)) {
        Files.createDirectory(dir);
      }
      Files.write(file, GSON.toJson(tasks).getBytes(UTF_8));
    } catch (IOException e) {
      logger.atSevere().withCause(e).log("Failed to persist tasks to : %s", file);
    }
  }

  private static Set<ParsingQueueTask> getTasksFrom(Path file) {
    Set<ParsingQueueTask> persistedTasks = null;
    try {
      if (Files.isRegularFile(file)) {
        persistedTasks =
            GSON.fromJson(
                Files.newBufferedReader(file, UTF_8),
                new TypeToken<Set<ParsingQueueTask>>() {

                  private static final long serialVersionUID = 1L;
                }.getType());
      }
    } catch (JsonIOException | JsonSyntaxException | IOException e) {
      logger.atSevere().withCause(e).log("Failed to get tasks from %s.", file);
    }
    return persistedTasks != null ? persistedTasks : Set.of();
  }

  private final Path pluginDataDir;

  @Inject
  public ParsingQueuePersistence(SitePaths sitePaths, @PluginName String pluginName) {
    this.pluginDataDir = sitePaths.data_dir.resolve(pluginName);
  }

  public void persistQueue(Set<ParsingQueueTask> tasks) {
    persistTo(tasks, pluginDataDir, PERSISTED_QUEUE_FILE_NAME);
  }

  public void persistFailed(Set<ParsingQueueTask> failedTasks) {
    persistTo(failedTasks, pluginDataDir, PERSISTED_FAILED_FILE_NAME);
  }

  public Collection<ParsingQueueTask> getPersistedQueue() {
    return getTasksFrom(pluginDataDir.resolve(PERSISTED_QUEUE_FILE_NAME));
  }

  public Collection<ParsingQueueTask> getFailedTasks() {
    return getTasksFrom(pluginDataDir.resolve(PERSISTED_FAILED_FILE_NAME));
  }
}
