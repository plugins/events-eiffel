// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.config;

import com.google.gerrit.extensions.annotations.PluginName;
import com.google.gerrit.server.config.PluginConfigFactory;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.eclipse.jgit.lib.Config;

@Singleton
public class EventParsingConfig {

  public static class Provider implements com.google.inject.Provider<EventParsingConfig> {
    private static final String EVENT_PARSING = "EventParsing";
    private static final String POOL_SIZE = "poolSize";
    private static final int DEFAULT_POOL_SIZE = 1;
    private final EventParsingConfig config;

    @Inject
    public Provider(PluginConfigFactory cfgFactory, @PluginName String pluginName) {
      Config cfg = cfgFactory.getGlobalPluginConfig(pluginName);
      this.config =
          new EventParsingConfig(cfg.getInt(EVENT_PARSING, null, POOL_SIZE, DEFAULT_POOL_SIZE));
    }

    @Override
    public EventParsingConfig get() {
      return config;
    }
  }

  private final int eventParsingPoolSize;

  public EventParsingConfig(int eventParsingPoolSize) {
    this.eventParsingPoolSize = eventParsingPoolSize;
  }

  public int poolSize() {
    return eventParsingPoolSize;
  }
}
