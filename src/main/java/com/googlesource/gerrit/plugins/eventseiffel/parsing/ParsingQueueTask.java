// Copyright (C) 2022 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package com.googlesource.gerrit.plugins.eventseiffel.parsing;

import com.google.gerrit.entities.RefNames;
import com.google.gerrit.extensions.common.AccountInfo;
import com.google.gerrit.extensions.events.RevisionCreatedListener;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType;
import java.util.Objects;

/**
 * DTO that contains all necessary data to describe and reschedule a task in the {@link
 * EiffelEventParsingQueue}.
 */
public class ParsingQueueTask {
  static Builder builder(EiffelEventType type, String repoName, String branchRefOrTag) {
    return new Builder(type, repoName, branchRefOrTag);
  }

  /**
   * The event contains all information necessary to create a SCC without querying the index.
   * Setting the event also automatically sets repoName, branchRefOrTag, commitId from the event and
   * type to SCC and ignores all other fields.
   */
  static Builder sccBuilder(RevisionCreatedListener.Event event) {
    return new Builder(event);
  }

  public static class Builder {
    private EiffelEventType _type;
    private String _repoName;
    private String _branchRefOrTag;
    private String _commitId = null;
    private AccountInfo _updater = null;
    private Long _updateTime = null;
    private Boolean _force = null;
    private String _previousTip = null;
    private PatchsetCreationData _patchsetCreationData;
    private Boolean _fillGaps = false;

    private Builder(EiffelEventType type, String repoName, String branchRefOrTag) {
      this._type = type;
      this._repoName = repoName;
      this._branchRefOrTag = branchRefOrTag;
    }

    private Builder(RevisionCreatedListener.Event event) {
      this._patchsetCreationData = new PatchsetCreationData(event);
    }

    public Builder commit(String commitId) {
      this._commitId = commitId;
      return this;
    }

    public Builder updater(AccountInfo updater) {
      this._updater = updater;
      return this;
    }

    public Builder updateTime(Long updateTime) {
      this._updateTime = updateTime;
      return this;
    }

    public Builder force(boolean force) {
      this._force = force;
      return this;
    }

    public Builder previousTip(String previousTip) {
      this._previousTip = previousTip;
      return this;
    }
    /**
     * This attribute is specified when you want to create single SC eiffel-events that may be
     * missing for a branch.
     */
    public Builder fillGaps(boolean fillGaps) {
      this._fillGaps = fillGaps;
      return this;
    }

    public ParsingQueueTask build() {
      if (this._patchsetCreationData != null) {
        return new ParsingQueueTask(_patchsetCreationData);
      }
      return new ParsingQueueTask(
          this._type,
          this._repoName,
          this._branchRefOrTag,
          this._commitId,
          this._updater,
          this._updateTime,
          this._previousTip,
          this._force,
          this._fillGaps);
    }
  }

  final String repoName;
  final String branchRefOrTag;
  final String commitId;
  final EiffelEventType type;
  final AccountInfo updater;
  final Long updateTime;
  final Boolean force;
  final String previousTip;
  final PatchsetCreationData patchsetCreationData;
  final Boolean fillGaps;

  private ParsingQueueTask(
      EiffelEventType type,
      String repoName,
      String branchRefOrTag,
      String commitId,
      AccountInfo updater,
      Long updateTime,
      String previousTip,
      Boolean force,
      Boolean fillGaps) {
    this.type = type;
    this.repoName = repoName;
    this.branchRefOrTag =
        branchRefOrTag.startsWith(RefNames.REFS_TAGS)
            ? branchRefOrTag.substring(RefNames.REFS_TAGS.length())
            : branchRefOrTag;
    this.commitId = commitId;
    this.updater = updater;
    this.updateTime = updateTime;
    this.previousTip = previousTip;
    this.force = force;
    this.patchsetCreationData = null;
    this.fillGaps = fillGaps;
  }

  private ParsingQueueTask(PatchsetCreationData data) {
    this.type = EiffelEventType.SCC;
    this.repoName = data.project;
    this.branchRefOrTag = data.branch;
    this.commitId = data.commitId;
    this.updater = null;
    this.updateTime = null;
    this.previousTip = null;
    this.force = null;
    this.patchsetCreationData = data;
    this.fillGaps = false;
  }

  @Override
  public String toString() {
    String target =
        branchRefOrTag.startsWith(RefNames.REFS_HEADS)
            ? branchRefOrTag.substring(RefNames.REFS_HEADS.length())
            : branchRefOrTag;
    if (commitId != null) {
      return String.format("%s[%s, %s, %s, %b]", type.name(), repoName, target, commitId, fillGaps);
    }
    return String.format("%s[%s, %s, %b]", type.name(), repoName, target, fillGaps);
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof ParsingQueueTask) {
      ParsingQueueTask that = (ParsingQueueTask) o;
      return Objects.equals(this.type, that.type)
          && Objects.equals(this.repoName, that.repoName)
          && Objects.equals(this.commitId, that.commitId)
          && Objects.equals(this.updater, that.updater)
          && Objects.equals(this.updateTime, that.updateTime)
          && Objects.equals(this.previousTip, that.previousTip)
          && Objects.equals(this.force, that.force)
          && Objects.equals(this.fillGaps, that.fillGaps);
    }
    return false;
  }

  @Override
  public int hashCode() {
    return Objects.hash(type, repoName, commitId, updater, updateTime, previousTip, force);
  }
}
