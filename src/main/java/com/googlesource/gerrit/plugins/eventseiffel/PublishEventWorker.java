// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel;

import com.github.rholder.retry.RetryException;
import com.github.rholder.retry.Retryer;
import com.github.rholder.retry.RetryerBuilder;
import com.github.rholder.retry.WaitStrategies;
import com.google.common.flogger.FluentLogger;
import com.google.inject.Inject;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.api.EiffelEventPublisher;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEvent;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class PublishEventWorker implements Runnable, EiffelEventHub.Consumer {
  static final String THREAD_NAME = "eiffel-event-publishing";
  private static final FluentLogger logger = FluentLogger.forEnclosingClass();
  private static int WAIT_SECONDS = 5;

  private final EiffelEventPublisher publisher;
  private EiffelEventQueue eventQueue;
  private Thread thread;
  boolean running = false;

  @Inject
  public PublishEventWorker(EiffelEventPublisher publisher) {
    this.publisher = publisher;
  }

  @Override
  public void run() {
    Set<EiffelEvent> events = null;
    try {
      while (running) {
        events = eventQueue.take(publisher.maxBatchSize());
        boolean unpublishedEvents = events != null && !events.isEmpty();
        while (running && unpublishedEvents) {
          unpublishedEvents = !publishWithRetry(events);
          if (!unpublishedEvents) {
            eventQueue.ack(events);
            events = null;
          } else {
            logger.atSevere().log("Failed to publish events, waiting %d seconds.", WAIT_SECONDS);
            Thread.sleep(WAIT_SECONDS * 1000);
          }
        }
      }
    } catch (RetryException | ExecutionException e) {
      logger.atSevere().withCause(e).log("Publishing failed even after retries.");
      running = false;
    } catch (InterruptedException e) {
      if (running) {
        logger.atSevere().withCause(e).log("Publisher thread interrupted while running.");
        running = false;
      }
    } finally {
      if (events != null && eventQueue != null) {
        eventQueue.nak(events);
      }
      eventQueue = null;
      logger.atInfo().log("Stopped publishing.");
    }
  }

  private boolean publishWithRetry(Set<EiffelEvent> events)
      throws RetryException, ExecutionException {
    int timeout = 30;
    Retryer<Boolean> retryer =
        RetryerBuilder.<Boolean>newBuilder()
            .retryIfException()
            .withWaitStrategy(WaitStrategies.fixedWait(timeout, TimeUnit.SECONDS))
            .build();
    return retryer.call(
        () -> {
          try {
            return publisher.publish(events);
          } catch (Exception e) {
            logger.atWarning().withCause(e).log(
                "Publishing event failed. Will retry again after %d seconds", timeout);
            if (running) {
              throw e;
            }
            return false;
          }
        });
  }

  @Override
  public void start(EiffelEventQueue queue) {
    if (thread != null && thread.isAlive()) {
      logger.atWarning().log("Call to start thread that is already alive.");
      return;
    }
    this.eventQueue = queue;
    this.running = true;
    thread = new Thread(this, THREAD_NAME);
    thread.start();
  }

  @Override
  public void stop() {
    logger.atInfo().log("Stopping publisher thread");
    this.running = false;
    try {
      thread.join(5000);
      if (thread.isAlive()) {
        thread.interrupt();
      }
    } catch (InterruptedException e) {
      // Do nothing
    }
  }

  @Override
  public boolean isRunning() {
    return running;
  }
}
