// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto;

import static com.google.common.base.Preconditions.checkState;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Optional;

/**
 * An optional object for enclosing security related information, particularly supporting data
 * integrity. See <a
 * href="https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-syntax-and-usage/security.md">Security</a>
 * for further information.
 *
 * <p>As declared by:
 * https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelSourceChangeCreatedEvent.md#metasecurity
 */
public class EiffelSecurityInfo {
  public static Builder builder() {
    return new Builder();
  }
  /**
   * The identity of the author of the event. This property is intended to enable the recipient to
   * identify the author of the event contents and/or look up the appropriate public key for
   * decrypting the meta.security.integrityProtection.signature value and thereby verifying author
   * identity and data integrity.
   */
  public final String authorIdentity;
  /** An optional object for enabling information integrity protection via cryptographic signing. */
  public final EiffelIntegrityProtectionInfo integrityProtection;
  /**
   * An optional object for enabling verification of intact event sequences in a distributed
   * environment, thereby protecting against data loss, race conditions and replay attacks
   */
  public final EiffelSequenceProtectionInfo[] sequenceProtection;

  private EiffelSecurityInfo(
      String authorIdentity,
      EiffelIntegrityProtectionInfo integrityProtection,
      EiffelSequenceProtectionInfo[] sequenceProtection) {
    this.authorIdentity = authorIdentity;
    this.integrityProtection = integrityProtection;
    this.sequenceProtection = sequenceProtection;
  }

  public static class Builder {
    private String _authorIdentity = null;
    private Optional<EiffelIntegrityProtectionInfo.Builder> _integrityProtection = Optional.empty();
    private Optional<ArrayList<EiffelSequenceProtectionInfo>> _sequenceProtections =
        Optional.empty();

    private Builder() {}

    public Builder authorIdentity(String authorIdentity) {
      this._authorIdentity = authorIdentity;
      return this;
    }

    public Builder integrityProtection(EiffelIntegrityProtectionInfo.Builder integrityProtection) {
      this._integrityProtection = Optional.of(integrityProtection);
      return this;
    }

    public Builder addSequenceProtection(EiffelSequenceProtectionInfo.Builder sequenceProtection) {
      _sequenceProtections = _sequenceProtections.or(() -> Optional.of(Lists.newArrayList()));
      _sequenceProtections.get().add(sequenceProtection.build());
      return this;
    }

    public EiffelSecurityInfo build() {
      checkState(_authorIdentity != null, "authorIdentity is required");
      return new EiffelSecurityInfo(
          _authorIdentity,
          _integrityProtection.map(b -> b.build()).orElse(null),
          _sequenceProtections
              .map(t -> t.stream().toArray(l -> new EiffelSequenceProtectionInfo[l]))
              .orElse(null));
    }
  }
}
