// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto;

import static com.google.common.base.Preconditions.checkState;

import java.util.Objects;
import java.util.UUID;

/** An object that represents a link to an Eiffel event. */
public class EiffelLinkInfo {

  public static Builder builder() {
    return new Builder();
  }
  /**
   * The Eiffel event type. Valid types, see <a
   * href="https://github.com/eiffel-community/eiffel/tree/edition-paris/eiffel-vocabulary">Eiffel
   * event spec</a>.
   */
  public final EiffelLinkType type;
  /** The UUID of the target Eiffel event. */
  public final UUID target;

  private EiffelLinkInfo(EiffelLinkType type, UUID target) {
    this.type = type;
    this.target = target;
  }

  @Override
  public boolean equals(Object other) {
    if (other instanceof EiffelLinkInfo) {
      EiffelLinkInfo o = (EiffelLinkInfo) other;
      return o.type.equals(this.type) && o.target.equals(this.target);
    }
    return false;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.type, this.target);
  }

  public static class Builder {
    private EiffelLinkType _type = null;
    private UUID _target = null;

    private Builder() {}

    public Builder type(EiffelLinkType type) {
      this._type = type;
      return this;
    }

    public Builder target(UUID targetUuid) {
      this._target = targetUuid;
      return this;
    }

    public EiffelLinkInfo build() {
      checkState(_type != null, "type is required");
      checkState(_target != null, "target is required");

      return new EiffelLinkInfo(this._type, _target);
    }
  }
}
