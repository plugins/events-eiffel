// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel;

import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEvent;
import java.util.Set;

public interface EiffelEventQueue {
  /**
   * Retrieves the head of this queue and marks it as taken, waiting if necessary until an event
   * becomes available and/or the already taken Event has been ack:ed by a call to ack(EiffelEvent).
   *
   * @param max number of events to take.
   * @return List<EiffelEvent>
   * @throws InterruptedException
   */
  public Set<EiffelEvent> take(int max) throws InterruptedException;
  /**
   * Marks events as submitted to Eiffel and removes it from this queue.
   *
   * @param events to acknowledge.
   */
  public void ack(Set<EiffelEvent> events);
  /**
   * Was not able to submit taken events to Eiffel.
   *
   * @param events
   */
  public void nak(Set<EiffelEvent> events);
}
