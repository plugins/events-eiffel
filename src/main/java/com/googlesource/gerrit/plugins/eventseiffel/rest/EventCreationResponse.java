// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.rest;

import static com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType.ARTC;
import static com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType.CD;
import static com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType.SCC;
import static com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType.SCS;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Lists;
import com.google.gerrit.extensions.restapi.CacheControl;
import com.google.gerrit.extensions.restapi.Response;
import com.google.gerrit.server.project.BranchResource;
import com.google.gerrit.server.project.RefResource;
import com.google.gerrit.server.project.TagResource;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType;
import java.util.List;

/** 202 confirmation of successful Event-creation request. */
class EventCreationResponse extends Response<EventCreationResponse.EventCreationScheduled> {
  static EventCreationResponse scc(BranchResource res) {
    return new EventCreationResponse(res, res.getRef(), SCC);
  }

  static EventCreationResponse scc(BranchResource res, String targetBranch) {
    return new EventCreationResponse(res, targetBranch, SCC);
  }

  static EventCreationResponse scs(BranchResource res) {
    return new EventCreationResponse(res, res.getRef(), SCS, SCC);
  }

  static EventCreationResponse artc(TagResource res) {
    return new EventCreationResponse(res, null, ARTC, CD, SCS, SCC);
  }

  private final EventCreationScheduled scheduled;

  private EventCreationResponse(RefResource res, String targetBranch, EiffelEventType... types) {
    this.scheduled = new EventCreationScheduled(res.getName(), res.getRef(), targetBranch, types);
  }

  @Override
  public boolean isNone() {
    return false;
  }

  @Override
  public int statusCode() {
    return 202;
  }

  @Override
  public EventCreationScheduled value() {
    return scheduled;
  }

  @Override
  public CacheControl caching() {
    return CacheControl.NONE;
  }

  @Override
  public Response<EventCreationScheduled> caching(CacheControl c) {
    throw new UnsupportedOperationException();
  }

  @Override
  public ImmutableMultimap<String, String> headers() {
    return ImmutableMultimap.of();
  }

  @Override
  public String toString() {
    return "["
        + statusCode()
        + "] "
        + scheduled.types
        + " "
        + scheduled.repoName
        + " "
        + scheduled.ref;
  }

  static class EventCreationScheduled {

    protected EventCreationScheduled(
        String repoName, String ref, String targetBranch, EiffelEventType... types) {
      this.types = Lists.newArrayList(types);
      this.repoName = repoName;
      this.ref = ref;
      this.branch = targetBranch;
      this.status = "Event creation scheduled";
    }

    public final List<EiffelEventType> types;
    public final String repoName;
    public final String ref;
    public final String status;
    public final String branch;
  }
}
