// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto;

import static com.google.common.base.Preconditions.checkState;

/**
 * An optional object for enabling verification of intact event sequences in a distributed
 * environment, thereby protecting against data loss, race conditions and replay attacks. It allows
 * event publishers to state the order in which they produce a certain set of events. In other
 * words, it cannot provide any global guarantees as to event sequencing, but rather per-publisher
 * guarantees. Every object in the array represents a named sequence of which this event forms a
 * part. For every event including a given named sequence, the publisher SHALL increment
 * meta.security.sequenceProtection.position by 1. The first event produced in a given named
 * sequence shall be numbered 1.
 *
 * <p>As declared by:
 * https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelSourceChangeCreatedEvent.md#metasecuritysequenceprotection
 */
public class EiffelSequenceProtectionInfo {
  public static Builder builder() {
    return new Builder();
  }
  /**
   * The name of the sequence. There MUST not be two identical
   * meta.security.sequenceProtection.sequenceName values in the same event.
   */
  public final String sequenceName;
  /** The number of the event within the named sequence. */
  public final Integer position;

  private EiffelSequenceProtectionInfo(String sequenceName, int position) {
    this.sequenceName = sequenceName;
    this.position = position;
  }

  public static class Builder {
    private String _sequenceName;
    private Integer _position;

    public Builder sequenceName(String sequenceName) {
      this._sequenceName = sequenceName;
      return this;
    }

    public Builder position(int position) {
      this._position = position;
      return this;
    }

    public EiffelSequenceProtectionInfo build() {
      checkState(_sequenceName != null, "sequenceName is required");
      checkState(_position != null, "position is required");
      checkState(_position > 0, "position must be larger than zero");
      return new EiffelSequenceProtectionInfo(_sequenceName, _position);
    }
  }
}
