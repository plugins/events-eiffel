// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.mapping;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Charsets;
import com.google.common.flogger.FluentLogger;
import com.google.gerrit.entities.RefNames;
import com.google.gerrit.extensions.annotations.PluginName;
import com.google.gerrit.extensions.api.GerritApi;
import com.google.gerrit.extensions.restapi.RestApiException;
import com.google.gerrit.server.config.CanonicalWebUrl;
import com.google.gerrit.server.ssh.SshAdvertisedAddresses;
import com.google.inject.Inject;
import com.googlesource.gerrit.plugins.eventseiffel.config.EiffelConfig;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelArtifactCreatedEventInfo;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelChangeInfo;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelCompositionDefinedEventInfo;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelGitIdentifierInfo;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelLinkInfo;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelLinkType;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelMetaInfo;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelPersonInfo;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelSourceChangeCreatedEventInfo;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelSourceChangeSubmittedEventInfo;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelSourceInfo;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.eclipse.jgit.util.SystemReader;

public class EiffelEventFactory {
  public static class Provider implements com.google.inject.Provider<EiffelEventFactory> {

    private static String getPluginVersion(GerritApi gApi, String pluginName) {
      try {
        return gApi.plugins().name(pluginName).get().version;
      } catch (RestApiException e) {
        logger.atWarning().withCause(e).log("Failed to get plugin version.");
      }
      return null;
    }

    private EiffelEventFactory eventFactory;
    private final GerritApi gApi;
    private final String webUrl;
    private final String pluginName;
    private final String sshAddress;
    private final Optional<String> namespace;

    @Inject
    public Provider(
        GerritApi gApi,
        @CanonicalWebUrl String webUrl,
        @PluginName String pluginName,
        @SshAdvertisedAddresses List<String> sshAddresses,
        EiffelConfig config) {
      this.gApi = gApi;
      this.webUrl = webUrl;
      this.pluginName = pluginName;
      this.sshAddress = sshAddresses.isEmpty() ? "" : sshAddresses.get(0);
      this.namespace = config.namespace();
    }

    @Override
    public EiffelEventFactory get() {
      if (eventFactory == null) {
        eventFactory =
            new EiffelEventFactory(
                pluginName, getPluginVersion(gApi, pluginName), webUrl, sshAddress, namespace);
      }
      return eventFactory;
    }
  }

  private static final FluentLogger logger = FluentLogger.forEnclosingClass();
  private static final String GERRIT = "Gerrit";
  private static final String PURL_BASE = "pkg:maven/com.googlesource.gerrit.plugins/";
  private final String projectCloneUrl;
  private final String commitRestUrl;
  private final String host;
  private final String pluginName;
  private final String packageUrl;
  private final String namespace;
  private final String tagPURLTemplate;

  @VisibleForTesting
  public EiffelEventFactory(
      String pluginName,
      String pluginVersion,
      String webUrl,
      String configuredSshAddress,
      Optional<String> namespace) {
    StringBuilder purl = new StringBuilder(PURL_BASE + pluginName);
    if (pluginVersion != null) {
      purl.append("@" + pluginVersion);
    }
    this.packageUrl = purl.toString();
    if (webUrl != null) {
      this.host = URI.create(webUrl).getHost();
    } else {
      this.host = SystemReader.getInstance().getHostname();
    }
    this.pluginName = pluginName;
    this.namespace = namespace.orElse(host);

    if (webUrl != null) {
      commitRestUrl = webUrl + (!webUrl.endsWith("/") ? "/" : "") + "a/projects/%s/commits/%s";
    } else {
      commitRestUrl = "/projects/%s/commits/%s";
    }

    String sshAddress = parseSshAddress(webUrl, configuredSshAddress);

    StringBuilder cloneUrl = new StringBuilder();
    cloneUrl.append("ssh://");
    cloneUrl.append(sshAddress);
    if (!sshAddress.endsWith("/")) {
      cloneUrl.append("/");
    }
    cloneUrl.append("%s");
    projectCloneUrl = cloneUrl.toString();
    tagPURLTemplate = "pkg:generic/" + this.namespace + "/%s@%s?vcs_url=git%%2B%s%%40%s";
  }

  private String parseSshAddress(String webUrl, String configuredSshAddress) {

    if (configuredSshAddress.startsWith("*:") || configuredSshAddress.isEmpty()) {
      try {
        configuredSshAddress =
            new URL(webUrl).getHost()
                + (!configuredSshAddress.isEmpty() ? configuredSshAddress.substring(1) : "");
      } catch (MalformedURLException e) {
        logger.atWarning().withCause(e).log(
            "Failed to construct SSH address from web URL \"%s\"", webUrl);
      }
    }

    int p = configuredSshAddress.indexOf(":");
    /* Has port. */
    if (p > 0) {
      try {
        /* Check if port-portion is valid integer. */
        Integer.parseInt(configuredSshAddress.substring(p + 1));
      } catch (NumberFormatException e) {
        /* Use default SSH port. */
        configuredSshAddress = configuredSshAddress.substring(0, p);
      }
    }
    return configuredSshAddress;
  }

  public EiffelSourceChangeCreatedEventInfo createScc(
      String authorName,
      String authorEmail,
      String authorUsername,
      String repoName,
      String branch,
      String commitSha1,
      Integer changeNbr,
      Long epochMillisCreated,
      List<UUID> parentEventIds) {
    return createScc(
        authorName,
        authorEmail,
        authorUsername,
        repoName,
        branch,
        commitSha1,
        changeNbr,
        epochMillisCreated,
        parentEventIds,
        Optional.empty());
  }

  public EiffelSourceChangeCreatedEventInfo createScc(
      String authorName,
      String authorEmail,
      String authorUsername,
      String repoName,
      String branch,
      String commitSha1,
      Integer changeNbr,
      Long epochMillisCreated,
      List<UUID> parentEventIds,
      Optional<UUID> uuid) {
    if (branch.startsWith(RefNames.REFS_HEADS)) {
      branch = branch.substring(RefNames.REFS_HEADS.length());
    }
    EiffelSourceChangeCreatedEventInfo.Builder scc =
        EiffelSourceChangeCreatedEventInfo.builder()
            .author(
                EiffelPersonInfo.builder().email(authorEmail).name(authorName).id(authorUsername))
            .gitIdentifier(
                EiffelGitIdentifierInfo.builder()
                    .commitId(commitSha1)
                    .branch(branch)
                    .repoName(repoName)
                    .repoUri(projectUrl(repoName)))
            .change(
                EiffelChangeInfo.builder()
                    .details(commitUrl(repoName, commitSha1))
                    .id(changeNbr != null ? String.valueOf(changeNbr) : null)
                    .tracker(GERRIT))
            .meta(getMeta(epochMillisCreated, uuid));
    parentEventIds.stream()
        .forEach(
            id ->
                scc.addLink(
                    EiffelLinkInfo.builder().type(EiffelLinkType.PREVIOUS_VERSION).target(id)));
    return scc.build();
  }

  public EiffelSourceChangeSubmittedEventInfo createScs(
      String name,
      String email,
      String username,
      String repoName,
      String branch,
      String commitSha1,
      Long epochMillisCreated,
      List<UUID> parentEventIds,
      UUID sccId,
      Optional<UUID> uuid) {
    EiffelSourceChangeSubmittedEventInfo.Builder scs =
        EiffelSourceChangeSubmittedEventInfo.builder()
            .submitter(EiffelPersonInfo.builder().email(email).name(name).id(username))
            .gitIdentifier(
                EiffelGitIdentifierInfo.builder()
                    .commitId(commitSha1)
                    .branch(branch)
                    .repoName(repoName)
                    .repoUri(projectUrl(repoName)))
            .meta(getMeta(epochMillisCreated, uuid));
    parentEventIds.stream()
        .forEach(
            id ->
                scs.addLink(
                    EiffelLinkInfo.builder().type(EiffelLinkType.PREVIOUS_VERSION).target(id)));
    scs.addLink(EiffelLinkInfo.builder().type(EiffelLinkType.CHANGE).target(sccId));
    return scs.build();
  }

  public EiffelArtifactCreatedEventInfo createArtc(
      String projectName, String tagName, Long epochMillisCreated, UUID cdId) {
    return EiffelArtifactCreatedEventInfo.builder()
        .identity(tagPURL(projectName, tagName))
        .meta(getMeta(epochMillisCreated))
        .addLink(EiffelLinkInfo.builder().type(EiffelLinkType.COMPOSITION).target(cdId))
        .build();
  }

  public EiffelCompositionDefinedEventInfo createCd(
      String projectName, String version, Long epochMillisCreated, UUID scsId) {
    return EiffelCompositionDefinedEventInfo.builder()
        .name(tagCompositionName(projectName))
        .version(version)
        .meta(getMeta(epochMillisCreated))
        .addLink(EiffelLinkInfo.builder().type(EiffelLinkType.ELEMENT).target(scsId))
        .build();
  }

  public String tagCompositionName(String projectName) {
    return "scmtag/git/" + namespace + "/" + URLEncoder.encode(projectName, StandardCharsets.UTF_8);
  }

  private EiffelMetaInfo.Builder getMeta(Long epochMillisCreated) {
    return getMeta(epochMillisCreated, Optional.empty());
  }

  private EiffelMetaInfo.Builder getMeta(Long epochMillisCreated, Optional<UUID> id) {
    return EiffelMetaInfo.builder()
        .source(
            EiffelSourceInfo.builder()
                .domainId(GERRIT)
                .name(pluginName)
                .host(host)
                .serializer(packageUrl))
        .time(epochMillisCreated)
        .id(id.orElse(UUID.randomUUID()));
  }

  private String projectUrl(String projectName) {
    return String.format(projectCloneUrl, projectName);
  }

  private String commitUrl(String projectName, String commitSha1) {
    return String.format(commitRestUrl, urlEncode(projectName), commitSha1);
  }

  private static String urlEncode(String name) {
    return URLEncoder.encode(name, Charsets.UTF_8);
  }

  public String tagPURL(String projectName, String tagName) {
    return String.format(tagPURLTemplate, projectName, tagName, projectUrl(projectName), tagName);
  }
}
