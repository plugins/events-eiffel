// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.cache;

import com.googlesource.gerrit.plugins.eventseiffel.eiffel.EventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.SourceChangeEventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEvent;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface EiffelEventIdCache {

  /**
   * Returns the id of the event that matches:
   *
   * <p>{@code EventKey.fromEvent(event).equals(key)}
   *
   * <p>Optional.Empty if no such event exists.
   *
   * @param key
   * @return Optional<UUID>
   * @throws EiffelEventIdLookupException if value failed to load.
   */
  Optional<UUID> getEventId(EventKey key) throws EiffelEventIdLookupException;

  /**
   * Returns the ids that is linked with PREVIOUS_VERSION by event that matches:
   *
   * <p>{@code EventKey.fromEvent(event).equals(key)}
   *
   * <p>Optional.Empty if no such event exists.
   *
   * @param key
   * @return Optional<List<UUID>>
   * @throws EiffelEventIdLookupException if value failed to load.
   */
  Optional<List<UUID>> getParentLinks(EventKey key) throws EiffelEventIdLookupException;

  /**
   * Returns the id that is linked with CHANGE by SCS-event that matches:
   *
   * <p>{@code EventKey.fromEvent(event).equals(key)}
   *
   * <p>Optional.Empty if no such event exists or if key is not a SCS key
   *
   * @param key
   * @return Optional<UUID>
   * @throws EiffelEventIdLookupException if value failed to load.
   */
  Optional<UUID> getSccEventLink(SourceChangeEventKey key) throws EiffelEventIdLookupException;

  /**
   * Returns the id of an event that matches the repo and the commit:
   *
   * <p>Optional.Empty if no such event exists.
   *
   * @param repo
   * @param commit
   * @param branches
   * @return Optional<UUID>
   */
  Optional<UUID> getScsForCommit(String repo, String commit, List<String> branches)
      throws EiffelEventIdLookupException;

  /**
   * Adds the meta.id of the event to the cache.
   *
   * @param event
   */
  void putId(EiffelEvent event);
}
