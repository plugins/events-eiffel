// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto;

/**
 * A description of the source of the event. This object is primarily for traceability purposes, and
 * while optional, some form of identification of the source is HIGHLY RECOMMENDED. It offers
 * multiple methods of identifying the source of the event, techniques which may be selected from
 * based on the technology domain and needs in any particular use case.
 *
 * <p>As declared by:
 * https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelSourceChangeCreatedEvent.md#metasource
 */
public class EiffelSourceInfo {
  public static Builder builder() {
    return new Builder();
  }
  /**
   * Identifies the domain that produced an event. A domain is an infrastructure topological
   * concept, which may or may not corresponds to an organization or product structures. A good
   * example would be Java packages notation, ex. com.mycompany.product.component or
   * mycompany.site.division. Also, keep in mind that all names are more or less prone to change.
   * Particularly, it is recommended to avoid organizational names or site names, as organizations
   * tend to be volatile and development is easily relocated. Relatively speaking, product and
   * component names tend to be more stable and are therefore encouraged, while code names may be an
   * option. You need to decide what is the most sensible option in your case.
   */
  public final String domainId;
  /** The hostname of the event sender. */
  public final String host;
  /** The name of the event sender. */
  public final String name;
  /**
   * The identity of the serializer software used to construct the event, in <a
   * href="https://github.com/package-url/purl-spec">purl format</a>.
   */
  public final String serializer;
  /** The URI of, related to or describing the event sender. */
  public final String uri;

  private EiffelSourceInfo(
      String domainId, String host, String name, String serializer, String uri) {
    this.domainId = domainId;
    this.host = host;
    this.name = name;
    this.serializer = serializer;
    this.uri = uri;
  }

  public static class Builder {
    private String _domainId;
    private String _host;
    private String _name;
    private String _serializer;
    private String _uri;

    private Builder() {}

    public Builder domainId(String domainId) {
      this._domainId = domainId;
      return this;
    }

    public Builder host(String host) {
      this._host = host;
      return this;
    }

    public Builder name(String name) {
      this._name = name;
      return this;
    }

    public Builder serializer(String serializer) {
      this._serializer = serializer;
      return this;
    }

    public Builder uri(String uri) {
      this._uri = uri;
      return this;
    }

    public EiffelSourceInfo build() {
      return new EiffelSourceInfo(_domainId, _host, _name, _serializer, _uri);
    }
  }
}
