// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel;

import static com.google.common.base.Preconditions.checkState;

import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelCompositionDefinedEventInfo;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEvent;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType;
import java.util.Objects;

/**
 * Identifies a CompositionDefinedEvent.
 *
 * @author davidak
 */
public class CompositionDefinedEventKey extends EventKey {
  public static CompositionDefinedEventKey create(String name, String version) {
    return new CompositionDefinedEventKey(name, version);
  }

  public static CompositionDefinedEventKey fromEvent(EiffelEvent event) {
    switch (event.meta.type) {
      case CD:
        EiffelCompositionDefinedEventInfo.DataInfo cdData =
            ((EiffelCompositionDefinedEventInfo) event).data;
        checkState(cdData != null, "data must not be null.");
        return new CompositionDefinedEventKey(cdData.name, cdData.version);
      default:
      case ARTC:
      case SCC:
      case SCS:
        throw new IllegalArgumentException(
            "Not a CompositionDefined* event type: " + event.meta.type.toString());
    }
  }

  private final String name;
  private final String version;

  private CompositionDefinedEventKey(String name, String version) {
    super(EiffelEventType.CD);
    checkState(name != null, "name must not be null");
    checkState(version != null, "version must not be null");
    this.name = name;
    this.version = version;
  }

  public String name() {
    return name;
  }

  public String version() {
    return version;
  }

  @Override
  public boolean equals(Object other) {
    if (other == null || !(other instanceof CompositionDefinedEventKey)) {
      return false;
    }
    CompositionDefinedEventKey o = (CompositionDefinedEventKey) other;
    return o.name.equals(this.name) && o.version.equals(this.version);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, version, type);
  }

  @Override
  public String toString() {
    return String.format("%s:[%s, %s]", type.name(), name, version);
  }
}
