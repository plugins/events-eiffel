// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.config;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.flogger.FluentLogger;
import com.google.gerrit.extensions.annotations.PluginName;
import com.google.gerrit.server.config.AllProjectsName;
import com.google.gerrit.server.config.PluginConfigFactory;
import com.google.gerrit.server.project.NoSuchProjectException;
import com.google.inject.Inject;
import org.eclipse.jgit.lib.Config;

public class EventListenersConfig {
  public static class Provider implements com.google.inject.Provider<EventListenersConfig> {
    private static final FluentLogger logger = FluentLogger.forEnclosingClass();
    static final String EVENT_LISTENERS = "EventListeners";
    static final String ENABLED = "enabled";

    @VisibleForTesting
    static EventListenersConfig fromConfig(Config cfg) {
      return new EventListenersConfig(cfg.getBoolean(EVENT_LISTENERS, ENABLED, true));
    }

    private final PluginConfigFactory configFactory;
    private final String pluginName;
    private final AllProjectsName allProjects;

    @Inject
    public Provider(
        PluginConfigFactory cfgFactory,
        @PluginName String pluginName,
        AllProjectsName allProjects) {
      this.configFactory = cfgFactory;
      this.pluginName = pluginName;
      this.allProjects = allProjects;
    }

    @Override
    public EventListenersConfig get() {
      try {
        Config cfg = configFactory.getProjectPluginConfig(allProjects, pluginName);
        return fromConfig(cfg);
      } catch (NoSuchProjectException e) {
        logger.atSevere().withCause(e).log(
            "Unable to read project.config, using default EventListenersConfig");
        return new EventListenersConfig(true);
      }
    }
  }

  private final boolean enabled;

  @VisibleForTesting
  public EventListenersConfig(boolean enabled) {
    this.enabled = enabled;
  }

  public boolean isEnabled() {
    return this.enabled;
  }
}
