// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto;

/**
 * A summary of the change.
 *
 * <p>As declared by:
 * https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelSourceChangeCreatedEvent.md#datachange
 */
public class EiffelChangeInfo {

  public static Builder builder() {
    return new Builder();
  }

  /** The number of inserted lines in the change. */
  public final Integer insertions;
  /** The number of deleted lines in the change. */
  public final Integer deletions;
  /** A URI to a list of files changed, on JSON String array format. */
  public final String files;
  /**
   * A URI to further details about the change. These details are not assumed to be on any
   * standardized format, and may be intended for human and/or machine consumption. Examples include
   * e.g. Gerrit patch set descriptions or GitHub commit pages. It is recommended to also include
   * data.change.tracker to provide a hint as to the nature of the linked details.
   */
  public final String details;
  /** The name of the tracker, if any, of the change. Examples include e.g. Gerrit or GitHub. */
  public final String tracker;
  /**
   * The unique identity, if any, of the change (apart from what is expressed in the identifier
   * object). Examples include e.g. Gerrit Change-Ids or GitHub Pull Requests. It is recommended to
   * also include data.change.tracker to provide a hint as to the nature of the identity.
   */
  public final String id;

  private EiffelChangeInfo(
      Integer insertions,
      Integer deletions,
      String files,
      String details,
      String tracker,
      String id) {
    this.insertions = insertions;
    this.deletions = deletions;
    this.files = files;
    this.details = details;
    this.tracker = tracker;
    this.id = id;
  }

  public static class Builder {
    private Integer _insertions = null;
    private Integer _deletions = null;
    private String _files = null;
    private String _details = null;
    private String _tracker = null;
    private String _id = null;

    private Builder() {}

    public Builder insertions(int insertions) {
      _insertions = insertions;
      return this;
    }

    public Builder deletions(int deletions) {
      _deletions = deletions;
      return this;
    }

    public Builder files(String files) {
      _files = files;
      return this;
    }

    public Builder details(String details) {
      _details = details;
      return this;
    }

    public Builder tracker(String tracker) {
      _tracker = tracker;
      return this;
    }

    public Builder id(String id) {
      _id = id;
      return this;
    }

    public EiffelChangeInfo build() {
      return new EiffelChangeInfo(_insertions, _deletions, _files, _details, _tracker, _id);
    }
  }
}
