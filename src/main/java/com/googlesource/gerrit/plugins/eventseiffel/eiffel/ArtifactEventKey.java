// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel;

import static com.google.common.base.Preconditions.checkState;

import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelArtifactCreatedEventInfo;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEvent;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType;
import java.util.Objects;

/**
 * Identifies a ArtifactCreatedEvent.
 *
 * @author davidak
 */
public class ArtifactEventKey extends EventKey {
  public static ArtifactEventKey create(String identity) {
    return new ArtifactEventKey(identity);
  }

  public static ArtifactEventKey fromEvent(EiffelEvent event) {
    switch (event.meta.type) {
      case ARTC:
        EiffelArtifactCreatedEventInfo.DataInfo artcData =
            ((EiffelArtifactCreatedEventInfo) event).data;
        checkState(artcData != null, "data must not be null.");
        return new ArtifactEventKey(artcData.identity);
      default:
      case CD:
      case SCC:
      case SCS:
        throw new IllegalArgumentException(
            "Not a Artifact* event type: " + event.meta.type.toString());
    }
  }

  private final String identity;

  private ArtifactEventKey(String identity) {
    super(EiffelEventType.ARTC);
    checkState(identity != null, "identity must not be null");
    this.identity = identity;
  }

  public String identity() {
    return identity;
  }

  @Override
  public boolean equals(Object other) {
    if (other == null || !(other instanceof ArtifactEventKey)) {
      return false;
    }
    ArtifactEventKey o = (ArtifactEventKey) other;
    return o.identity.equals(this.identity);
  }

  @Override
  public int hashCode() {
    return Objects.hash(identity, type);
  }

  @Override
  public String toString() {
    return String.format("%s:%s", type.name(), identity);
  }
}
