// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.parsing;

import static com.google.common.util.concurrent.MoreExecutors.directExecutor;

import com.google.common.annotations.VisibleForTesting;
import com.google.gerrit.server.git.ProjectRunnable;
import com.google.gerrit.server.git.WorkQueue;
import com.google.inject.Inject;
import com.googlesource.gerrit.plugins.eventseiffel.config.EventParsingConfig;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public interface EiffelEventParsingExecutor {
  public ScheduledFuture<?> schedule(ProjectRunnable runnable);

  public void shutDown();

  public static class Scheduled implements EiffelEventParsingExecutor {
    private volatile ScheduledThreadPoolExecutor pool;

    @Inject
    public Scheduled(WorkQueue workQueue, EventParsingConfig config) {
      pool = workQueue.createQueue(config.poolSize(), "Eiffel-event-parsing", false);
      pool.setRemoveOnCancelPolicy(true);
    }

    @Override
    public ScheduledFuture<?> schedule(ProjectRunnable runnable) {
      if (pool != null) {
        return pool.schedule(runnable, 0, TimeUnit.SECONDS);
      }
      return null;
    }

    @Override
    public void shutDown() {
      pool.shutdownNow();
    }
  }

  @VisibleForTesting
  public static class Direct implements EiffelEventParsingExecutor {

    @Override
    public ScheduledFuture<?> schedule(ProjectRunnable runnable) {
      directExecutor().execute(runnable);
      return null;
    }

    @Override
    public void shutDown() {}
  }
}
