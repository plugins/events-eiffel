// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto;

import static com.google.common.base.Preconditions.checkState;

import com.google.common.collect.ImmutableSet;
import java.util.Optional;
import java.util.Set;

/**
 * The <a
 * href="https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelCompositionDefinedEvent.md">EiffelCompositionDefinedEvent</a>
 * declares a composition of items (artifacts, sources and other compositions) has been defined,
 * typically with the purpose of enabling further downstream artifacts to be generated.
 *
 * <p>As declared by:
 * https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelCompositionDefinedEvent.md
 */
public class EiffelCompositionDefinedEventInfo extends EiffelEvent {

  /**
   * See <a
   * href="https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelCompositionDefinedEvent.md#version-history">Event
   * version history</a>.
   */
  public static final String EVENT_VERSION = "3.0.0";

  public static final EiffelEventType EVENT_TYPE = EiffelEventType.CD;
  /**
   * https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelCompositionDefinedEvent.md#links
   */
  public static final Set<EiffelLinkType> SUPPORTED_LINKS =
      ImmutableSet.of(
          EiffelLinkType.CAUSE,
          EiffelLinkType.CONTEXT,
          EiffelLinkType.ELEMENT,
          EiffelLinkType.FLOW_CONTEXT,
          EiffelLinkType.PREVIOUS_VERSION);

  public static Builder builder() {
    return new Builder();
  }

  public DataInfo data;

  private EiffelCompositionDefinedEventInfo(
      DataInfo data, EiffelMetaInfo meta, EiffelLinkInfo[] links) {
    super(meta, links);
    this.data = data;
  }

  public static class Builder
      extends EiffelEvent.Builder<EiffelCompositionDefinedEventInfo, Builder> {
    private String _name = null;
    private Optional<String> _version = Optional.empty();

    public Builder name(String name) {
      _name = name;
      return this;
    }

    public Builder version(String version) {
      _version = Optional.of(version);
      return this;
    }

    @Override
    public EiffelCompositionDefinedEventInfo build() {
      checkState(_name != null, "name is required");
      return new EiffelCompositionDefinedEventInfo(
          new DataInfo(_name, _version.orElse(null)), buildMeta(), buildLinks());
    }

    @Override
    protected String getEventVersion() {
      return EVENT_VERSION;
    }

    @Override
    protected EiffelEventType getEventType() {
      return EVENT_TYPE;
    }

    @Override
    protected Set<EiffelLinkType> supportedLinks() {
      return SUPPORTED_LINKS;
    }
  }

  public static class DataInfo {
    public final String name;
    public final String version;

    private DataInfo(String name, String version) {
      this.name = name;
      this.version = version;
    }
  }
}
