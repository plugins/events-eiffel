// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.cache;

import static com.google.common.base.Preconditions.checkState;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.gerrit.server.cache.CacheModule;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.google.inject.TypeLiteral;
import com.google.inject.name.Named;
import com.googlesource.gerrit.plugins.eventseiffel.config.EventIdCacheConfig;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.EventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.SourceChangeEventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.api.EventStorage;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.api.EventStorageException;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEvent;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class EiffelEventIdCacheImpl implements EiffelEventIdCache {
  public static final String CACHE_NAME = "eiffel_event_id";

  @VisibleForTesting
  public static Module module() {
    return module(1000);
  }

  public static Module module(int maxNbrOfEntries) {
    return new CacheModule() {
      @Override
      public void configure() {
        persist(CACHE_NAME, EventKey.class, new TypeLiteral<Optional<UUID>>() {})
            .maximumWeight(maxNbrOfEntries)
            .loader(EventLoader.class)
            .keySerializer(new EventKeySerializer())
            .valueSerializer(new UUIDSerializer());
        bind(EiffelEventIdCacheImpl.class).asEagerSingleton();
        bind(EiffelEventIdCache.class).to(EiffelEventIdCacheImpl.class);
      }
    };
  }

  private LoadingCache<EventKey, Optional<UUID>> eventIds;
  private final EventIdCacheConfig cfg;
  private final EventStorage eventStorage;

  @Inject
  public EiffelEventIdCacheImpl(
      @Named(CACHE_NAME) LoadingCache<EventKey, Optional<UUID>> eventIds,
      EventIdCacheConfig cfg,
      EventStorage eventStorage) {
    this.eventIds = eventIds;
    this.cfg = cfg;
    this.eventStorage = eventStorage;
  }

  @Override
  public Optional<UUID> getEventId(EventKey key) throws EiffelEventIdLookupException {
    try {
      if (cfg.trustLocalCache()) {
        Optional<UUID> fromCache = eventIds.getIfPresent(key);
        return fromCache != null ? fromCache : Optional.empty();
      }
      return eventIds.get(key);

    } catch (ExecutionException e) {
      throw new EiffelEventIdLookupException(
          e,
          key.type(),
          String.format("failed to lookup Eiffel event id for %s from GraphQl", key));
    }
  }

  private Optional<UUID> getEventIdIfPresent(EventKey key) {
    Optional<UUID> fromCache = eventIds.getIfPresent(key);
    if (fromCache == null) {
      return Optional.empty();
    }
    return fromCache;
  }

  @Override
  public Optional<UUID> getScsForCommit(String repo, String commit, List<String> branches)
      throws EiffelEventIdLookupException {
    List<SourceChangeEventKey> keys =
        branches.stream()
            .map(branch -> SourceChangeEventKey.scsKey(repo, branch, commit))
            .collect(Collectors.toList());
    Optional<UUID> id =
        keys.stream()
            .map(key -> getEventIdIfPresent(key))
            .filter(Optional::isPresent)
            .findAny()
            .orElse(Optional.empty());
    if (id.isPresent()) return id;
    if (cfg.trustLocalCache()) {
      return Optional.empty();
    }

    List<UUID> ids;
    try {
      ids = eventStorage.getScsIds(repo, commit);
    } catch (EventStorageException e) {
      throw new EiffelEventIdLookupException(
          e,
          EiffelEventType.SCS,
          String.format(
              "failed to lookup Eiffel event id for event with repo %s and commit %s from GraphQl",
              repo, commit));
    }
    return !ids.isEmpty() ? Optional.of(ids.get(0)) : Optional.empty();
  }

  @Override
  public Optional<List<UUID>> getParentLinks(EventKey key) throws EiffelEventIdLookupException {
    try {
      return eventStorage.getParentLinks(key);
    } catch (EventStorageException e) {
      throw new EiffelEventIdLookupException(
          e,
          key.type(),
          String.format("failed to lookup Eiffel event links for %s from GraphQl", key));
    }
  }

  @Override
  public Optional<UUID> getSccEventLink(SourceChangeEventKey key)
      throws EiffelEventIdLookupException {
    try {
      return eventStorage.getSccEventLink(key);
    } catch (EventStorageException e) {
      throw new EiffelEventIdLookupException(
          e,
          key.type(),
          String.format("failed to lookup Eiffel SCC event link for %s from GraphQl", key));
    }
  }

  /* (non-Javadoc)
   * @see com.googlesource.gerrit.plugins.eventseiffel.EiffelEventIdCache#putId(com.googlesource.gerrit.plugins.eventseiffel.eiffel.EiffelEvent)
   */
  @Override
  public void putId(EiffelEvent event) {
    checkState(event.meta.id != null, "meta.id must not be null");
    eventIds.put(EventKey.fromEvent(event), Optional.of(event.meta.id));
  }

  static class EventLoader extends CacheLoader<EventKey, Optional<UUID>> {
    private final EventStorage es;

    @Inject
    public EventLoader(EventStorage es) {
      this.es = es;
    }

    @Override
    public Optional<UUID> load(EventKey key) throws Exception {
      return es.getEventId(key);
    }
  }
}
