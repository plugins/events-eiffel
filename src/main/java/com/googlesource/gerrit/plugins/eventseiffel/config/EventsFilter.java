// Copyright (C) 2022 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package com.googlesource.gerrit.plugins.eventseiffel.config;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.flogger.FluentLogger;
import com.google.gerrit.entities.AccessSection;
import com.google.gerrit.extensions.annotations.PluginName;
import com.google.gerrit.server.config.AllProjectsName;
import com.google.gerrit.server.config.PluginConfigFactory;
import com.google.gerrit.server.project.NoSuchProjectException;
import com.google.inject.Inject;
import java.util.Arrays;
import java.util.function.Function;
import java.util.regex.Pattern;
import org.eclipse.jgit.lib.Config;

public class EventsFilter {

  public static class Provider implements com.google.inject.Provider<EventsFilter> {
    private static final FluentLogger logger = FluentLogger.forEnclosingClass();
    static final String EVENTS_FILTER = "EventsFilter";
    static final String BLOCKED_REFS = "blockedRef";
    static final String BLOCKED_PROJECTS = "blockedProject";
    static final String BLOCK_LIGHTWEIGHT_TAGS = "blockLightWeightTags";

    @VisibleForTesting
    static boolean filterMatches(String filter, String repoName) {
      if (filter.startsWith(AccessSection.REGEX_PREFIX)) {
        return Pattern.matches(filter, repoName);
      }
      if (filter.endsWith("*")) {
        return repoName.startsWith(filter.substring(0, filter.length() - 1));
      }
      return repoName.equals(filter);
    }

    @VisibleForTesting
    public static Function<String, Boolean> toCombinedMatcher(String... filters) {
      return repoName -> Arrays.stream(filters).anyMatch(f -> filterMatches(f, repoName));
    }

    @VisibleForTesting
    static EventsFilter fromConfig(Config cfg) {
      return new EventsFilter(
          toCombinedMatcher(cfg.getStringList(EVENTS_FILTER, null, BLOCKED_PROJECTS)),
          toCombinedMatcher(cfg.getStringList(EVENTS_FILTER, null, BLOCKED_REFS)),
          cfg.getBoolean(EVENTS_FILTER, BLOCK_LIGHTWEIGHT_TAGS, false));
    }

    private final PluginConfigFactory configFactory;
    private final String pluginName;
    private final AllProjectsName allProjects;

    @Inject
    public Provider(
        PluginConfigFactory cfgFactory,
        @PluginName String pluginName,
        AllProjectsName allProjects) {
      this.configFactory = cfgFactory;
      this.pluginName = pluginName;
      this.allProjects = allProjects;
    }

    @Override
    public EventsFilter get() {
      try {
        Config cfg = configFactory.getProjectPluginConfig(allProjects, pluginName);
        return fromConfig(cfg);
      } catch (NoSuchProjectException e) {
        logger.atSevere().withCause(e).log(
            "Unable to read project.config, using default EventsFilter");
        return new EventsFilter(toCombinedMatcher(""), toCombinedMatcher(""), false);
      }
    }
  }

  private final Function<String, Boolean> blockedRefMatcher;
  private final Function<String, Boolean> blockedProjectMatcher;
  private final boolean blockLightWeightTags;

  @VisibleForTesting
  public EventsFilter(
      Function<String, Boolean> blockedProjectMatcher,
      Function<String, Boolean> blockedRefMatcher,
      boolean blockLightWeightTags) {
    this.blockedProjectMatcher = blockedProjectMatcher;
    this.blockedRefMatcher = blockedRefMatcher;
    this.blockLightWeightTags = blockLightWeightTags;
  }

  public boolean refIsBlocked(String ref) {
    return blockedRefMatcher.apply(ref);
  }

  public boolean projectIsBlocked(String repoName) {
    return blockedProjectMatcher.apply(repoName);
  }

  public boolean blockLightWeightTags() {
    return blockLightWeightTags;
  }
}
