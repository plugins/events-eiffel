// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel.api;

import com.google.gerrit.extensions.restapi.NotImplementedException;
import com.google.inject.Singleton;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.EventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.SourceChangeEventKey;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface EventStorage {

  /**
   * Returns the meta.id of the SCC or SCS event that matches EventKey key. Optional.empty() if
   * there exists no such event.
   *
   * @param key
   * @return {@link Optional} of the {@link UUID} id of the event.
   * @throws EventStorageException
   */
  public Optional<UUID> getEventId(EventKey key) throws EventStorageException;

  /**
   * Returns the links.target for links where links.type is PREVIOUS_VERSION for the event that
   * matches EventKey key. Optional.empty() if there exists no such event.
   *
   * @param key
   * @return {@link Optional} of {@link List} of {@link UUID} ids of the linked events.
   * @throws EventStorageException
   */
  public Optional<List<UUID>> getParentLinks(EventKey key) throws EventStorageException;

  /**
   * Returns the links.target for links where links.type is CHANGE for the event that matches
   * EventKey key. Optional.empty() if there exists no such event or if key is not of the type SCS.
   *
   * @param key
   * @return {@link Optional} of {@link UUID} id of the linked event.
   * @throws EventStorageException
   */
  public Optional<UUID> getSccEventLink(SourceChangeEventKey key) throws EventStorageException;

  /**
   * Returns the meta.ids of the SCS events that match the repo and the commitid.
   *
   * @param repo the repo of the SCS events.
   * @param commit the commit of the SCS events.
   * @return {@link List} of {@link UUID} ids of the events.
   * @throws EventStorageException
   */
  public List<UUID> getScsIds(String repo, String commit) throws EventStorageException;

  @Singleton
  public static class NoOpEventStorage implements EventStorage {

    @Override
    public Optional<UUID> getEventId(EventKey key) throws EventStorageException {
      throw new NotImplementedException();
    }

    @Override
    public Optional<List<UUID>> getParentLinks(EventKey key) throws EventStorageException {
      throw new NotImplementedException();
    }

    @Override
    public Optional<UUID> getSccEventLink(SourceChangeEventKey key) throws EventStorageException {
      throw new NotImplementedException();
    }

    @Override
    public List<UUID> getScsIds(String repo, String commit) throws EventStorageException {
      throw new NotImplementedException();
    }
  }
}
