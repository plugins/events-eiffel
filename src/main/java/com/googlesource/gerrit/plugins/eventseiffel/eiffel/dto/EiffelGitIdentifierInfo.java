// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto;

import static com.google.common.base.Preconditions.checkState;

import java.util.regex.Pattern;

/**
 * Identifier of a Git change.
 *
 * <p>As declared by:
 * https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelSourceChangeCreatedEvent.md#datagitidentifier
 * https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelSourceChangeSubmittedEvent.md#datagitidentifier
 */
public class EiffelGitIdentifierInfo {
  public static Builder builder() {
    return new Builder();
  }

  private static Pattern SHA1_FORMAT = Pattern.compile("^[a-fA-F0-9]{40}$");

  /** The commit identity (hash) of the change. */
  public final String commitId;
  /** The name of the branch where the change was made. */
  public final String branch;
  /** The name of the repository containing the change. */
  public final String repoName;
  /** The URI of the repository containing the change. */
  public final String repoUri;

  private EiffelGitIdentifierInfo(String commitId, String branch, String repoName, String repoUri) {
    this.commitId = commitId;
    this.branch = branch;
    this.repoName = repoName;
    this.repoUri = repoUri;
  }

  public static class Builder {
    private String _commitId = null;
    private String _branch = null;
    private String _repoName = null;
    private String _repoUri = null;

    private Builder() {}

    public Builder commitId(String commitId) {
      checkState(SHA1_FORMAT.matcher(commitId).matches(), "commitId must be a valid SHA1");

      _commitId = commitId;
      return this;
    }

    public Builder branch(String branch) {
      _branch = branch;
      return this;
    }

    public Builder repoName(String repoName) {
      _repoName = repoName;
      return this;
    }

    public Builder repoUri(String repoUri) {
      _repoUri = repoUri;
      return this;
    }

    public EiffelGitIdentifierInfo build() {
      checkState(_commitId != null, "commitId is required");
      checkState(_repoUri != null, "repoUri is required");
      return new EiffelGitIdentifierInfo(_commitId, _branch, _repoName, _repoUri);
    }
  }
}
