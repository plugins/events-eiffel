// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto;

import com.google.common.collect.ImmutableSet;
import java.util.Optional;
import java.util.Set;

/**
 * The <a
 * href="https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelSourceChangeSubmittedEvent.md">EiffelSourceChangeSubmittedEvent</a>
 * declares that a change has been integrated into to a shared source branch of interest (e.g.
 * "master", "dev" or "mainline") as opposed to a private or local branch. Note that it does not
 * describe what has changed, but instead uses the CHANGE link type to reference
 * EiffelSourceChangeCreatedEvent.
 *
 * <p>Typical use cases for EiffelSourceChangeSubmittedEvent is to signal that a patch has passed
 * code review and been submitted or that a feature/topic/team branch has been merged into the
 * mainline/trunk/master. Where changes are made directly on the mainline, it is recommended to send
 * both EiffelSourceChangeCreatedEvent and EiffelSourceChangeSubmittedEvent together for information
 * completeness.
 *
 * <p>Even though multiple types of identifiers are available (see below), the event SHOULD include
 * one and SHALL not include more than one; changes to multiple repositories are represented by
 * multiple events.
 *
 * <p>As declared by:
 * https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelSourceChangeSubmittedEvent.md
 */
public class EiffelSourceChangeSubmittedEventInfo extends EiffelEvent {

  /**
   * See <a
   * href="https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelSourceChangeSubmittedEvent.md#version-history">Event
   * version history</a>.
   */
  public static final String EVENT_VERSION = "3.0.0";

  public static final EiffelEventType EVENT_TYPE = EiffelEventType.SCS;
  /**
   * https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelSourceChangeSubmittedEvent.md#links
   */
  public static final Set<EiffelLinkType> SUPPORTED_LINKS =
      ImmutableSet.of(
          EiffelLinkType.CAUSE,
          EiffelLinkType.CHANGE,
          EiffelLinkType.CONTEXT,
          EiffelLinkType.FLOW_CONTEXT,
          EiffelLinkType.PREVIOUS_VERSION);

  public static Builder builder() {
    return new Builder();
  }

  public DataInfo data;

  private EiffelSourceChangeSubmittedEventInfo(
      DataInfo data, EiffelMetaInfo meta, EiffelLinkInfo[] links) {
    super(meta, links);
    this.data = data;
  }

  public static class Builder
      extends EiffelEvent.Builder<EiffelSourceChangeSubmittedEventInfo, Builder> {
    private Optional<EiffelPersonInfo.Builder> _submitter = Optional.empty();
    private Optional<EiffelGitIdentifierInfo.Builder> _gitIdentifier = Optional.empty();

    public Builder gitIdentifier(EiffelGitIdentifierInfo.Builder gitIdentifier) {
      _gitIdentifier = Optional.of(gitIdentifier);
      return this;
    }

    public Builder submitter(EiffelPersonInfo.Builder author) {
      _submitter = Optional.of(author);
      return this;
    }

    @Override
    public EiffelSourceChangeSubmittedEventInfo build() {
      return new EiffelSourceChangeSubmittedEventInfo(
          _gitIdentifier.isEmpty() && _submitter.isEmpty()
              ? null
              : new DataInfo(
                  _gitIdentifier.map(EiffelGitIdentifierInfo.Builder::build).orElse(null),
                  _submitter.map(EiffelPersonInfo.Builder::build).orElse(null)),
          buildMeta(),
          buildLinks());
    }

    @Override
    protected String getEventVersion() {
      return EVENT_VERSION;
    }

    @Override
    protected EiffelEventType getEventType() {
      return EVENT_TYPE;
    }

    @Override
    protected Set<EiffelLinkType> supportedLinks() {
      return SUPPORTED_LINKS;
    }
  }

  public static class DataInfo {
    public final EiffelGitIdentifierInfo gitIdentifier;
    public final EiffelPersonInfo submitter;

    private DataInfo(EiffelGitIdentifierInfo gitIdentifier, EiffelPersonInfo submitter) {
      this.gitIdentifier = gitIdentifier;
      this.submitter = submitter;
    }
  }
}
