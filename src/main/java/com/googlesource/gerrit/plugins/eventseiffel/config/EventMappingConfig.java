package com.googlesource.gerrit.plugins.eventseiffel.config;

import com.google.gerrit.extensions.annotations.PluginName;
import com.google.gerrit.server.config.PluginConfigFactory;
import com.google.inject.Inject;
import org.eclipse.jgit.lib.Config;

public class EventMappingConfig {
  public static class Provider implements com.google.inject.Provider<EventMappingConfig> {
    private static final String EVENT_MAPPING = "EventMapping";
    private static final String USE_INDEX = "useIndex";
    private static final boolean DEFAULT_USE_INDEX = true;
    private final EventMappingConfig config;

    @Inject
    public Provider(PluginConfigFactory cfgFactory, @PluginName String pluginName) {
      Config cfg = cfgFactory.getGlobalPluginConfig(pluginName);
      config = new EventMappingConfig(cfg.getBoolean(EVENT_MAPPING, USE_INDEX, DEFAULT_USE_INDEX));
    }

    @Override
    public EventMappingConfig get() {
      return config;
    }
  }

  private final boolean useIndex;

  protected EventMappingConfig(boolean useIndex) {
    this.useIndex = useIndex;
  }

  public boolean useIndex() {
    return useIndex;
  }
}
