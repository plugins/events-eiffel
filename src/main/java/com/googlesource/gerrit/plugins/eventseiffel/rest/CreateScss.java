// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.rest;

import com.google.gerrit.common.data.GlobalCapability;
import com.google.gerrit.entities.RefNames;
import com.google.gerrit.extensions.annotations.RequiresCapability;
import com.google.gerrit.extensions.restapi.AuthException;
import com.google.gerrit.extensions.restapi.BadRequestException;
import com.google.gerrit.extensions.restapi.ResourceConflictException;
import com.google.gerrit.extensions.restapi.Response;
import com.google.gerrit.extensions.restapi.RestModifyView;
import com.google.gerrit.server.project.BranchResource;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.googlesource.gerrit.plugins.eventseiffel.parsing.EiffelEventParsingQueue;

@Singleton
@RequiresCapability(GlobalCapability.ADMINISTRATE_SERVER)
public class CreateScss implements RestModifyView<BranchResource, CreateScss.Input> {
  static class Input {
    boolean fillGaps;
  }

  private final EiffelEventParsingQueue queue;

  @Inject
  CreateScss(EiffelEventParsingQueue queue) {
    this.queue = queue;
  }

  @Override
  public Response<?> apply(BranchResource resource, CreateScss.Input input)
      throws AuthException, BadRequestException, ResourceConflictException, Exception {
    if (resource.getRef().startsWith(RefNames.REFS_HEADS)) {
      if (input.fillGaps) {
        queue.scheduleMissingScsCreation(resource.getName(), resource.getRef());
      } else {
        queue.scheduleScsCreation(resource.getName(), resource.getRef());
      }
      return EventCreationResponse.scs(resource);
    }
    throw new BadRequestException("SCS event creation are only allowed from branches.");
  }
}
