// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.config;

import com.google.common.flogger.FluentLogger;
import com.google.gerrit.extensions.annotations.PluginName;
import com.google.gerrit.server.config.PluginConfigFactory;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.eclipse.jgit.lib.Config;

@Singleton
public class EventIdCacheConfig {
  private static final FluentLogger logger = FluentLogger.forEnclosingClass();

  @Singleton
  public static class Provider implements com.google.inject.Provider<EventIdCacheConfig> {
    private static String EVENT_ID_CACHE = "EventIdCache";
    private static String TRUST_LOCAL_CACHE = "trustLocalCache";
    private static String MAX_NBR_ENTRIES = "maxNbrOfEntries";
    private static int DEFAULT_MAX_ENTRIES = 10000000;

    private final EventIdCacheConfig config;

    @Inject
    public Provider(PluginConfigFactory cfgFactory, @PluginName String pluginName) {
      Config cfg = cfgFactory.getGlobalPluginConfig(pluginName);
      this.config =
          new EventIdCacheConfig(
              cfg.getBoolean(EVENT_ID_CACHE, TRUST_LOCAL_CACHE, false),
              cfg.getInt(EVENT_ID_CACHE, MAX_NBR_ENTRIES, DEFAULT_MAX_ENTRIES));
    }

    @Override
    public EventIdCacheConfig get() {
      return config;
    }
  }

  private final boolean trustLocalCache;
  private final int maxNbrOfEntries;

  public EventIdCacheConfig(boolean trustLocalCache, int maxNbrOfEntries) {
    this.trustLocalCache = trustLocalCache;
    this.maxNbrOfEntries = maxNbrOfEntries;
    logger.atInfo().log("Event-id cache max-size is set to: %d", maxNbrOfEntries);
  }

  public boolean trustLocalCache() {
    return trustLocalCache;
  }

  public int maxNbrOfEntries() {
    return maxNbrOfEntries;
  }
}
