// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.rest;

import static com.google.gerrit.server.project.BranchResource.BRANCH_KIND;
import static com.google.gerrit.server.project.TagResource.TAG_KIND;

import com.google.gerrit.extensions.restapi.RestApiModule;

public class RestModule extends RestApiModule {

  @Override
  public void configure() {
    post(BRANCH_KIND, "createSccs").to(CreateSccs.class);
    post(BRANCH_KIND, "createScss").to(CreateScss.class);
    post(TAG_KIND, "createArtcs").to(CreateArtcs.class);
  }
}
