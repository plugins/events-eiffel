// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto;

import static com.google.common.base.Preconditions.checkState;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;

/**
 * The <a
 * href="https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelArtifactCreatedEvent.md">EiffelArtifactCreatedEvent</a>
 * declares that a software artifact has been created, what its coordinates are, what it contains
 * and how it was created.
 *
 * <p>As declared by:
 * https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelArtifactCreatedEvent.md
 */
public class EiffelArtifactCreatedEventInfo extends EiffelEvent {

  /**
   * See <a
   * href="https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelArtifactCreatedEvent.md#version-history">Event
   * version history</a>.
   */
  public static final String EVENT_VERSION = "3.0.0";

  public static final EiffelEventType EVENT_TYPE = EiffelEventType.ARTC;
  /**
   * https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelArtifactCreatedEvent.md#links
   */
  public static final Set<EiffelLinkType> SUPPORTED_LINKS =
      ImmutableSet.of(
          EiffelLinkType.CAUSE,
          EiffelLinkType.COMPOSITION,
          EiffelLinkType.CONTEXT,
          EiffelLinkType.ENVIRONMENT,
          EiffelLinkType.FLOW_CONTEXT,
          EiffelLinkType.PREVIOUS_VERSION);

  public static Builder builder() {
    return new Builder();
  }

  public DataInfo data;

  private EiffelArtifactCreatedEventInfo(
      DataInfo data, EiffelMetaInfo meta, EiffelLinkInfo[] links) {
    super(meta, links);
    this.data = data;
  }

  public static class Builder extends EiffelEvent.Builder<EiffelArtifactCreatedEventInfo, Builder> {
    private String _identity = null;
    private Optional<ArrayList<EiffelFileInfo.Builder>> _fileInformation = Optional.empty();
    private Optional<String> _buildCommand = Optional.empty();
    private Optional<String> _requiresImplementation = Optional.empty();
    private Optional<ArrayList<String>> _implementsField = Optional.empty();
    private Optional<ArrayList<String>> _dependsOn = Optional.empty();
    private Optional<String> _name = Optional.empty();

    public Builder identity(String identity) {
      _identity = identity;
      return this;
    }

    public Builder fileInformation(EiffelFileInfo.Builder fileInfo) {
      _fileInformation = _fileInformation.or(() -> Optional.of(Lists.newArrayList()));
      _fileInformation.get().add(fileInfo);
      return this;
    }

    public Builder buildCommand(String buildCommand) {
      _buildCommand = Optional.of(buildCommand);
      return this;
    }

    public Builder requiresImplementation(String requiresImplementation) {
      _requiresImplementation = Optional.of(requiresImplementation);
      return this;
    }

    public Builder addImplements(String implementsField) {
      _implementsField = _implementsField.or(() -> Optional.of(Lists.newArrayList()));
      _implementsField.get().add(implementsField);
      return this;
    }

    public Builder addDependancy(String dependancy) {
      _dependsOn = _dependsOn.or(() -> Optional.of(Lists.newArrayList()));
      _dependsOn.get().add(dependancy);
      return this;
    }

    public Builder name(String name) {
      _name = Optional.of(name);
      return this;
    }

    @Override
    public EiffelArtifactCreatedEventInfo build() {
      checkState(_identity != null, "identity is required");
      return new EiffelArtifactCreatedEventInfo(
          new DataInfo(
              _identity,
              _fileInformation
                  .map(fi -> fi.stream().map(i -> i.build()).toArray(EiffelFileInfo[]::new))
                  .orElse(null),
              _buildCommand.orElse(null),
              _requiresImplementation.orElse(null),
              _implementsField.map(i -> i.stream().toArray(String[]::new)).orElse(null),
              _dependsOn.map(d -> d.stream().toArray(String[]::new)).orElse(null),
              _name.orElse(null)),
          buildMeta(),
          buildLinks());
    }

    @Override
    protected String getEventVersion() {
      return EVENT_VERSION;
    }

    @Override
    protected EiffelEventType getEventType() {
      return EVENT_TYPE;
    }

    @Override
    protected Set<EiffelLinkType> supportedLinks() {
      return SUPPORTED_LINKS;
    }
  }

  public static class DataInfo {
    public final String identity;
    public final EiffelFileInfo[] fileInformation;
    public final String buildCommand;
    public final String requiresImplementation;

    @SerializedName("implements")
    public final String[] implementsField;

    public final String[] dependsOn;
    public final String name;

    private DataInfo(
        String identity,
        EiffelFileInfo[] fileInformation,
        String buildCommand,
        String requiresImplementation,
        String[] implementsField,
        String[] dependsOn,
        String name) {
      this.identity = identity;
      this.fileInformation = fileInformation;
      this.buildCommand = buildCommand;
      this.requiresImplementation = requiresImplementation;
      this.implementsField = implementsField;
      this.dependsOn = dependsOn;
      this.name = name;
    }
  }
}
