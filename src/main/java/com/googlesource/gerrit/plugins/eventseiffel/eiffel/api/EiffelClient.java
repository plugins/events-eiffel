// Copyright (C) 2022 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel.api;

import com.google.common.flogger.FluentLogger;
import com.googlesource.gerrit.plugins.eventseiffel.config.EiffelRepoApiConfig;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.EventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.SourceChangeEventKey;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Redirect;
import java.time.Duration;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class EiffelClient implements EventStorage {
  public static FluentLogger logger = FluentLogger.forEnclosingClass();

  private EiffelGraphQlClient graphQlClient;
  private EiffelGoRestClient restClient;

  public EiffelClient(EiffelRepoApiConfig config) {
    HttpClient.Builder clientBuilder =
        HttpClient.newBuilder()
            .followRedirects(Redirect.NORMAL)
            .connectTimeout(Duration.ofSeconds(config.connectTimeout()));
    String username = config.username();
    char[] password = config.password();
    if (username != null && password != null) {
      clientBuilder.authenticator(
          new Authenticator() {

            @Override
            public PasswordAuthentication getPasswordAuthentication() {
              return new PasswordAuthentication(username, password);
            }
          });
    }
    HttpClient client = clientBuilder.build();
    this.graphQlClient = new EiffelGraphQlClient(client, config.graphQlUrl());
    this.restClient = new EiffelGoRestClient(client, config.goRestUrl());
  }

  @Override
  public Optional<UUID> getEventId(EventKey key) throws EventStorageException {
    return graphQlClient.getEventId(key);
  }

  @Override
  public Optional<List<UUID>> getParentLinks(EventKey key) throws EventStorageException {
    return restClient.getParentLinks(key);
  }

  @Override
  public Optional<UUID> getSccEventLink(SourceChangeEventKey key) throws EventStorageException {
    return restClient.getSccEventLink(key);
  }

  @Override
  public List<UUID> getScsIds(String repo, String commit) throws EventStorageException {
    return graphQlClient.getScsIds(repo, commit);
  }
}
