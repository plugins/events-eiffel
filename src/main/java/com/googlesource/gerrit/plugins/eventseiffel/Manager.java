// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel;

import com.google.gerrit.extensions.events.LifecycleListener;
import com.google.inject.Inject;
import com.googlesource.gerrit.plugins.eventseiffel.parsing.EiffelEventParsingQueue;

public class Manager implements LifecycleListener {
  private final EiffelEventHub eventHub;
  private final EiffelEventParsingQueue parsingQueue;

  @Inject
  public Manager(EiffelEventHub eventHub, EiffelEventParsingQueue parsingQueue) {
    this.eventHub = eventHub;
    this.parsingQueue = parsingQueue;
  }

  @Override
  public void start() {
    eventHub.startPublishing();
    parsingQueue.init();
  }

  @Override
  public void stop() {
    parsingQueue.shutDown();
    eventHub.stopPublishing();
  }
}
