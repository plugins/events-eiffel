// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.config;

import com.google.common.flogger.FluentLogger;
import com.google.gerrit.extensions.annotations.PluginName;
import com.google.gerrit.server.config.ConfigUtil;
import com.google.gerrit.server.config.PluginConfigFactory;
import com.google.gerrit.server.util.time.TimeUtil;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.ConnectionFactory;
import java.io.FileInputStream;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateFactory;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import org.eclipse.jgit.lib.Config;

@Singleton
public class RabbitMqConfig {
  public static class Provider implements com.google.inject.Provider<RabbitMqConfig> {
    private static final String APP_ID = "appId";
    private static final String EXCHANGE = "exchange";
    private static final String PASSWORD = "password";
    private static final String PERSISTENT_DELIVERY = "persistentDelivery";
    private static final String RABBIT_MQ = "RabbitMq";
    private static final String ROUTING_KEY_TAG = "routingKeyTag";
    private static final String USER_NAME = "username";
    private static final String URL = "url";
    private static final String VIRTUAL_HOST = "virtualHost";
    private static final String WAIT_FOR_CONFIRM = "waitForConfirm";
    private static final String MAX_BATCH_SIZE = "maxBatchSize";
    private static final String CERTIFICATE = "certificate";
    private static final boolean DEFAULT_PERSISTENT_DELIVERY = true;
    private static final long DEFAULT_WAIT_FOR_CONFIRM = 5000;
    private static final int DEFAULT_MAX_BATCH_SIZE = 1;

    private RabbitMqConfig config;

    @Inject
    public Provider(PluginConfigFactory cfgFactory, @PluginName String pluginName) {
      Config cfg = cfgFactory.getGlobalPluginConfig(pluginName);
      config =
          new RabbitMqConfig(
              cfg.getString(RABBIT_MQ, null, URL),
              cfg.getString(RABBIT_MQ, null, VIRTUAL_HOST),
              cfg.getString(RABBIT_MQ, null, USER_NAME),
              cfg.getString(RABBIT_MQ, null, PASSWORD),
              cfg.getString(RABBIT_MQ, null, EXCHANGE),
              cfg.getBoolean(RABBIT_MQ, null, PERSISTENT_DELIVERY, DEFAULT_PERSISTENT_DELIVERY),
              cfg.getString(RABBIT_MQ, null, ROUTING_KEY_TAG),
              cfg.getString(RABBIT_MQ, null, APP_ID),
              cfg.getString(RABBIT_MQ, null, CERTIFICATE),
              ConfigUtil.getTimeUnit(
                  cfg,
                  RABBIT_MQ,
                  null,
                  WAIT_FOR_CONFIRM,
                  DEFAULT_WAIT_FOR_CONFIRM,
                  TimeUnit.MILLISECONDS),
              cfg.getInt(RABBIT_MQ, MAX_BATCH_SIZE, DEFAULT_MAX_BATCH_SIZE));
    }

    @Override
    public RabbitMqConfig get() {
      return config;
    }
  }

  private static final FluentLogger logger = FluentLogger.forEnclosingClass();
  private static final String CHAR_ENCODING = StandardCharsets.UTF_8.name();
  private static final String CONTENT_TYPE = "application/json";
  private static final String DEFAULT_APP_ID = "Gerrit";
  private static final String DEFAULT_ROUTING_KEY_TAG = "_";
  private static final int NON_PERSISTENT_DELIVERY_VAL = 1;
  private static final int PERSISTENT_DELIVERY_VAL = 2;

  private final ConnectionFactory connectionFactory;
  private final String exchange;
  private final int deliveryMode;
  private final String routingKeyTag;
  private final String appId;
  private final long waitForConfirms;
  private final int maxBatchSize;

  private RabbitMqConfig(
      String uri,
      String virtualHost,
      String username,
      String passWord,
      String exchange,
      boolean persistentDelivery,
      String routingKey,
      String appId,
      String certificatePath,
      long waitForConfirms,
      int maxBatchSize) {
    this.exchange = exchange;
    this.deliveryMode = persistentDelivery ? PERSISTENT_DELIVERY_VAL : NON_PERSISTENT_DELIVERY_VAL;
    this.routingKeyTag = routingKey != null ? routingKey : DEFAULT_ROUTING_KEY_TAG;
    this.appId = appId != null ? appId : DEFAULT_APP_ID;
    this.waitForConfirms = waitForConfirms;
    this.maxBatchSize = maxBatchSize;
    logger.atInfo().log("Max batch size: %d", maxBatchSize);
    if (uri == null) {
      logger.atSevere().log("RabbitMq URL is not set.");
      this.connectionFactory = null;
      return;
    }

    ConnectionFactory cf = new ConnectionFactory();

    if (certificatePath != null) {
      try {
        CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");

        KeyStore tks = KeyStore.getInstance("JKS");
        tks.load(null);
        tks.setCertificateEntry(
            "server_certificate",
            certificateFactory.generateCertificate(new FileInputStream(certificatePath)));

        TrustManagerFactory tmf =
            TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        tmf.init(tks);

        SSLContext c = SSLContext.getInstance("TLSv1.3");
        c.init(null, tmf.getTrustManagers(), null);

        cf.useSslProtocol(c);
        cf.enableHostnameVerification();
      } catch (Exception e) {
        logger.atSevere().withCause(e).log(
            "Failed to setup certificate for peer-verification of RabbitMQ-server");
        this.connectionFactory = null;
        return;
      }
    } else {
      logger.atInfo().log("No certificate provided, continuing without peer verification.");
    }

    try {
      cf.setUri(uri);
    } catch (KeyManagementException | NoSuchAlgorithmException | URISyntaxException e) {
      logger.atSevere().withCause(e).log("Invalid RabbitMq URL \"%s\" configured.", uri);
      this.connectionFactory = null;
      return;
    }
    if (virtualHost != null) {
      cf.setVirtualHost(virtualHost);
    }

    if (username != null) {
      cf.setUsername(username);
      if (passWord != null) {
        cf.setPassword(passWord);
      }
    }
    this.connectionFactory = cf;
  }

  public ConnectionFactory connectionFactory() {
    return this.connectionFactory;
  }

  public AMQP.BasicProperties basicProperties() {
    return new AMQP.BasicProperties.Builder()
        .appId(appId)
        .contentType(CONTENT_TYPE)
        .contentEncoding(CHAR_ENCODING)
        .deliveryMode(deliveryMode)
        .timestamp(Date.from(TimeUtil.now()))
        .build();
  }

  public String charEncoding() {
    return CHAR_ENCODING;
  }

  public String exchange() {
    return exchange;
  }

  public String routingKeyTag() {
    return routingKeyTag;
  }

  public boolean isConfigured() {
    return connectionFactory != null;
  }

  public long waitForConfirms() {
    return waitForConfirms;
  }

  public boolean confirmsEnabled() {
    return waitForConfirms > 0;
  }

  public int maxBatchSize() {
    return maxBatchSize;
  }
}
