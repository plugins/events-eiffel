// Copyright (C) 2022 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package com.googlesource.gerrit.plugins.eventseiffel.parsing;

import com.google.gerrit.extensions.common.GitPerson;
import com.google.gerrit.extensions.events.RevisionCreatedListener;
import java.util.List;
import java.util.stream.Collectors;

public class PatchsetCreationData {
  public final GitPerson author;
  public final String project;
  public final String branch;
  public final String commitId;
  public final List<String> parentCommitIds;
  public final int changeNumber;
  public final int patchsetNumber;
  public final long createdAt;

  public PatchsetCreationData(RevisionCreatedListener.Event event) {
    this.author = event.getRevision().commit.author;
    this.project = event.getChange().project;
    this.branch = event.getChange().branch;
    this.commitId = event.getRevision().commit.commit;
    this.parentCommitIds =
        event.getRevision().commit.parents.stream()
            .map(ci -> ci.commit)
            .collect(Collectors.toList());
    this.changeNumber = event.getChange()._number;
    this.patchsetNumber = event.getRevision()._number;
    this.createdAt = event.getWhen().toEpochMilli();
  }
}
