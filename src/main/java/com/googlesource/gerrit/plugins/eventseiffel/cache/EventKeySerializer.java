// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.cache;

import static com.google.common.base.Preconditions.checkState;

import com.google.gerrit.server.cache.serialize.CacheSerializer;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.ArtifactEventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.CompositionDefinedEventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.EventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.SourceChangeEventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType;

public class EventKeySerializer implements CacheSerializer<EventKey> {
  private static String SEPARATOR = "-@-";

  @Override
  public byte[] serialize(EventKey object) {
    switch (object.type()) {
      case SCC:
      case SCS:
        return getBytes((SourceChangeEventKey) object);
      case ARTC:
        return getBytes((ArtifactEventKey) object);
      case CD:
        return getBytes((CompositionDefinedEventKey) object);
      default:
        return new byte[0];
    }
  }

  private static byte[] getBytes(SourceChangeEventKey key) {
    return String.join(SEPARATOR, key.type().name(), key.repo(), key.branch(), key.commit())
        .getBytes();
  }

  private static byte[] getBytes(ArtifactEventKey key) {
    return String.join(SEPARATOR, key.type().name(), key.identity()).getBytes();
  }

  private static byte[] getBytes(CompositionDefinedEventKey key) {
    return String.join(SEPARATOR, key.type().name(), key.name(), key.version()).getBytes();
  }

  @Override
  public EventKey deserialize(byte[] in) {
    String[] parts = new String(in).split(SEPARATOR);
    EiffelEventType type = EiffelEventType.valueOf(parts[0]);
    switch (type) {
      case SCC:
      case SCS:
        checkState(parts.length == 4, "SourceChangeEventKey has 4 properties");
        return SourceChangeEventKey.create(parts[1], parts[2], parts[3], type);
      case ARTC:
        checkState(parts.length == 2, "ArtifactEventKey has 2 properties");
        return ArtifactEventKey.create(parts[1]);
      case CD:
        checkState(parts.length == 3, "CompositionDefinedEventKey has 3 properties");
        return CompositionDefinedEventKey.create(parts[1], parts[2]);
      default:
        return null;
    }
  }
}
