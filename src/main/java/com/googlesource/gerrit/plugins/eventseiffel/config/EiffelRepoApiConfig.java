// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.config;

import com.google.common.flogger.FluentLogger;
import com.google.gerrit.extensions.annotations.PluginName;
import com.google.gerrit.server.config.PluginConfigFactory;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.net.URI;
import java.net.URISyntaxException;
import org.eclipse.jgit.lib.Config;

@Singleton
public class EiffelRepoApiConfig {
  private static final FluentLogger logger = FluentLogger.forEnclosingClass();

  public static class Provider implements com.google.inject.Provider<EiffelRepoApiConfig> {

    private static final String CONNECT_TIMEOUT = "connectTimeout";
    private static final String EIFFELREPO_CLIENT = "EiffelRepoClient";
    private static final String PASSWORD = "password";
    private static final String USER_NAME = "username";
    private static final String GRAPHQL_URL = "graphQlUrl";
    private static final String GOREST_URL = "goRestUrl";
    private static final int DEFAULT_CONNECT_TIMEOUT = 20;

    private final EiffelRepoApiConfig config;

    @Inject
    public Provider(PluginConfigFactory cfgFactory, @PluginName String pluginName) {
      Config cfg = cfgFactory.getGlobalPluginConfig(pluginName);
      config =
          new EiffelRepoApiConfig(
              getHttpUri(cfg, EIFFELREPO_CLIENT, GRAPHQL_URL),
              getHttpUri(cfg, EIFFELREPO_CLIENT, GOREST_URL),
              cfg.getString(EIFFELREPO_CLIENT, null, USER_NAME),
              cfg.getString(EIFFELREPO_CLIENT, null, PASSWORD),
              cfg.getInt(EIFFELREPO_CLIENT, CONNECT_TIMEOUT, DEFAULT_CONNECT_TIMEOUT));
    }

    @Override
    public EiffelRepoApiConfig get() {
      return config;
    }
  }

  /* Check that the url is a valid http url. */
  private static URI getHttpUri(Config cfg, String section, String name) {
    String url = cfg.getString(section, null, name);
    String configKey = section + "." + name;
    URI uri = null;
    try {
      uri = url == null ? null : new URI(url);
    } catch (URISyntaxException e) {
      logger.atSevere().withCause(e).log("%s \"%s\" is malformed.", configKey, url);
    }
    if (uri != null) {
      String scheme = uri.getScheme();
      if (scheme == null) {
        logger.atSevere().log("%s \"%s\" is missing schema.", configKey, url);
        return null;
      }
      String lcScheme = scheme.toLowerCase();
      if (!lcScheme.equals("http") && !lcScheme.equals("https")) {
        logger.atSevere().log("%s  \"%s\" scheme is not valid [http|https].", configKey, url);
        return null;
      }
      if (uri.getHost() == null) {
        logger.atSevere().log("%s \"%s\" is missing host.", configKey, url);
        return null;
      }
    }
    return uri;
  }

  private final URI graphQlUrl;
  private final URI goRestUrl;
  private final String username;
  private final char[] password;
  private final int connectTimeOut;

  private EiffelRepoApiConfig(
      URI graphQlUrl, URI goRestUrl, String username, String password, int connectTimeout) {
    this.graphQlUrl = graphQlUrl;
    this.goRestUrl = goRestUrl;
    this.username = username;
    this.password = password == null ? null : password.toCharArray();
    this.connectTimeOut = connectTimeout;
  }

  public boolean isConfigured() {
    return graphQlUrl != null && goRestUrl != null;
  }

  public URI graphQlUrl() {
    return this.graphQlUrl;
  }

  public URI goRestUrl() {
    return this.goRestUrl;
  }

  public String username() {
    return username;
  }

  public char[] password() {
    return password;
  }

  public int connectTimeout() {
    return connectTimeOut;
  }
}
