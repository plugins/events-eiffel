// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto;

import static com.google.common.base.Preconditions.checkState;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Optional;

/**
 * Artifact file content
 *
 * <p>As declared by:
 * https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelArtifactCreatedEvent.md#datafileInformation
 */
public class EiffelFileInfo {

  public static Builder builder() {
    return new Builder();
  }

  /**
   * The name (including relative path from the root of the artifact) on syntax appropriate for the
   * artifact packaging type.
   */
  public final String name;
  /**
   * Any tags associated with the file, to support navigation and identification of items of
   * interest.
   */
  public final String[] tags;

  private EiffelFileInfo(String name, String[] tags) {
    this.name = name;
    this.tags = tags;
  }

  public static class Builder {
    private String _name = null;
    private Optional<ArrayList<String>> _tags = Optional.empty();

    private Builder() {}

    public Builder name(String name) {
      this._name = name;
      return this;
    }

    public Builder addTag(String tag) {
      _tags = _tags.or(() -> Optional.of(Lists.newArrayList()));
      _tags.get().add(tag);
      return this;
    }

    public EiffelFileInfo build() {
      checkState(_name != null, "name is required");
      return new EiffelFileInfo(
          _name, _tags.map(t -> t.stream().toArray(String[]::new)).orElse(null));
    }
  }
}
