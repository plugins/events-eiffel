// Copyright (C) 2022 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package com.googlesource.gerrit.plugins.eventseiffel.parsing;

import com.google.gerrit.extensions.common.AccountInfo;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.SourceChangeEventKey;

public interface EiffelEventParser {

  /**
   * Creates SCC events for all commits reachable from the commit that triggered the patchset
   * creation described by the {@link PatchsetCreationData} data.
   *
   * @param data - Describes a patchset creation from which to create SCC events.
   * @throws EventParsingException- When event-creation fails.
   */
  void createAndScheduleSccFromPatchsetCreation(PatchsetCreationData data)
      throws EventParsingException;

  /**
   * Creates SCC events for all commits reachable from branchRef. I.e. create SCCs for already
   * merged commit that are missing SCCs.
   *
   * @param repoName - Name of the repository where the branch exists.
   * @param branchRef - Creates missing events for all commits reachable from branch.
   * @throws EventParsingException - When event-creation fails.
   */
  void createAndScheduleSccFromBranch(String repoName, String branchRef)
      throws EventParsingException;

  /**
   * Creates SCC events for all commits reachable from commit.
   *
   * @param repoName - Name of the repository were the commits exists.
   * @param branchRef - Ref of the branch to create events for.
   * @param commit - Creates missing events for all commits reachable from commit.
   * @throws EventParsingException - When event-creation fails.
   */
  void createAndScheduleSccFromCommit(String repoName, String branchRef, String commit)
      throws EventParsingException;

  /**
   * Recreates missing SCC events that are referenced by other, existing, events.
   *
   * @param repoName - Name of the repository were the commits exists.
   * @param branchRef - Ref of the branch to create events for.
   * @throws EventParsingException - When event-creation fails.
   */
  void fillGapsForSccFromBranch(String repoName, String branchRef) throws EventParsingException;

  /**
   * Recreates missing SCC events that are referenced by other, existing, events.
   *
   * @param repoName - Name of the repository were the commits exists.
   * @param branchRef - Ref of the branch to create events for.
   * @param commit - Creates missing events for all commits reachable from commit.
   * @throws EventParsingException - When event-creation fails.
   */
  void fillGapsForSccFromCommit(String repoName, String branchRef, String commit)
      throws EventParsingException;

  /**
   * Recreates missing SCS events that are referenced by other, existing, events.
   *
   * @param repoName - Name of the repository were the commits exists.
   * @param branchRef - Ref of the branch to create events for.
   * @throws EventParsingException - When event-creation fails.
   */
  void fillGapsForScsFromBranch(String repoName, String branchRef) throws EventParsingException;

  /**
   * Creates missing SCS events for repoName, branch.
   *
   * @param repoName - Name of the repository where the branch exists.
   * @param branchRef - Creates missing events for all commits reachable from branchRef.
   * @throws EventParsingException - When event-creation fails.
   */
  void createAndScheduleMissingScssFromBranch(String repoName, String branchRef)
      throws EventParsingException;

  /**
   * Creates missing SCS events for a submit transaction. submitter and submittedAt is set for all
   * events created within the submit transaction, i.e. not reachable from commitSha1TransactionEnd.
   *
   * @param scs - Key for the event that should be created, together with parents.
   * @param commitSha1TransactionEnd - Tip before submit transaction.
   * @param submitter - The submitter
   * @param submittedAt - When the commit was submitted.
   * @throws EventParsingException - When event-creation fails.
   */
  void createAndScheduleMissingScss(
      SourceChangeEventKey scs,
      String commitSha1TransactionEnd,
      AccountInfo submitter,
      Long submittedAt)
      throws EventParsingException;

  /**
   * Creates missing ARTC for a tag, together with CD and (if missing) SCS for referenced commit.
   *
   * @param repoName - Name of the repository were the tag exists.
   * @param tagName - The name of the tag.
   * @param creationTime - The time at which the tag was created.
   * @param force - Whether existing events should be replaced or not.
   * @throws EventParsingException - When event-creation fails.
   */
  void createAndScheduleArtc(String repoName, String tagName, Long creationTime, boolean force)
      throws EventParsingException;
}
