// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel;

import com.googlesource.gerrit.plugins.eventseiffel.cache.EiffelEventIdLookupException;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.EventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.SourceChangeEventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEvent;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface EiffelEventHub extends EiffelEventQueue {
  /** A consumer that consumes event from the EiffelEventHub. */
  public interface Consumer {
    /**
     * Start consuming events from queue.
     *
     * @param queue
     */
    public void start(EiffelEventQueue queue);
    /**
     * Stop consuming events from this queue. Consumer is expected to not keep a reference to queue
     * after this call.
     */
    public void stop();
    /**
     * Checks if the consumer is currently ready to consume from the queue or needs to be restarted.
     *
     * @return True if Consumer is ready to consume events from queue.
     */
    public boolean isRunning();
  }
  /**
   * Adds an event to the internal FIFO publish queue, waiting if necessary until queue has capacity
   * to accept another event.
   *
   * <p>This operation is a no-op if an event already exists in the hub such that:
   *
   * <p>{@code EventKey.fromEvent(this).equals(EventKey.fromEvent(existing)}
   *
   * <p>is true and force is set to false
   *
   * <p><b>NOTE:</b>
   *
   * <p>Do not use the event.meta.id directly for linking purposes, instead getExistingId should be
   * used after adding the event to this hub.
   *
   * @param event
   * @param force Create the event even if there already exists an event with an identical key.
   * @throws InterruptedException
   */
  public void push(EiffelEvent event, boolean force) throws InterruptedException;
  /**
   * If an event exists, already published or awaiting publish in this EiffelEventHub, such that:
   *
   * <p>{@code EventKey.fromEvent(this).equals(EventKey.fromEvent(existing)}
   *
   * <p>is true, the meta.id of this event is returned, otherwise Optional.empty().
   *
   * @param key EventKey
   * @return Optional<UUID> event.meta.id
   * @throws EiffelEventIdLookupException when failing to lookup event from Eiffel.
   */
  public Optional<UUID> getExistingId(EventKey key) throws EiffelEventIdLookupException;
  /**
   * If an event exists, already published or awaiting publish in this EiffelEventHub, such that:
   *
   * <p>it has the type SCS, has the repository repo, has the commitid commit
   *
   * <p>and has one of the branches in branches, the meta.id of this event is returned,
   *
   * <p>otherwise Optional.empty().
   *
   * @param repo String
   * @param commit String
   * @param branches List<String>
   * @return Optional<UUID> event.meta.id
   * @throws EiffelEventIdLookupException when failing to lookup event from Eiffel.
   */
  public Optional<UUID> getScsForCommit(String repo, String commit, List<String> branches)
      throws EiffelEventIdLookupException;

  /**
   * If an event exists, already published or awaiting publish in this EiffelEventHub, such that:
   *
   * <p>{@code EventKey.fromEvent(this).equals(EventKey.fromEvent(existing)}
   *
   * <p>is true, links.target for all links of type PREVIOUS_VERSION of this event is returned,
   * otherwise Optional.empty()
   *
   * @param key EventKey
   * @return Optional<List<UUID>> event.links[].target
   * @throws EiffelEventIdLookupException when failing to lookup event from Eiffel.
   */
  public Optional<List<UUID>> getParentLinks(EventKey key) throws EiffelEventIdLookupException;

  /**
   * If an event exists, already published or awaiting publish in this EiffelEventHub, such that:
   *
   * <p>{@code EventKey.fromEvent(this).equals(EventKey.fromEvent(existing) &&
   * this.getEventType().equals(SCS)}
   *
   * <p>is true, the links.target for the link where links.type is CHANGE is returned, otherwise
   * Optional.empty().
   *
   * @param key SourceChangeEventKey
   * @return Optional<UUID> event.links.target
   * @throws EiffelEventIdLookupException when failing to lookup event from Eiffel.
   */
  public Optional<UUID> getSccEventLink(SourceChangeEventKey key)
      throws EiffelEventIdLookupException;
  /** Start publishing events to Eiffel. */
  public void startPublishing();
  /** Stop publishing events to Eiffel. */
  public void stopPublishing();
  /**
   * Is this EventHub accepting new events.
   *
   * @return True if new events will be accepted.
   */
  public boolean isOpen();
}
