// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.googlesource.gerrit.plugins.eventseiffel.config.EiffelRepoApiConfig;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.api.EiffelClient;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.api.EventStorage;

public class EventStorageProvider implements Provider<EventStorage> {

  private EiffelClient eiffelClient;

  @Inject
  public EventStorageProvider(EiffelRepoApiConfig config) {
    this.eiffelClient = new EiffelClient(config);
  }

  @Override
  public EventStorage get() {
    return eiffelClient;
  }
}
