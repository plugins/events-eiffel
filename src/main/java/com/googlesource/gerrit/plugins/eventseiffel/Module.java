// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel;

import com.google.common.flogger.FluentLogger;
import com.google.gerrit.extensions.config.FactoryModule;
import com.google.gerrit.extensions.events.GitReferenceUpdatedListener;
import com.google.gerrit.extensions.events.LifecycleListener;
import com.google.gerrit.extensions.events.RevisionCreatedListener;
import com.google.gerrit.extensions.registration.DynamicSet;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Scopes;
import com.google.inject.internal.UniqueAnnotations;
import com.googlesource.gerrit.plugins.eventseiffel.cache.EiffelEventIdCacheImpl;
import com.googlesource.gerrit.plugins.eventseiffel.config.EiffelConfig;
import com.googlesource.gerrit.plugins.eventseiffel.config.EiffelRepoApiConfig;
import com.googlesource.gerrit.plugins.eventseiffel.config.EventIdCacheConfig;
import com.googlesource.gerrit.plugins.eventseiffel.config.EventListenersConfig;
import com.googlesource.gerrit.plugins.eventseiffel.config.EventMappingConfig;
import com.googlesource.gerrit.plugins.eventseiffel.config.EventParsingConfig;
import com.googlesource.gerrit.plugins.eventseiffel.config.EventsFilter;
import com.googlesource.gerrit.plugins.eventseiffel.config.RabbitMqConfig;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.api.EiffelEventPublisher;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.api.EventStorage;
import com.googlesource.gerrit.plugins.eventseiffel.listeners.PatchsetCreatedListener;
import com.googlesource.gerrit.plugins.eventseiffel.listeners.RefUpdateListener;
import com.googlesource.gerrit.plugins.eventseiffel.mapping.EiffelEventFactory;
import com.googlesource.gerrit.plugins.eventseiffel.mapping.EiffelEventMapper;
import com.googlesource.gerrit.plugins.eventseiffel.mq.RabbitMqPublisher;
import com.googlesource.gerrit.plugins.eventseiffel.parsing.EiffelEventParser;
import com.googlesource.gerrit.plugins.eventseiffel.parsing.EiffelEventParserImpl;
import com.googlesource.gerrit.plugins.eventseiffel.parsing.EiffelEventParsingExecutor;
import com.googlesource.gerrit.plugins.eventseiffel.parsing.EiffelEventParsingQueue;
import com.googlesource.gerrit.plugins.eventseiffel.rest.RestModule;

class Module extends FactoryModule {
  private static final FluentLogger logger = FluentLogger.forEnclosingClass();
  private final Provider<RabbitMqConfig> rabbitMq;
  private final Provider<EiffelRepoApiConfig> eiffelRepoConfig;
  private final Provider<EventIdCacheConfig> idCache;

  @Inject
  public Module(
      EiffelRepoApiConfig.Provider eiffelRepoConfig,
      RabbitMqConfig.Provider rabbitMqConfig,
      EventIdCacheConfig.Provider idCache) {
    this.eiffelRepoConfig = eiffelRepoConfig;
    this.rabbitMq = rabbitMqConfig;
    this.idCache = idCache;
  }

  @Override
  protected void configure() {
    if (idCache.get().trustLocalCache()) {
      bind(EventStorage.class).to(EventStorage.NoOpEventStorage.class);
      logger.atWarning().log("GraphQlApi Event Storage is not used, local cache is trusted.");
    } else if (eiffelRepoConfig.get().isConfigured()) {
      bind(EiffelRepoApiConfig.class)
          .toProvider(EiffelRepoApiConfig.Provider.class)
          .in(Scopes.SINGLETON);
      bind(EventStorage.class).toProvider(EventStorageProvider.class).in(Scopes.SINGLETON);
    } else {
      logger.atSevere().log("Found no configuration for EventStorage.");
    }
    bind(EiffelConfig.class).toProvider(EiffelConfig.Provider.class).in(Scopes.SINGLETON);
    bind(EiffelEventFactory.class)
        .toProvider(EiffelEventFactory.Provider.class)
        .in(Scopes.SINGLETON);
    bind(EventMappingConfig.class)
        .toProvider(EventMappingConfig.Provider.class)
        .in(Scopes.SINGLETON);
    bind(EiffelEventMapper.class);
    if (rabbitMq.get().isConfigured()) {
      bind(RabbitMqConfig.class).toProvider(RabbitMqConfig.Provider.class).in(Scopes.SINGLETON);
      bind(RabbitMqPublisher.class).in(Scopes.SINGLETON);
      bind(EiffelEventPublisher.class).to(RabbitMqPublisher.class);
      bind(LifecycleListener.class)
          .annotatedWith(UniqueAnnotations.create())
          .to(RabbitMqPublisher.class);
    } else {
      logger.atSevere().log("Valid publisher configuration is missing.");
    }
    bind(EventIdCacheConfig.class).toProvider(EventIdCacheConfig.Provider.class);
    install(EiffelEventIdCacheImpl.module(idCache.get().maxNbrOfEntries()));
    install(new RestModule());
    bind(PublishEventWorker.class).in(Scopes.SINGLETON);
    bind(EiffelEventHub.Consumer.class).to(PublishEventWorker.class);
    bind(EventParsingConfig.class)
        .toProvider(EventParsingConfig.Provider.class)
        .in(Scopes.SINGLETON);
    bind(EventsFilter.class).toProvider(EventsFilter.Provider.class);
    bind(EiffelEventParsingExecutor.Scheduled.class).in(Scopes.SINGLETON);
    bind(EiffelEventParsingExecutor.class).to(EiffelEventParsingExecutor.Scheduled.class);
    bind(EiffelEventParser.class).to(EiffelEventParserImpl.class);
    bind(EiffelEventParsingQueue.class).in(Scopes.SINGLETON);
    bind(EiffelEventHubImpl.class).in(Scopes.SINGLETON);
    bind(EiffelEventHub.class).to(EiffelEventHubImpl.class);
    bind(EiffelEventQueue.class).to(EiffelEventHubImpl.class);
    bind(EventListenersConfig.class)
        .toProvider(EventListenersConfig.Provider.class)
        .in(Scopes.SINGLETON);
    DynamicSet.bind(binder(), RevisionCreatedListener.class).to(PatchsetCreatedListener.class);
    DynamicSet.bind(binder(), GitReferenceUpdatedListener.class).to(RefUpdateListener.class);

    bind(Manager.class).in(Scopes.SINGLETON);
    bind(LifecycleListener.class).annotatedWith(UniqueAnnotations.create()).to(Manager.class);
  }
}
