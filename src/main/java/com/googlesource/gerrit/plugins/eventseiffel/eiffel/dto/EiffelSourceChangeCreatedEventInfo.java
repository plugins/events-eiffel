// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto;

import com.google.common.collect.ImmutableSet;
import java.util.Optional;
import java.util.Set;

/**
 * The <a
 * href="https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelSourceChangeCreatedEvent.md">EiffelSourceChangeCreatedEvent</a>
 * declares that a change to sources has been made, but not yet submitted (see
 * EiffelSourceChangeSubmittedEvent). This can be used to represent a change done on a private
 * branch, undergoing review or made in a forked repository. Unlike
 * EiffelSourceChangeSubmittedEvent, EiffelSourceChangeCreatedEvent describes the change in terms of
 * who authored it and which issues it addressed.
 *
 * <p>Where changes are integrated (or "submitted") directly on a shared branch of interest (e.g.
 * "master", "dev" or "mainline") both EiffelSourceChangeCreatedEvent and
 * EiffelSourceChangeSubmittedEvent are sent together.
 *
 * <p>See also <a
 * href="https://github.com/eiffel-community/eiffel/blob/edition-paris/examples/events/EiffelSourceChangeCreatedEvent/simple.json">example
 * event</a>.
 */
public class EiffelSourceChangeCreatedEventInfo extends EiffelEvent {
  /**
   * See <a
   * href="https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelSourceChangeCreatedEvent.md#version-history">Event
   * version history</a>.
   */
  public static final String EVENT_VERSION = "4.0.0";

  public static final EiffelEventType EVENT_TYPE = EiffelEventType.SCC;
  /**
   * https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelSourceChangeCreatedEvent.md#links
   */
  public static Set<EiffelLinkType> SUPPORTED_LINKS =
      ImmutableSet.of(
          EiffelLinkType.BASE,
          EiffelLinkType.CAUSE,
          EiffelLinkType.CONTEXT,
          EiffelLinkType.DERESOLVED_ISSUE,
          EiffelLinkType.FLOW_CONTEXT,
          EiffelLinkType.PARTIALLY_RESOLVED_ISSUE,
          EiffelLinkType.PREVIOUS_VERSION,
          EiffelLinkType.RESOLVED_ISSUE);

  public static Builder builder() {
    return new Builder();
  }

  public final DataInfo data;

  private EiffelSourceChangeCreatedEventInfo(
      DataInfo data, EiffelMetaInfo meta, EiffelLinkInfo[] links) {
    super(meta, links);
    this.data = data;
  }

  public static class Builder
      extends EiffelEvent.Builder<EiffelSourceChangeCreatedEventInfo, Builder> {
    private Optional<EiffelGitIdentifierInfo.Builder> _gitIdentifier = Optional.empty();
    private Optional<EiffelPersonInfo.Builder> _author = Optional.empty();
    private Optional<EiffelChangeInfo.Builder> _change = Optional.empty();

    private Builder() {}

    public Builder gitIdentifier(EiffelGitIdentifierInfo.Builder gitIdentifier) {
      _gitIdentifier = Optional.of(gitIdentifier);
      return this;
    }

    public Builder author(EiffelPersonInfo.Builder author) {
      _author = Optional.of(author);
      return this;
    }

    public Builder change(EiffelChangeInfo.Builder change) {
      _change = Optional.of(change);
      return this;
    }

    @Override
    public EiffelSourceChangeCreatedEventInfo build() {
      return new EiffelSourceChangeCreatedEventInfo(
          _gitIdentifier.isEmpty() && _author.isEmpty() && _change.isEmpty()
              ? null
              : new DataInfo(
                  _gitIdentifier.map(EiffelGitIdentifierInfo.Builder::build).orElse(null),
                  _author.map(EiffelPersonInfo.Builder::build).orElse(null),
                  _change.map(EiffelChangeInfo.Builder::build).orElse(null)),
          buildMeta(),
          buildLinks());
    }

    @Override
    protected String getEventVersion() {
      return EVENT_VERSION;
    }

    @Override
    protected EiffelEventType getEventType() {
      return EVENT_TYPE;
    }

    @Override
    protected Set<EiffelLinkType> supportedLinks() {
      return SUPPORTED_LINKS;
    }
  }

  public static class DataInfo {
    public final EiffelGitIdentifierInfo gitIdentifier;
    public final EiffelPersonInfo author;
    public final EiffelChangeInfo change;

    DataInfo(
        EiffelGitIdentifierInfo gitIdentifier, EiffelPersonInfo author, EiffelChangeInfo change) {
      this.gitIdentifier = gitIdentifier;
      this.author = author;
      this.change = change;
    }
  }
}
