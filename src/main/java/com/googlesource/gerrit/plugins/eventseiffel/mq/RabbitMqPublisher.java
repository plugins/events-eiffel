// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.mq;

import com.google.common.flogger.FluentLogger;
import com.google.gerrit.extensions.events.LifecycleListener;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.googlesource.gerrit.plugins.eventseiffel.config.RabbitMqConfig;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.EventKey;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.api.EiffelEventPublisher;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEvent;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.TimeoutException;

public class RabbitMqPublisher implements EiffelEventPublisher, LifecycleListener {
  /* https://eiffel-community.github.io/eiffel-sepia/rabbitmq-message-broker.html */
  private static final String ROUTING_KEY = "eiffel.%s.%s.%s.%s";
  /* In Sepia "family" is part of the routing-key but the concept of event-family doesn't exist.
   * https://eiffel-community.github.io/eiffel-sepia/rabbitmq-message-broker.html*/
  private static final String DEFAULT_FAMILY = "_";
  public static FluentLogger logger = FluentLogger.forEnclosingClass();

  private static String toRoutingKey(EiffelEvent event, String tag) {
    return String.format(
        ROUTING_KEY, DEFAULT_FAMILY, event.meta.type.getType(), tag, event.meta.source.domainId);
  }

  private volatile Connection connection;
  private volatile Channel channel;
  private volatile boolean closing = false;
  private final RabbitMqConfig config;
  private final Gson gson = new Gson();

  @Inject
  public RabbitMqPublisher(RabbitMqConfig config) {
    this.config = config;
  }

  @Override
  public boolean publish(Collection<EiffelEvent> events) {
    if (channel == null || !channel.isOpen()) {
      channel = openChannel();
    }
    if (channel != null && channel.isOpen()) {
      for (EiffelEvent event : events) {
        if (!publishEvent(event)) {
          return false;
        }
      }
      if (config.confirmsEnabled()) {
        try {
          return channel.waitForConfirms(config.waitForConfirms());
        } catch (TimeoutException te) {
          logger.atWarning().withCause(te).log("Failed to confirm delivery");
          return false;
        } catch (InterruptedException ie) {
          logger.atWarning().log("Interupted while waiting for confirms, new attempt");
          try {
            channel.waitForConfirms(2000);
            return true;
          } catch (InterruptedException | TimeoutException e) {
            logger.atSevere().withCause(e).log(
                "Failed to confirm pushed events before cleanup: %s",
                Arrays.toString(events.toArray()));
            return false;
          }
        }
      }
      return true;
    }
    return false;
  }

  private boolean publishEvent(EiffelEvent event) {
    byte[] payLoad;
    try {
      payLoad = gson.toJson(event).getBytes(config.charEncoding());
    } catch (UnsupportedEncodingException e) {
      logger.atSevere().withCause(e).log(
          "Unable to serialize event %s with encoding: %s",
          EventKey.fromEvent(event), config.charEncoding());
      return false;
    }
    try {
      channel.basicPublish(
          config.exchange(),
          toRoutingKey(event, config.routingKeyTag()),
          config.basicProperties(),
          payLoad);
    } catch (IOException e) {
      logger.atSevere().withCause(e).log("Failed to send event: %s", gson.toJson(event));
      return false;
    }
    return true;
  }

  @Override
  public int maxBatchSize() {
    return config.maxBatchSize();
  }

  private Channel openChannel() {
    if (ensureConnected()) {
      try {
        Channel ch = newChannel();
        if (config.confirmsEnabled()) {
          ch.confirmSelect();
        }
        return ch;
      } catch (IOException e) {
        logger.atSevere().withCause(e).log("Failed to open channel");
        return null;
      }
    }
    logger.atSevere().log("Failed to create channel, unable to open connection.");
    return null;
  }

  private boolean ensureConnected() {
    if (closing) {
      return false;
    }
    if (connection != null && connection.isOpen()) {
      return true;
    }
    try {
      connection = newConnection();
      return connection != null && connection.isOpen();
    } catch (IOException | TimeoutException e) {
      logger.atSevere().withCause(e).log("Unable to open connection");
      return false;
    }
  }

  private Connection newConnection() throws IOException, TimeoutException {
    Connection conn = config.connectionFactory().newConnection();
    conn.addShutdownListener(
        cause -> {
          if (cause.isInitiatedByApplication()) {
            logger.atInfo().log("Connection closed by application.");
          } else {
            logger.atWarning().log("Connection closed. Cause: %s", cause.getMessage());
          }
        });
    logger.atInfo().log("Connection established");
    return conn;
  }

  private Channel newChannel() throws IOException {
    Channel ch = connection.createChannel();
    int channelId = ch.getChannelNumber();
    ch.addShutdownListener(
        cause -> {
          if (cause.isInitiatedByApplication()) {
            logger.atInfo().log("Channel #%d closed by application.", channelId);
          } else {
            logger.atWarning().log("Channel #%d closed. Cause: %s", channelId, cause.getMessage());
          }
        });
    return ch;
  }

  @Override
  public void start() {}

  @Override
  public void stop() {
    closing = true;
    if (channel != null) {
      try {
        channel.close();
        channel = null;
      } catch (IOException | TimeoutException e) {
        logger.atWarning().withCause(e).log("Exception when closing channel.");
      }
    }
    if (connection != null) {
      try {
        connection.close();
        connection = null;
      } catch (IOException e) {
        logger.atWarning().withCause(e).log("Exception when closing connection.");
      }
    }
  }
}
