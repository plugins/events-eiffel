// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.rest;

import com.google.gerrit.common.data.GlobalCapability;
import com.google.gerrit.entities.PatchSet;
import com.google.gerrit.entities.RefNames;
import com.google.gerrit.extensions.annotations.RequiresCapability;
import com.google.gerrit.extensions.restapi.AuthException;
import com.google.gerrit.extensions.restapi.BadRequestException;
import com.google.gerrit.extensions.restapi.ResourceConflictException;
import com.google.gerrit.extensions.restapi.Response;
import com.google.gerrit.extensions.restapi.RestModifyView;
import com.google.gerrit.server.project.BranchResource;
import com.google.gerrit.server.query.change.InternalChangeQuery;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.googlesource.gerrit.plugins.eventseiffel.parsing.EiffelEventParsingQueue;
import java.util.Optional;

@Singleton
@RequiresCapability(GlobalCapability.ADMINISTRATE_SERVER)
public class CreateSccs implements RestModifyView<BranchResource, CreateSccs.Input> {
  static class Input {
    boolean fillGaps;
  }

  private final EiffelEventParsingQueue queue;
  private final Provider<InternalChangeQuery> queryProvider;

  @Inject
  CreateSccs(EiffelEventParsingQueue queue, Provider<InternalChangeQuery> queryProvider) {
    this.queue = queue;
    this.queryProvider = queryProvider;
  }

  @Override
  public Response<?> apply(BranchResource resource, CreateSccs.Input input)
      throws AuthException, BadRequestException, ResourceConflictException, Exception {
    if (resource.getRef().startsWith(RefNames.REFS_HEADS)) {
      if (input.fillGaps) {
        queue.scheduleMissingSccCreation(resource.getName(), resource.getRef());
      } else {
        queue.scheduleSccCreation(resource.getName(), resource.getRef());
      }
      return EventCreationResponse.scc(resource);
    }
    PatchSet.Id maybePatchSet = PatchSet.Id.fromRef(resource.getRef());
    if (maybePatchSet != null) {
      Optional<String> targetBranch =
          queryProvider.get().byLegacyChangeId(maybePatchSet.changeId()).stream()
              .filter(cd -> cd.project().equals(resource.getProjectState().getNameKey()))
              .findFirst()
              .map(c -> c.change().getDest().branch());
      if (targetBranch.isPresent()) {
        if (input.fillGaps) {
          queue.scheduleMissingSccCreation(
              resource.getName(), targetBranch.get(), resource.getRevision().get());
        } else {
          queue.scheduleSccCreation(
              resource.getName(), targetBranch.get(), resource.getRevision().get());
        }
        return EventCreationResponse.scc(resource, targetBranch.get());
      }
      throw new BadRequestException(
          "Cannot identify change from patch-set ref: " + resource.getRef());
    }

    throw new BadRequestException("SCC creation is only allowed from branches or patch-set refs.");
  }
}
