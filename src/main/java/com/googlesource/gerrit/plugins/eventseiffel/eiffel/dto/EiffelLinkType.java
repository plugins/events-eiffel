// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto;

public enum EiffelLinkType {
  /**
   * Identifies the base revision of the created source change.
   *
   * <p>Legal targets: EiffelSourceChangeSubmittedEvent
   */
  BASE(false),
  /**
   * Identifies a cause of the event occurring. Should not be used in conjunction with CONTEXT:
   * individual events providing CAUSE within a larger context gives rise to ambiguity. It is
   * instead recommended to let the root event of the context declare CAUSE.
   *
   * <p>Legal targets: Any
   */
  CAUSE(true),
  /**
   * Identifies the change that was submitted.
   *
   * <p>Legal targets: EiffelSourceChangeCreatedEvent
   */
  CHANGE(false),
  /**
   * Identifies the composition from which this artifact was built.
   *
   * <p>Legal targets: EiffelCompositionDefinedEvent
   */
  COMPOSITION(false),
  /**
   * Identifies the activity or test suite of which this event constitutes a part.
   *
   * <p>Legal targets: EiffelActivityTriggeredEvent, EiffelTestSuiteStartedEvent
   */
  CONTEXT(false),
  /**
   * Identifies an issue which was previously resolved, but that this SCC claims it has made changes
   * to warrant removing the resolved status. For example, if an issue "Feature X" was resolved, but
   * this SCC removed the implementation that led to "Feature X" being resolved, that issue should
   * no longer be considered resolved.
   *
   * <p>Legal targets: EiffelIssueDefinedEvent
   */
  DERESOLVED_ISSUE(true),
  /**
   * Identifies an element and/or sub-composition of this composition. The latter is particularly
   * useful for documenting large and potentially decentralized compositions, and may be used to
   * reduce the need to repeat large compositions in which only small parts are subject to frequent
   * change.
   *
   * <p>Legal targets: EiffelCompositionDefinedEvent, EiffelSourceChangeCreatedEvent,
   * EiffelSourceChangeSubmittedEvent, EiffelArtifactCreatedEvent
   */
  ELEMENT(true),
  /**
   * Identifies the environment in which this artifact was built.
   *
   * <p>Legal targets: EiffelEnvironmentDefinedEvent
   */
  ENVIRONMENT(false),
  /**
   * Identifies the flow context of the event: which is the continuous integration and delivery flow
   * in which this occurred – e.g. which product, project, track or version this is applicable to.
   *
   * <p>Legal targets: EiffelFlowContextDefinedEvent
   */
  FLOW_CONTEXT(true),
  /**
   * Identifies an issue that this event partially resolves. That is, this SCC introduces some
   * change that has advanced an issue towards a resolved state, but not completely resolved.
   *
   * <p>Legal targets: EiffelIssueDefinedEvent
   */
  PARTIALLY_RESOLVED_ISSUE(true),
  /**
   * Identifies a latest previous version (there may be more than one in case of merges) of the
   * created source change.
   *
   * <p>Legal targets: EiffelSourceChangeCreatedEvent
   */
  PREVIOUS_VERSION(true),
  /**
   * Identifies an issue that this SCC is claiming it has done enough to resolve. This is not an
   * authoritative resolution, only a claim. The issue may or may not change status as a consequence
   * this, e.g. through a successful verification or a manual update on the issue tracker.
   *
   * <p>Legal targets: EiffelIssueDefinedEvent
   */
  RESOLVED_ISSUE(true);

  private final boolean multipleAllowed;

  EiffelLinkType(boolean multipleAllowed) {
    this.multipleAllowed = multipleAllowed;
  }

  boolean multipleAllowed() {
    return multipleAllowed;
  }
}
