// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.parsing;

import static com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType.ARTC;
import static com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType.SCC;
import static com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType.SCS;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Sets;
import com.google.common.flogger.FluentLogger;
import com.google.gerrit.entities.Project;
import com.google.gerrit.entities.Project.NameKey;
import com.google.gerrit.extensions.events.GitReferenceUpdatedListener;
import com.google.gerrit.extensions.events.RevisionCreatedListener;
import com.google.gerrit.server.git.ProjectRunnable;
import com.google.gerrit.server.project.TagResource;
import com.google.gerrit.server.util.time.TimeUtil;
import com.google.inject.Inject;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.SourceChangeEventKey;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;

public class EiffelEventParsingQueue {
  private static final FluentLogger logger = FluentLogger.forEnclosingClass();

  private volatile EiffelEventParsingExecutor pool;
  private final EiffelEventParser eventParser;
  private final ParsingQueuePersistence persistence;
  private final ConcurrentHashMap<EventParsingWorker, ScheduledFuture<?>> pending =
      new ConcurrentHashMap<>();
  private final Set<ParsingQueueTask> failedTasks = new HashSet<>();
  private final Object failedTasksLock = new Object();

  @Inject
  public EiffelEventParsingQueue(
      EiffelEventParsingExecutor pool,
      EiffelEventParser eventParser,
      ParsingQueuePersistence persistence) {
    this.pool = pool;
    this.eventParser = eventParser;
    this.persistence = persistence;
  }

  public void scheduleSccCreation(RevisionCreatedListener.Event event) {
    schedule(ParsingQueueTask.sccBuilder(event));
  }

  /**
   * Schedules SCC creation for all commits reachable from branchRef.
   *
   * @param repoName - the repository containing the commits to create events from.
   * @param branchRef - create events for commits reachable from tip of branchRef.
   */
  public void scheduleSccCreation(String repoName, String branchRef) {
    schedule(ParsingQueueTask.builder(SCC, repoName, branchRef));
  }
  /**
   * Schedules SCC creation for all commits with missing SCC-events where the commit is reachable
   * from branchRef.
   *
   * @param repoName - the repository containing the commits to create events from.
   * @param branchRef - create events for commits reachable from tip of branchRef.
   */
  public void scheduleMissingSccCreation(String repoName, String branchRef) {
    schedule(ParsingQueueTask.builder(SCC, repoName, branchRef).fillGaps(true));
  }

  /**
   * Schedule SCC creation for all commits reachable from commit.
   *
   * @param repoName - the repository containing the commits to create events from.
   * @param branchRef - used to populate data.gitIdentifier.branch.
   * @param commit - create events from commits reachable from commit.
   */
  public void scheduleSccCreation(String repoName, String branchRef, String commit) {
    schedule(ParsingQueueTask.builder(SCC, repoName, branchRef).commit(commit));
  }

  /**
   * Schedules SCC creation for all commits with missing SCC-events where the commit is reachable
   * from commit.
   *
   * @param repoName - the repository containing the commits to create events from.
   * @param branchRef - used to populate data.gitIdentifier.branch.
   * @param commit - create events from commits reachable from commit.
   */
  public void scheduleMissingSccCreation(String repoName, String branchRef, String commit) {
    schedule(ParsingQueueTask.builder(SCC, repoName, branchRef).commit(commit).fillGaps(true));
  }

  public void scheduleScsCreation(GitReferenceUpdatedListener.Event event) {
    schedule(
        ParsingQueueTask.builder(SCS, event.getProjectName(), event.getRefName())
            .commit(event.getNewObjectId())
            .updater(event.getUpdater())
            .updateTime(TimeUtil.nowMs())
            .previousTip(event.getOldObjectId()));
  }

  public void scheduleScsCreation(String repoName, String branchRef) {
    schedule(ParsingQueueTask.builder(SCS, repoName, branchRef));
  }

  /**
   * Schedules SCS creation for all commits with missing SCS-events where the commit is reachable
   * from branchRef.
   *
   * @param repoName - the repository containing the commits to create events from.
   * @param branchRef - create events for commits reachable from tip of branchRef.
   */
  public void scheduleMissingScsCreation(String repoName, String branchRef) {
    schedule(ParsingQueueTask.builder(SCS, repoName, branchRef).fillGaps(true));
  }

  public void scheduleArtcCreation(GitReferenceUpdatedListener.Event event) {
    schedule(
        ParsingQueueTask.builder(ARTC, event.getProjectName(), event.getRefName())
            .updateTime(TimeUtil.nowMs())
            .force(false));
  }

  public void scheduleArtcCreation(TagResource resource, boolean force) {
    schedule(
        ParsingQueueTask.builder(ARTC, resource.getName(), resource.getRef())
            .updateTime(resource.getTagInfo().created.getTime())
            .force(force));
  }

  public void shutDown() {
    Set<ParsingQueueTask> tasks = Sets.newHashSet();
    for (Map.Entry<EventParsingWorker, ScheduledFuture<?>> e : pending.entrySet()) {
      e.getValue().cancel(true);
      tasks.add(e.getKey().task);
    }
    persistence.persistQueue(tasks);
    persistence.persistFailed(failedTasks);
    pool.shutDown();
  }

  public void init() {
    for (ParsingQueueTask task : persistence.getFailedTasks()) {
      schedule(task);
    }
    for (ParsingQueueTask task : persistence.getPersistedQueue()) {
      schedule(task);
    }
  }

  /**
   * Reschedules all failed tasks.
   *
   * @return the number of failed tasks that were rescheduled.
   */
  public int rescheduleFailed() {
    int failedCnt = 0;
    synchronized (failedTasksLock) {
      failedCnt = failedTasks.size();
      for (ParsingQueueTask task : failedTasks) {
        schedule(task);
      }
      failedTasks.clear();
    }
    return failedCnt;
  }

  protected void markAsCompleted(EventParsingWorker worker) {
    pending.remove(worker);
    if (!worker.successful) {
      synchronized (failedTasksLock) {
        failedTasks.add(worker.task);
      }
    }
  }

  @VisibleForTesting
  void schedule(ParsingQueueTask.Builder taskBuilder) {
    schedule(taskBuilder.build());
  }

  private void schedule(ParsingQueueTask task) {
    logger.atFine().log("Schedule task: %s", task);
    EventParsingWorker worker = new EventParsingWorker(task);
    ScheduledFuture<?> future = pool.schedule(worker);
    if (future != null) {
      pending.put(worker, future);
    }
  }

  class EventParsingWorker implements ProjectRunnable {
    private final ParsingQueueTask task;
    boolean running;
    boolean successful = true;

    public EventParsingWorker(ParsingQueueTask task) {
      this.task = task;
    }

    public void doRun() {
      String append = null;
      try {
        switch (task.type) {
          case SCC:
            if (task.patchsetCreationData != null) {
              append = ", from event, ";
              eventParser.createAndScheduleSccFromPatchsetCreation(task.patchsetCreationData);
            } else if (task.fillGaps == true) {
              if (task.commitId != null) {
                eventParser.fillGapsForSccFromCommit(
                    task.repoName, task.branchRefOrTag, task.commitId);
              } else {
                eventParser.fillGapsForSccFromBranch(task.repoName, task.branchRefOrTag);
              }
            } else if (task.commitId != null) {
              eventParser.createAndScheduleSccFromCommit(
                  task.repoName, task.branchRefOrTag, task.commitId);
            } else {
              eventParser.createAndScheduleSccFromBranch(task.repoName, task.branchRefOrTag);
            }
            break;
          case SCS:
            if (task.fillGaps == true) {
              eventParser.fillGapsForScsFromBranch(task.repoName, task.branchRefOrTag);
            } else if (task.commitId != null) {
              append = ", from submit, ";
              eventParser.createAndScheduleMissingScss(
                  SourceChangeEventKey.scsKey(task.repoName, task.branchRefOrTag, task.commitId),
                  task.previousTip,
                  task.updater,
                  task.updateTime);
            } else {
              eventParser.createAndScheduleMissingScssFromBranch(
                  task.repoName, task.branchRefOrTag);
            }
            break;
          case ARTC:
            eventParser.createAndScheduleArtc(
                task.repoName, task.branchRefOrTag, task.updateTime, task.force);
            break;
          case CD:
          default:
            logger.atWarning().log("Cannot schedule events for type: %s.", task.type);
        }
        successful = true;
      } catch (EventParsingException e) {
        logger.atSevere().withCause(e).log(
            "Failed to create events%sfrom %s", append != null ? append : " ", task);
        successful = false;
      }
    }

    @Override
    public void run() {
      running = true;
      doRun();
      running = false;
      markAsCompleted(this);
    }

    @Override
    public NameKey getProjectNameKey() {
      return Project.nameKey(task.repoName);
    }

    @Override
    public String getRemoteName() {
      return null;
    }

    @Override
    public boolean hasCustomizedPrint() {
      return true;
    }

    @Override
    public String toString() {
      return String.format("Parsing: %s%s", task, running ? " (running)" : "");
    }
  }
}
