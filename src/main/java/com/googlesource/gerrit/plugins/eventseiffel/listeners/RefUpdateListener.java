// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package com.googlesource.gerrit.plugins.eventseiffel.listeners;

import com.google.gerrit.entities.RefNames;
import com.google.gerrit.extensions.events.GitReferenceUpdatedListener;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.googlesource.gerrit.plugins.eventseiffel.config.EventListenersConfig;
import com.googlesource.gerrit.plugins.eventseiffel.config.EventsFilter;
import com.googlesource.gerrit.plugins.eventseiffel.parsing.EiffelEventParsingQueue;

public class RefUpdateListener implements GitReferenceUpdatedListener {

  private final EiffelEventParsingQueue queue;
  private final Provider<EventListenersConfig> configProvider;
  private final Provider<EventsFilter> eventsFilter;

  @Inject
  public RefUpdateListener(
      EiffelEventParsingQueue queue,
      Provider<EventListenersConfig> configProvider,
      Provider<EventsFilter> eventsFilter) {
    this.queue = queue;
    this.configProvider = configProvider;
    this.eventsFilter = eventsFilter;
  }

  @Override
  public void onGitReferenceUpdated(Event event) {
    EventListenersConfig config = configProvider.get();
    if (!config.isEnabled()) {
      return;
    }

    EventsFilter filter = eventsFilter.get();

    if (filter.refIsBlocked(event.getRefName())
        || filter.projectIsBlocked(event.getProjectName())) {
      return;
    }
    /* The reference was not deleted. */
    if (!event.isDelete()) {
      /* Updated reference is a branch. */
      if (event.getRefName().startsWith(RefNames.REFS_HEADS)) {
        queue.scheduleScsCreation(event);
      }
      /* Updated reference is a tag. */
      else if (event.getRefName().startsWith(RefNames.REFS_TAGS)) {
        queue.scheduleArtcCreation(event);
      }
    }
  }
}
