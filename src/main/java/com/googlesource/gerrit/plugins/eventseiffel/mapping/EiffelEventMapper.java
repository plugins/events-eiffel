// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.mapping;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.flogger.FluentLogger;
import com.google.gerrit.entities.PatchSet;
import com.google.gerrit.entities.PatchSetApproval;
import com.google.gerrit.entities.RefNames;
import com.google.gerrit.extensions.common.AccountInfo;
import com.google.gerrit.extensions.common.GitPerson;
import com.google.gerrit.server.IdentifiedUser;
import com.google.gerrit.server.account.AccountResolver;
import com.google.gerrit.server.account.AccountResolver.UnresolvableAccountException;
import com.google.gerrit.server.account.AccountState;
import com.google.gerrit.server.query.change.ChangeData;
import com.google.gerrit.server.query.change.InternalChangeQuery;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.googlesource.gerrit.plugins.eventseiffel.config.EventMappingConfig;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelArtifactCreatedEventInfo;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelCompositionDefinedEventInfo;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelSourceChangeCreatedEventInfo;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelSourceChangeSubmittedEventInfo;
import com.googlesource.gerrit.plugins.eventseiffel.parsing.PatchsetCreationData;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.eclipse.jgit.errors.ConfigInvalidException;
import org.eclipse.jgit.lib.PersonIdent;
import org.eclipse.jgit.revwalk.RevCommit;

public class EiffelEventMapper {
  private static final FluentLogger logger = FluentLogger.forEnclosingClass();
  private static final long SECONDS_TO_MILLIS = 1000l;
  private final Provider<EiffelEventFactory> eventFactoryProvider;
  private final AccountResolver accountResolver;
  private final Provider<InternalChangeQuery> queryProvider;
  private final EventMappingConfig cfg;

  private static long commitTimeInEpochMillis(RevCommit commit) {
    /* Git records time in seconds since epoch, multiply by 1000. */
    return commit.getCommitTime() * SECONDS_TO_MILLIS;
  }

  @Inject
  public EiffelEventMapper(
      Provider<EiffelEventFactory> eventFactoryProvider,
      AccountResolver accountResolver,
      Provider<InternalChangeQuery> queryProvider,
      EventMappingConfig cfg) {
    this.eventFactoryProvider = eventFactoryProvider;
    this.accountResolver = accountResolver;
    this.queryProvider = queryProvider;
    this.cfg = cfg;
  }

  public EiffelSourceChangeCreatedEventInfo toScc(
      PatchsetCreationData data, List<UUID> parentEventIds)
      throws ConfigInvalidException, IOException {
    PersonInfo author = toPersonInfo(data.author);
    return eventFactoryProvider
        .get()
        .createScc(
            author.name,
            author.email,
            author.username,
            data.project,
            data.branch,
            data.commitId,
            data.changeNumber,
            data.createdAt,
            parentEventIds);
  }

  public EiffelSourceChangeCreatedEventInfo toScc(
      RevCommit commit, String repoName, String branch, List<UUID> parentEventIds)
      throws ConfigInvalidException, IOException {
    return toScc(commit, repoName, branch, parentEventIds, Optional.empty());
  }

  public EiffelSourceChangeCreatedEventInfo toScc(
      RevCommit commit,
      String repoName,
      String branch,
      List<UUID> parentEventIds,
      Optional<UUID> id)
      throws ConfigInvalidException, IOException {
    Optional<ChangeData> change = findChange(commit, repoName, branch);
    Integer changeNbr = null;
    Long epochMillisCreated = null;

    /* Don't trust the ChangeData if there are inconsistencies between the index and what's
     * actually merged. */
    if (change.isPresent()) {
      Optional<PatchSet> patchSet = getPatchSetOf(commit, change.get());
      if (patchSet.isPresent()) {
        changeNbr = change.get().change().getId().get();
        epochMillisCreated = patchSet.get().createdOn().toEpochMilli();
      }
    }

    PersonInfo author = toPersonInfo(commit.getAuthorIdent());
    return eventFactoryProvider
        .get()
        .createScc(
            author.name,
            author.email,
            author.username,
            repoName,
            branch,
            commit.getName(),
            changeNbr,
            epochMillisCreated != null ? epochMillisCreated : commitTimeInEpochMillis(commit),
            parentEventIds,
            id);
  }

  public EiffelSourceChangeSubmittedEventInfo toScs(
      RevCommit commit,
      String repoName,
      String branch,
      AccountInfo submitter,
      Long submittedAt,
      List<UUID> parentEventIds,
      UUID sccId)
      throws ConfigInvalidException, IOException {
    return toScs(
        commit, repoName, branch, submitter, submittedAt, parentEventIds, sccId, Optional.empty());
  }

  public EiffelSourceChangeSubmittedEventInfo toScs(
      RevCommit commit,
      String repoName,
      String branch,
      AccountInfo submitter,
      Long submittedAt,
      List<UUID> parentEventIds,
      UUID sccId,
      Optional<UUID> id)
      throws ConfigInvalidException, IOException {
    SubmitInfo submitInfo;
    if (submitter != null) {
      submitInfo = new SubmitInfo();
      submitInfo.submitter = toPersonInfo(submitter);
      if (submittedAt != null) {
        submitInfo.submittedAt = submittedAt;
      } else {
        logger.atFine().log(
            "\"submittedAt\" not set for (%s, %s, %s), falling back to commit.commitTime",
            repoName, branch, commit.getName());
        submitInfo.submittedAt = commitTimeInEpochMillis(commit);
      }
    } else {
      submitInfo = getSubmitInfo(commit, repoName, branch);
    }
    return eventFactoryProvider
        .get()
        .createScs(
            submitInfo.submitter.name,
            submitInfo.submitter.email,
            submitInfo.submitter.username,
            repoName,
            branch,
            commit.getName(),
            submitInfo.submittedAt,
            parentEventIds,
            sccId,
            id);
  }

  @VisibleForTesting
  public EiffelArtifactCreatedEventInfo toArtc(
      String projectName, String tagName, Long epochMillisCreated, UUID cdId) {
    return eventFactoryProvider.get().createArtc(projectName, tagName, epochMillisCreated, cdId);
  }

  @VisibleForTesting
  public EiffelCompositionDefinedEventInfo toCd(
      String projectName, String version, Long epochMillisCreated, UUID scsId) {
    return eventFactoryProvider.get().createCd(projectName, version, epochMillisCreated, scsId);
  }

  public String tagCompositionName(String projectName) {
    return eventFactoryProvider.get().tagCompositionName(projectName);
  }

  public String tagPURL(String projectName, String tagName) {
    return eventFactoryProvider.get().tagPURL(projectName, tagName);
  }

  private Optional<PatchSet> getPatchSetOf(RevCommit commit, ChangeData change) {
    String commitSha = commit.getName().toLowerCase();
    return change.notes().getPatchSets().values().stream()
        .filter(patchSet -> patchSet.commitId().getName().toLowerCase().equals(commitSha))
        .findAny();
  }

  private PersonInfo toPersonInfo(IdentifiedUser user) {
    PersonInfo pi = new PersonInfo();
    pi.name = user.getName();
    pi.email = user.getAccount().preferredEmail();
    pi.username = user.getUserName().orElse(null);
    return pi;
  }

  /* When applicable returns the person who submitted the change and when.
   * Defaults to commit.committer and commit.commitTime. */
  private SubmitInfo getSubmitInfo(RevCommit commit, String repoName, String branch)
      throws ConfigInvalidException, IOException {

    SubmitInfo si = new SubmitInfo();
    Optional<PatchSetApproval> approval =
        findChange(commit, repoName, branch)
            .filter(cd -> cd.change().isMerged())
            .flatMap(c -> c.getSubmitApproval());

    if (approval.isPresent()) {
      try {
        IdentifiedUser submittingUser =
            accountResolver.resolve(String.valueOf(approval.get().accountId())).asUniqueUser();

        si.submitter = toPersonInfo(submittingUser);
        si.submittedAt = approval.get().granted().toEpochMilli();
        return si;
      } catch (ConfigInvalidException | IOException | UnresolvableAccountException e) {
        logger.atWarning().withCause(e).log(
            "Failed to resolve account from: %d ", approval.get().accountId().get());
      }
    }
    logger.atFine().log(
        "Unable to identify submitter of (%s,%s,%s), falling back to commit.committer.",
        repoName, branch, commit.getName());
    si.submitter = toPersonInfo(commit.getCommitterIdent());
    si.submittedAt = commitTimeInEpochMillis(commit);
    return si;
  }

  private Optional<ChangeData> findChange(RevCommit commit, String repoName, String branch) {
    if (cfg.useIndex()) {
      List<ChangeData> changes =
          queryProvider
              .get()
              .byBranchCommit(
                  repoName,
                  branch.startsWith(RefNames.REFS_HEADS) ? branch : RefNames.REFS_HEADS + branch,
                  commit.getName());
      return changes.size() == 1 ? Optional.of(changes.get(0)) : Optional.empty();
    }
    return Optional.empty();
  }

  private PersonInfo toPersonInfo(GitPerson gitPerson) throws ConfigInvalidException, IOException {
    return toPersonInfo(gitPerson.name, gitPerson.email);
  }

  private PersonInfo toPersonInfo(PersonIdent personIdent)
      throws ConfigInvalidException, IOException {
    return toPersonInfo(personIdent.getName(), personIdent.getEmailAddress());
  }

  private PersonInfo toPersonInfo(AccountInfo accountInfo) {
    PersonInfo pi = new PersonInfo();
    pi.name = accountInfo.name;
    pi.email = accountInfo.email;
    pi.username = accountInfo.username;
    return pi;
  }

  private PersonInfo toPersonInfo(String name, String email)
      throws ConfigInvalidException, IOException {
    PersonInfo pi = new PersonInfo();
    List<AccountState> result = accountResolver.resolve(email).asList();
    /* Committer may not have an account on this Gerrit server. */
    if (result.size() == 1) {
      pi.name = result.get(0).account().fullName();
      pi.email = result.get(0).account().preferredEmail();
      pi.username = result.get(0).userName().orElse(null);
    } else {
      pi.name = name;
      pi.email = email;
      pi.username = null;
    }
    return pi;
  }

  private static class PersonInfo {
    String name;
    String username;
    String email;
  }

  private static class SubmitInfo {
    PersonInfo submitter;
    Long submittedAt;
  }
}
