// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto;

import static com.google.common.base.Preconditions.checkState;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

/**
 * Since meta.id and meta.time are populated at event creation and this plugin doesn't create
 * events, only submit them to rem-rem, both are excluded from the Builder even though they are
 * specified in the declaration as required.
 *
 * <p>As declared by:
 * https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelSourceChangeSubmittedEvent.md#meta-members
 */
public class EiffelMetaInfo {
  public static Builder builder() {
    return new Builder();
  }

  /**
   * The type of event. This field is required by the recipient of the event, as each event type has
   * a specific meaning and a specific set of members in the data and links objects.
   */
  public final EiffelEventType type;
  /**
   * The version of the event type. This field is required by the recipient of the event to
   * interpret the contents. Please see <a
   * href="https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-syntax-and-usage/versioning.md">Versioning</a>
   * for more information.
   */
  public final String version;
  /** Any tags or keywords associated with the events, for searchability purposes. */
  public final String[] tags;
  /** A description of the source of the event. */
  public final EiffelSourceInfo source;
  /**
   * An optional object for enclosing security related information, particularly supporting data
   * integrity.
   */
  public final EiffelSecurityInfo security;
  /** The event creation timestamp in UNIX epoch time milliseconds. */
  public final Long time;
  /** The unique identity of the event, generated at event creation. */
  public final UUID id;

  private EiffelMetaInfo(
      EiffelEventType type,
      String version,
      UUID id,
      Long time,
      String[] tags,
      EiffelSourceInfo source,
      EiffelSecurityInfo security) {
    this.type = type;
    this.version = version;
    this.id = id;
    this.time = time;
    this.tags = tags;
    this.source = source;
    this.security = security;
  }

  public static class Builder {
    private EiffelEventType _type = null;
    private String _version = null;
    private UUID _id = null;
    private Long _time = null;
    private Optional<ArrayList<String>> _tags = Optional.empty();
    private Optional<EiffelSourceInfo.Builder> _source = Optional.empty();
    private Optional<EiffelSecurityInfo.Builder> _security = Optional.empty();

    /* Package visible since type is populated by the event itself. */
    Builder type(EiffelEventType type) {
      this._type = type;
      return this;
    }

    /* Package visible since version is populated by the event itself. */
    Builder version(String version) {
      this._version = version;
      return this;
    }

    public Builder id(UUID id) {
      this._id = id;
      return this;
    }

    public Builder time(Long epochMillis) {
      this._time = epochMillis;
      return this;
    }

    public Builder time(Integer epochMillis) {
      this._time = (long) epochMillis;
      return this;
    }

    public Builder addTag(String tag) {
      _tags = _tags.or(() -> Optional.of(Lists.newArrayList()));
      _tags.get().add(tag);
      return this;
    }

    public Builder security(EiffelSecurityInfo.Builder security) {
      this._security = Optional.of(security);
      return this;
    }

    public Builder source(EiffelSourceInfo.Builder source) {
      this._source = Optional.of(source);
      return this;
    }

    public EiffelMetaInfo build() {
      checkState(_type != null, "type is required");
      checkState(_version != null, "version is required");
      return new EiffelMetaInfo(
          _type,
          _version,
          _id,
          _time,
          _tags.map(t -> t.stream().toArray(l -> new String[l])).orElse(null),
          _source.map(EiffelSourceInfo.Builder::build).orElse(null),
          _security.map(EiffelSecurityInfo.Builder::build).orElse(null));
    }
  }
}
