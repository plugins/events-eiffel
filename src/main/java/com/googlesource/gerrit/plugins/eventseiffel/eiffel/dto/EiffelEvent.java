// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto;

import static com.google.common.base.Preconditions.checkState;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;

public abstract class EiffelEvent {

  public final EiffelMetaInfo meta;
  public final EiffelLinkInfo[] links;

  protected EiffelEvent(EiffelMetaInfo meta, EiffelLinkInfo[] links) {
    this.meta = meta;
    this.links = links;
  }

  public abstract static class Builder<T extends EiffelEvent, B extends Builder<T, B>> {
    private Optional<EiffelMetaInfo.Builder> _meta = Optional.empty();
    private Optional<ArrayList<EiffelLinkInfo>> _links = Optional.empty();
    private Set<EiffelLinkType> existingLinkTypes = Sets.newHashSet();

    @SuppressWarnings("unchecked")
    public B meta(EiffelMetaInfo.Builder meta) {
      _meta = Optional.of(meta);
      return (B) this;
    }

    @SuppressWarnings("unchecked")
    public B addLink(EiffelLinkInfo.Builder linkBuilder) {
      EiffelLinkInfo link = linkBuilder.build();
      checkState(
          supportedLinks().contains(link.type),
          "%s not supported for event-type %s",
          link.type.name(),
          getEventType().getType());
      checkState(
          link.type.multipleAllowed() == true || !existingLinkTypes.contains(link.type),
          "A link of type %s already exists and multiple instances are not allowed",
          link.type.name());
      existingLinkTypes.add(link.type);
      _links = _links.or(() -> Optional.of(Lists.newArrayList()));
      _links.get().add(link);
      return (B) this;
    }

    protected EiffelMetaInfo buildMeta() {
      return _meta
          .orElse(EiffelMetaInfo.builder())
          .version(getEventVersion())
          .type(getEventType())
          .build();
    }

    protected EiffelLinkInfo[] buildLinks() {
      return _links.map(a -> a.stream().toArray(l -> new EiffelLinkInfo[l])).orElse(null);
    }

    /**
     * Builds an Eiffel-event where correct event-version and event-type are set automatically.
     *
     * @return an Eiffel event.
     */
    public abstract T build();

    protected abstract String getEventVersion();

    protected abstract EiffelEventType getEventType();

    protected abstract Set<EiffelLinkType> supportedLinks();
  }
}
