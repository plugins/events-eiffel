// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel;

import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEvent;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType;

/**
 * Identifies an EiffelEvent .
 *
 * @author svense
 */
public abstract class EventKey {

  public static EventKey fromEvent(EiffelEvent event) {
    switch (event.meta.type) {
      case SCC:
      case SCS:
        return SourceChangeEventKey.fromEvent(event);
      case ARTC:
        return ArtifactEventKey.fromEvent(event);
      case CD:
        return CompositionDefinedEventKey.fromEvent(event);
      default:
        throw new IllegalArgumentException("Unsupported event type: " + event.meta.type.toString());
    }
  }

  public static boolean isSupported(EiffelEvent event) {
    switch (event.meta.type) {
      case SCC:
      case SCS:
      case ARTC:
      case CD:
        return true;
      default:
        return false;
    }
  }

  protected final EiffelEventType type;

  protected EventKey(EiffelEventType type) {
    this.type = type;
  }

  public EiffelEventType type() {
    return type;
  }

  public boolean matches(EiffelEvent event) {
    return this.equals(fromEvent(event));
  }

  public boolean supportsForce() {
    switch (type) {
      case ARTC:
      case CD:
        return true;
      case SCC:
      case SCS:
      default:
        return false;
    }
  }

  @Override
  public String toString() {
    return String.format("%s(%s)", type.name(), type.getType());
  }
}
