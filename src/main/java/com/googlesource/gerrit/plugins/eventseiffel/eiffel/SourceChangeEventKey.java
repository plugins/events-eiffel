// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel;

import static com.google.common.base.Preconditions.checkState;

import com.google.gerrit.entities.RefNames;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEvent;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelEventType;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelGitIdentifierInfo;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelSourceChangeCreatedEventInfo;
import com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto.EiffelSourceChangeSubmittedEventInfo;
import java.util.Objects;
import org.eclipse.jgit.revwalk.RevCommit;

/**
 * Identifies a SourceChangeEvent, SourceChangeCreatedEvent or SourceChangeSubmittedEvent.
 *
 * @author svense
 */
public class SourceChangeEventKey extends EventKey {
  public static SourceChangeEventKey create(
      String repoName, String branch, String commitId, EiffelEventType type) {
    return new SourceChangeEventKey(repoName, branch, commitId, type);
  }

  public static SourceChangeEventKey sccKey(String repoName, String branch, String commitId) {
    return new SourceChangeEventKey(repoName, branch, commitId, EiffelEventType.SCC);
  }

  public static SourceChangeEventKey sccKey(String repoName, String branch, RevCommit commit) {
    return new SourceChangeEventKey(repoName, branch, commit.getName(), EiffelEventType.SCC);
  }

  public static SourceChangeEventKey sccKey(EiffelGitIdentifierInfo gitId) {
    checkState(gitId != null, "GitIdentifier must not be null");
    return new SourceChangeEventKey(
        gitId.repoName, gitId.branch, gitId.commitId, EiffelEventType.SCC);
  }

  public static SourceChangeEventKey scsKey(String repoName, String branch, String commitId) {
    return new SourceChangeEventKey(repoName, branch, commitId, EiffelEventType.SCS);
  }

  public static SourceChangeEventKey scsKey(String repoName, String branch, RevCommit commit) {
    return new SourceChangeEventKey(repoName, branch, commit.getName(), EiffelEventType.SCS);
  }

  public static SourceChangeEventKey scsKey(EiffelGitIdentifierInfo gitId) {
    checkState(gitId != null, "GitIdentifier must not be null");
    return new SourceChangeEventKey(
        gitId.repoName, gitId.branch, gitId.commitId, EiffelEventType.SCS);
  }

  public static SourceChangeEventKey fromEvent(EiffelEvent event) {
    switch (event.meta.type) {
      case SCC:
        EiffelSourceChangeCreatedEventInfo.DataInfo sccData =
            ((EiffelSourceChangeCreatedEventInfo) event).data;
        checkState(sccData != null, "data must not be null.");
        return SourceChangeEventKey.sccKey(sccData.gitIdentifier);
      case SCS:
        EiffelSourceChangeSubmittedEventInfo.DataInfo scsData =
            ((EiffelSourceChangeSubmittedEventInfo) event).data;
        checkState(scsData != null, "data must not be null.");
        return SourceChangeEventKey.scsKey(scsData.gitIdentifier);
      default:
      case ARTC:
      case CD:
        throw new IllegalArgumentException(
            "Not a SourceChange* event type: " + event.meta.type.toString());
    }
  }

  private final String repoName;
  private final String branch;
  private final String commitId;

  private SourceChangeEventKey(
      String repoName, String branch, String commitId, EiffelEventType type) {
    super(type);
    checkState(repoName != null, "repoName must not be null");
    checkState(branch != null, "branch must not be null");
    checkState(commitId != null, "commitId must not be null");
    checkState(type != null, "type must not be null");
    this.repoName = repoName;
    this.branch =
        branch.startsWith(RefNames.REFS_HEADS)
            ? branch.substring(RefNames.REFS_HEADS.length())
            : branch;
    this.commitId = commitId;
  }

  public String repo() {
    return repoName;
  }

  public String branch() {
    return branch;
  }

  public String commit() {
    return commitId;
  }

  /**
   * Creates a new EventKey with the same repo, branch, type but with commitId set to commitId.
   *
   * @param commitId
   * @return EventKey
   */
  public SourceChangeEventKey copy(String commitId) {
    return new SourceChangeEventKey(this.repoName, this.branch, commitId, this.type);
  }

  /**
   * Creates a new EventKey with the same repo, branch, type but with commitId set to
   * commit.getName().
   *
   * @param commit
   * @return EventKey
   */
  public SourceChangeEventKey copy(RevCommit commit) {
    return new SourceChangeEventKey(this.repoName, this.branch, commit.getName(), this.type);
  }

  /**
   * Creates a new EventKey with the same repo, branch, commit but with type set to type.
   *
   * @param type new EiffelEventType
   * @return EventKey
   */
  public SourceChangeEventKey copy(EiffelEventType type) {
    return new SourceChangeEventKey(this.repoName, this.branch, this.commitId, type);
  }

  @Override
  public boolean equals(Object other) {
    if (other == null || !(other instanceof SourceChangeEventKey)) {
      return false;
    }
    SourceChangeEventKey o = (SourceChangeEventKey) other;
    return o.type.equals(this.type)
        && o.repoName.equals(this.repoName)
        && o.branch.equals(this.branch)
        && o.commitId.equals(this.commitId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(repoName, branch, commitId, type);
  }

  @Override
  public String toString() {
    return String.format("%s:[%s, %s, %s]", type.name(), repoName, branch, commitId);
  }
}
