// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto;

/**
 * An optional object for enabling information integrity protection via cryptographic signing.
 *
 * <p>To generate a correct meta.security.integrityProtection object:
 *
 * <ol>
 *   <li>Generate the entire event, but with the meta.security.integrityProtection.signature value
 *       set to an empty string.
 *   <li>Serialize the event on Canonical JSON Form.
 *   <li>Generate the signature using the meta.security.integrityProtection.alg algorithm.
 *   <li>Set the meta.security.integrityProtection.signature value to the resulting signature while
 *       maintaining Canonical JSON Form. To verify the integrity of the event, the consumer then
 *       resets meta.security.integrityProtection.signature to an empty string and ensures Canonical
 *       JSON Form before verifying the signature.
 * </ol>
 *
 * As declared by:
 * https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelSourceChangeCreatedEvent.md#metasecurityintegrityprotection
 */
public class EiffelIntegrityProtectionInfo {

  public static Builder builder() {
    return new Builder();
  }

  /**
   * A valid <a href="https://tools.ietf.org/html/rfc7518#section-3.1">JWA RFC 7518 alg
   * parameter</a> value, excluding "none"
   */
  public final String alg;
  /** The signature produced by the signing algorithm. */
  public String signature;
  /**
   * The producer of the event may include the relevant public key for convenience, rather than
   * relying on a separate key distribution mechanism. Note that this property, along with the rest
   * of the event, is encompassed by the integrity protection offered via
   * meta.security.integrityProtection.
   */
  public final String publicKey;

  private EiffelIntegrityProtectionInfo(String alg, String publicKey) {
    this.alg = alg;
    this.publicKey = publicKey;
    this.signature = "";
  }

  public static class Builder {
    private String _alg = null;
    private String _publicKey = null;

    private Builder() {}

    /* TODO: verify value. */
    public Builder alg(String alg) {
      this._alg = alg;
      return this;
    }

    public Builder pulicKey(String publicKey) {
      this._publicKey = publicKey;
      return this;
    }

    public EiffelIntegrityProtectionInfo build() {
      return new EiffelIntegrityProtectionInfo(_alg, _publicKey);
    }
  }
}
