// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package com.googlesource.gerrit.plugins.eventseiffel.listeners;

import com.google.gerrit.entities.RefNames;
import com.google.gerrit.extensions.events.RevisionCreatedListener;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.googlesource.gerrit.plugins.eventseiffel.config.EventListenersConfig;
import com.googlesource.gerrit.plugins.eventseiffel.config.EventsFilter;
import com.googlesource.gerrit.plugins.eventseiffel.parsing.EiffelEventParsingQueue;

public class PatchsetCreatedListener implements RevisionCreatedListener {
  private final EiffelEventParsingQueue queue;
  private final Provider<EventListenersConfig> configProvider;
  private final Provider<EventsFilter> eventsFilter;

  @Inject
  public PatchsetCreatedListener(
      EiffelEventParsingQueue queue,
      Provider<EventListenersConfig> configProvider,
      Provider<EventsFilter> eventsFilter) {
    this.queue = queue;
    this.configProvider = configProvider;
    this.eventsFilter = eventsFilter;
  }

  @Override
  public void onRevisionCreated(RevisionCreatedListener.Event event) {
    if (!configProvider.get().isEnabled()) {
      return;
    }
    /* 'branch' is not the exact reference in this case.
     * Filtering for startsWith("refs/") to filter out refs/meta/config.*/
    if (!event.getChange().branch.startsWith(RefNames.REFS)) {
      EventsFilter filter = eventsFilter.get();
      if (filter.refIsBlocked(RefNames.REFS_HEADS + event.getChange().branch)
          || filter.projectIsBlocked(event.getChange().project)) {
        return;
      }
      queue.scheduleSccCreation(event);
    }
  }
}
