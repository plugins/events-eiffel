// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.eiffel.dto;

/**
 * The author of the change.
 *
 * <p>As declared by:
 * https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelSourceChangeCreatedEvent.md#dataauthor
 * https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelSourceChangeSubmittedEvent.md#datasubmitter
 */
public class EiffelPersonInfo {

  public static Builder builder() {
    return new Builder();
  }

  /** The name of the person. */
  public final String name;
  /** The email address of the person. */
  public final String email;
  /** Any identity, username or alias of the person. */
  public final String id;
  /** Any group or organization to which the person belongs. */
  public final String group;

  private EiffelPersonInfo(String name, String email, String id, String group) {
    this.name = name;
    this.email = email;
    this.id = id;
    this.group = group;
  }

  public static class Builder {
    private String _name = null;
    private String _email = null;
    private String _id = null;
    private String _group = null;

    private Builder() {}

    public Builder name(String name) {
      this._name = name;
      return this;
    }

    public Builder email(String email) {
      this._email = email;
      return this;
    }

    public Builder id(String id) {
      this._id = id;
      return this;
    }

    public Builder group(String group) {
      this._group = group;
      return this;
    }

    public EiffelPersonInfo build() {
      return new EiffelPersonInfo(_name, _email, _id, _group);
    }
  }
}
