// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.googlesource.gerrit.plugins.eventseiffel.cache;

import com.google.gerrit.server.cache.serialize.CacheSerializer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

public class UUIDSerializer implements CacheSerializer<Optional<UUID>> {
  public static final byte[] EMPTY = "empty".getBytes(StandardCharsets.US_ASCII);

  @Override
  public byte[] serialize(Optional<UUID> object) {
    if (object.isEmpty()) {
      return EMPTY;
    }
    byte[] result = new byte[Long.BYTES * 2];
    int i = result.length - 1;
    long ms = object.get().getMostSignificantBits();
    for (; i >= Long.BYTES; i--) {
      result[i] = (byte) (ms & 0xFF);
      ms >>= 8;
    }
    long ls = object.get().getLeastSignificantBits();
    for (; i >= 0; i--) {
      result[i] = (byte) (ls & 0xFF);
      ls >>= 8;
    }
    return result;
  }

  @Override
  public Optional<UUID> deserialize(byte[] in) {
    if (Arrays.equals(in, EMPTY)) {
      return Optional.empty();
    }
    long ls = 0;
    int i = 0;
    for (; i < Long.BYTES; i++) {
      ls <<= 8;
      ls |= (in[i] & 0xFF);
    }
    long ms = 0;
    for (; i < Long.BYTES * 2; i++) {
      ms <<= 8;
      ms |= (in[i] & 0xFF);
    }
    return Optional.of(new UUID(ms, ls));
  }
}
