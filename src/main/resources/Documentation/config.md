# Configuration file: ./etc/@PLUGIN@.config #

Config location: `$SITE/etc/@PLUGIN@.config`.

## Example config ##

```
    [RabbitMq]
      url = amqp://rabbit.company.com:5672
      exchange = eiffel.events
      username = user
      password = secret
      waitForConfirms = 7 seconds
      maxBatchSize = 100
      certificate = /path/to/certificate.crt
    [EiffelRepoClient]
      graphQlUrl = https://eiffel.company.com/graphql
      goRestUrl = https://eiffel.company.com/rest/
      username = user
      password = secret
    [EventParsing]
      poolSize = 4
    [EiffelEvent]
      namespace = github
    [EventIdCache]
      trustLocalCache = False
      maxNbrOfEntries = 10000
    [EventMapping]
      useIndex = False
```

## Section "RabbitMq" ##

The configuration for the Rabbit MQ connection.

### Settings ###

url
: The url to the RabbitMq instance.
  (_Required_)

virtualHost
: The virtual host to use when connecting to the broker
  (Default: _"/"_).

username
: The AMQP user name to use when connecting to the broker.

password
: The password to use when connecting to the broker.

exchange
: The exchange to publish the message to.
  (_Required_)

persistentDelivery
: Whether or not to use persistent delivery mode.
  (Default: _true_).

appId
: The Rabbit MQ message property App ID.
  (Default: _"Gerrit"_).

routingKeyTag
: Routing key for eiffel event [should be formatted as](https://github.com/eiffel-community/eiffel-sepia/pull/9):
  `eiffel.$EVENT_TYPE.$TAG.$DOMAIN_ID`
  This config option decides what $TAG should be set to.
  (Default: _"notag"_.)

waitForConfirms
: How long the publisher should wait for confirms from RabbitMq.
  If set to a 0 or less confirms are disabled.
  (Default: _5000 ms_.)

maxBatchSize
: The maximum number of events that are published and confirmed together.
  If confirms are disabled this option has no real effect.
  (Default: _1_.)

certificate
: The certificate used for peer verification of server. If no certificate is set
we will trust anyone.

## Section "EiffelRepoClient" ##

Configuration for connecting to Eiffel repository APIs:
* [Eiffel GraphQL API](https://github.com/eiffel-community/eiffel-graphql-api).
* [Eiffel GoER](https://github.com/eiffel-community/eiffel-goer)

### Settings ###

graphQlUrl
: The url to the GraphQL endpoint.
  (_Required_)

goRestUrl
: The url to the GoER REST endpoint.
  (_Required_)

username
: The user name for the GraphQL service.

password
: The password for the GraphQL service.

connectTimeout
: Time to wait, in seconds, for a connection to be established.
  (Default: _20_).

## Section "EventParsing" ##

Configuration for tuning the event parsing machinery.

### Settings ###

poolSize
: Number of parallel workers parsing events.
  (Default: _1_).

## Section "EiffelEvent" ##

Configuration for Eiffel-specific configurations options

### Settings ###

namespace
: Specifies which namespace the repository is available in. This could be the name
  of a server or a symbolic name that identifies the repository manager. Typical
  values are "gerrit", "gitlab" or "github".

## Section "EventIdCache" ##

Configuration for tuning the EventIdCache.

### Settings ###

trustLocalCache
: Trusts the state of the local cache and do not query Event Repository on cache-miss.
  This is useful when doing large back-fills of events or when you don't have an Event
  Repository set up.
  (Default: _False_).

maxNbrOfEntries
: Number of entries that are allowed in the cache before entries starts to get evicted.
  (Default: _10000000_)

## Section "EventMapping" ##

Configuration for tuning the mapping of eiffel-events from historic commits and Gerrit
events.

### Settings ###

useIndex
: Whether or not the index should be used when mapping.
  This will drastically increase performance when backfilling eiffel-events from history
  but will omit data from the eiffel-events such as change-id (when applicable) and
  submitter will be set as committer instead of actual submitter.
  (Default: _True_).
