POST createArtcs
==============================

```
POST /projects/{project-name}/tags/{tag-name}/@PLUGIN@~createArtcs
```

DESCRIPTION
-----------
Creates missing
[ArtifactCreatedEvent(ARTC)](https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelArtifactCreatedEvent.md)
,
[CompositionDefinedEvent(CD)](https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelCompositionDefinedEvent.md)
,
[SourceChangeSubmitted(SCS)](https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelSourceChangeSubmittedEvent.md)
and
[SourceChangeCreated(SCC)](https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelSourceChangeCreatedEvent.md)
events for commit pointed by {tag-name}.

ACCESS
------
[Administrate Server](../../../Documentation/access-control.html#capability_administrateServer)
capability is required.

EXAMPLES
--------

#### Create missing ARTC events for the tag stable-3.5 of project my/project: ####

```
  curl -X POST --user janeadmin:secret \
    -H "content-type:application/json" \
    http://host:port/a/projects/my%2Fproject/tags/stable-3.5/@PLUGIN@~createArtcs
```

__Response:__

```
  )]}'
  {
   "types": [
     "EiffelArtifactCreatedEvent",
     "EiffelCompositionDefinedEvent",
     "EiffelSourceChangeCreatedEvent",
     "EiffelSourceChangeSubmittedEvent",
   ],
   "repo_name": "my/project",
   "ref": "refs/tags/stable-3.5",
   "status": "Event creation scheduled",
  }
```

### Response object ###
types
: The types of events that may be created.

repo_name
: Name of the repository.

ref
: The ref of the tag from which we will create an ARTC event.

SEE ALSO
--------
* [Plugin Development](../../../Documentation/dev-plugins.html)
* [REST API Protocol Details](../../../Documentation/rest-api.html#_protocol_details)

GERRIT
------
Part of [Gerrit Code Review](../../../Documentation/index.html)
