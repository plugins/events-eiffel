# All-Projects configuration: @PLUGIN@.config #

Config location: `@PLUGIN@.config` on refs/meta/config branch of All-Projects.

## Example config ##

```
    [EventListeners]
      enabled = True
    [EventsFilter]
      blockedRef = refs/heads/temp/*
      blockedRef = ^refs/heads/user/.*
      blockedProject = user/projects/*
      blockedProject = top/secret/project
      blockLightWeightTags = True.
```

## Section "EventListeners" ##

Configuration for if Gerrit events should trigger Eiffel event creation.

### Settings ###

enabled
: Whether or not Gerrit events should trigger Eiffel event creation.
  (Default: _True_).


## Section "EventsFilter" ##

  Configuration for which Gerrit events should trigger Eiffel event creation.

### Settings ###

blockedRef
: Pattern(s) that blocks event creation for all matching refs.
  Supported: REGEX - if starts with "^", WILDCARD - if ends with "*", EXACT - in other cases.

blockedProject
: Pattern(s) that blocks event creation for all matching projects.
  Supported: REGEX - if starts with "^", WILDCARD - if ends with "*", EXACT - in other cases.

blockLightWeightTags
: Decides whether ArtC and CD events should be created for lightweight tags.
  (Default: _False_).
