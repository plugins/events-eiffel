# Setting up events-eiffel

## Requirements

### Publishing
Infrastructure to which the events can be posted.

Supported:
* [RabbitMQ](https://www.rabbitmq.com/)

### Event consistency
To ensure consistency with previously published events the plugin needs access
to the following eiffel event repository APIs:
* [Eiffel GraphQL API](https://github.com/eiffel-community/eiffel-graphql-api).
* [Eiffel GoER](https://github.com/eiffel-community/eiffel-goer)

This can be bypassed by setting `EventIdCache.trustLocalCache = True`.
Setting this means the local Gerrit cache of published events is taken as the
truth and the Eiffel Repostory APIs aren't necessary. The side-effect is that
inconsistencies may follow if events are sent by others than the events-eiffel
plugin.

## Tuning

### RabbitMQ

#### Confirms

By default the RabbitMQ publisher waits for confirms for 5 seconds. If you have
high latency on your network you might want to increase this value in the config
`RabbitMq.waitForConfirms`.
If performance is more important to you than the risk of losing events you can
disable confirms by setting it to `0`.

#### Batch

The overhead of sending one message when `waitForConfirm` is enabled is quite
large so you might want to increase the batch size from the default `1` by
setting `RabbitMq.maxBatchSize` to `10` and increase more if necessary.
This means that the publisher will publish and confirm messages in batches
of 10.

The batch size is maximum 10 in this case, if the queue only contains 2 events
it will not wait until 8 more events are added to the queue but instead send
those 2 events in one batch.
