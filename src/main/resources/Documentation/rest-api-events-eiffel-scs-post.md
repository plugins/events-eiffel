POST createScss
==============================

```
POST /projects/{project-name}/branches/{branch-name}/@PLUGIN@~createScss
```

DESCRIPTION
-----------
Creates missing
[SourceChangeSubmitted(SCS)](https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelSourceChangeSubmittedEvent.md)
and
[SourceChangeCreated(SCC)](https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelSourceChangeCreatedEvent.md)
events for all commits reachable from {branch-name}.

ACCESS
------
[Administrate Server](../../../Documentation/access-control.html#capability_administrateServer)
capability is required.

EXAMPLES
--------

#### Create missing SCS events for master branch of project my/project: ####

```
  curl -X POST --user janeadmin:secret \
    -H "content-type:application/json" \
    http://host:port/a/projects/my%2Fproject/branches/master/@PLUGIN@~createScss
```

__Response:__

```
  )]}'
  {
   "types": [
     "EiffelSourceChangeCreatedEvent",
     "EiffelSourceChangeSubmittedEvent"
   ],
   "repo_name": "my/project",
   "ref": "refs/heads/master",
   "status": "Event creation scheduled",
   "branch": "refs/heads/master"
  }
```

#### Re-create missing SCS events for commits reachable from master of project my/project: ####
```
  curl -X POST --user janeadmin:secret \
    -H "content-type:application/json" \
    http://host:port/a/projects/my%2Fproject/branches/master/@PLUGIN@~createSccs \
    -d '{"fill_gaps":true}'
```

__Response:__

```
  )]}'
  {
   "types": [
     "EiffelSourceChangeCreatedEvent",
     "EiffelSourceChangeSubmittedEvent"
   ],
   "repo_name": "my/project",
   "ref": "refs/heads/master",
   "status": "Event creation scheduled",
   "branch": "refs/heads/master"
  }
```

### Request object ###
fill_gaps
: Decide if you should do a backfill or re-create missing events. If this is true you re-create
missing events.

### Response object ###
types
: The types of events that will be created.

repo_name
: Name of the repository.

ref
: The ref from which reachable commits will result in SCC events.

branch:
: The branch for which these events will be created (`data.gitIdentifier.branch`).

SEE ALSO
--------
* [Plugin Development](../../../Documentation/dev-plugins.html)
* [REST API Protocol Details](../../../Documentation/rest-api.html#_protocol_details)

GERRIT
------
Part of [Gerrit Code Review](../../../Documentation/index.html)
