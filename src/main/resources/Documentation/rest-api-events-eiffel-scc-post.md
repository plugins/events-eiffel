POST createSccs
==============================

```
POST /projects/{project-name}/branches/{branch-name}/@PLUGIN@~createSccs
```

DESCRIPTION
-----------
Creates missing
[SourceChangeCreated(SCC)](https://github.com/eiffel-community/eiffel/blob/edition-paris/eiffel-vocabulary/EiffelSourceChangeCreatedEvent.md)
events for all commits reachable from {branch-name}.

ACCESS
------
[Administrate Server](../../../Documentation/access-control.html#capability_administrateServer)
capability is required.

EXAMPLES
--------

#### Create missing SCC events for master branch of project my/project: ####

```
  curl -X POST --user janeadmin:secret \
    -H "content-type:application/json" \
    http://host:port/a/projects/my%2Fproject/branches/master/@PLUGIN@~createSccs
```

__Response:__

```
  )]}'
  {
   "types": [
     "EiffelSourceChangeCreatedEvent"
   ],
   "repo_name": "my/project",
   "ref": "refs/heads/master",
   "status": "Event creation scheduled",
   "branch": "refs/heads/master"
  }
```

#### Create missing SCC events for patch-set 3:1 of project my/project: ####

```
  curl -X POST --user janeadmin:secret \
    -H "content-type:application/json" \
    http://host:port/a/projects/my%2Fproject/branches/refs%2Fchanges%2F03%2F3%2F1/@PLUGIN@~createSccs
```

__Response:__

```
  )]}'
  {
    "types": [
      "EiffelSourceChangeCreatedEvent"
    ],
    "repo_name": "my/project",
    "ref": "refs/changes/03/3/1",
    "status": "Event creation scheduled",
    "branch": "refs/heads/master"
  }
```

#### Re-create missing SCC events for commits reachable from master of project my/project: ####
```
  curl -X POST --user janeadmin:secret \
    -H "content-type:application/json" \
    http://host:port/a/projects/my%2Fproject/branches/master/@PLUGIN@~createSccs \
    -d '{"fill_gaps":true}'
```

__Response:__

```
  )]}'
  {
   "types": [
     "EiffelSourceChangeCreatedEvent"
   ],
   "repo_name": "my/project",
   "ref": "refs/heads/master",
   "status": "Event creation scheduled",
   "branch": "refs/heads/master"
  }
```

### Request object ###
fill_gaps
: Decide if you should do a backfill or re-create missing events. If this is true you re-create
missing events.

### Response object ###
types
: The types of events that will be created.

repo_name
: Name of the repository.

ref
: The ref from which reachable commits will result in SCC events.

branch:
: The branch for which these events will be created (`data.gitIdentifier.branch`).

SEE ALSO
--------
* [Plugin Development](../../../Documentation/dev-plugins.html)
* [REST API Protocol Details](../../../Documentation/rest-api.html#_protocol_details)

GERRIT
------
Part of [Gerrit Code Review](../../../Documentation/index.html)
