Creates [Eiffel](https://github.com/eiffel-community/eiffel)
Source Code Events based on gerrit-change, branch and tag updates.

Currently supports Eiffel versions:
* [edition-paris](https://github.com/eiffel-community/eiffel/releases/tag/edition-paris).
* [edition-lyon](https://github.com/eiffel-community/eiffel/releases/tag/edition-lyon).
* [edition-arica](https://github.com/eiffel-community/eiffel/releases/tag/edition-arica)
* [edition-orizaba](https://github.com/eiffel-community/eiffel/releases/tag/edition-orizaba)
