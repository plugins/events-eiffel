# Eiffel events basics

Eiffel events describes a DAG. The link is the meta.id of the downstream event.
This means, like with git, that you cannot create a node (event) before all
previous, parent, events are created.

## Eiffel events produced by the plugin

|Event|Gerrit Counterpart|Description|
|-----|------------------|-----------|
|[SourceChangeCreated (SCC)](https://github.com/eiffel-community/eiffel/blob/master/eiffel-vocabulary/EiffelSourceChangeCreatedEvent.md)|Patchset Created|Declares that a change request to sources has been created.|
|[SourceChangeSubmitted (SCS)](https://github.com/eiffel-community/eiffel/blob/master/eiffel-vocabulary/EiffelSourceChangeSubmittedEvent.md)|Ref-Update (branch)|Declares that a commit has been integrated into a source branch|
|[ArtifactCreated (ArtC)](https://github.com/eiffel-community/eiffel/blob/master/eiffel-vocabulary/EiffelArtifactCreatedEvent.md)|Ref-Update (tag)|Used together with CD to represent at tag (see example below)|
|[CompositionDefined (CD)](https://github.com/eiffel-community/eiffel/blob/master/eiffel-vocabulary/EiffelCompositionDefinedEvent.md)|Ref-Update (tag)|Used together with ArtC to represent at tag (see example below)|

### Tag representation

Eiffel currently [edition Arica](https://github.com/eiffel-community/eiffel/tree/edition-arica) doesn't have an event to represent a tag. Instead a combination of
ArtC and CD events are used to represent the tag and reference the SCS.

![Tag events](./images/tag-created-events.png)

## EiffelSourceChange event quirks

Unlike git commits EiffelSourceChange events (SCC, SCS) contains a reference to
the branch.

This has the unfortunate effect that creating a branch in git is simply to create
a reference when in Eiffel you'll need to create 2 * NumberOfCommits events to
represent the branch creation.

Another unfortunate side-effect is that even though git branches and git tags
are orthogonal and live in separate spaces you cannot (currently) represent the
creation of a git tag without referencing a SCS that in turn references a branch.

Since the current source-code events are modeled around Gerrit workflow it's a
requirement that each event that models an integration (SCS) must have a
corresponding SCC event. This has the unfortunate effect that in order to model
a direct push (integration from upstream or a newly imported project) you will
need to fake a change upload (SCC) for each commit even though no such change
was created.

There are ongoing efforts in the Eiffel community to remedy these shortcomings.

## Consistency vs performance

For complete consistency we wouldn't be able to create a child event until the
parent event is created and published to the Eiffel infrastructure.
For performance reasons this isn't plausible, we need to keep the newest part
of the DAG in memory in the plugin waiting to be published.

Therefore an Eiffel event can have three states within the scope of the plugin:
* Non existent.
* Created (living in memory waiting to be published).
* Published.

The plugin takes all three states into consideration when determining which
events needs to be created.

## Identifying missing events

In these examples creation is triggered for commit C5 which has a commit
history like:

![Commit graph](./images/commit-graph.png)

### SCC

SCCs should mirror the commit graph where the parent(s) links are replaced with
PREVIOUS_VERSION link(s).

![SCC events](./images/scc-events.png)

To find commits that aren't yet represented by a SCC event we walk the commit
graph until we find a commit where the corresponding events are already created
for all parent commits (C4 in this case).

From the image we can see that SCC1 is published to Eiffel whereas SCC2 and
SCC3 are created but only live in memory in the event-hub in the plugin.

### SCS

SCSs are slightly more complicated since it, apart from links to parent events,
has a link to the SCC of the change that was submitted.

We create SCCs for the commit first (see above).
After which we identify the commits that aren't yet represented by a SCS and
create SCS events for them too.

![SCS events](./images/scs-events.png)
