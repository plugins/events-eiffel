load("//tools/bzl:maven_jar.bzl", "maven_jar")

def external_plugin_deps():
    maven_jar(
        name = "amqp_client",
        artifact = "com.rabbitmq:amqp-client:5.11.0",
        sha1 = "302bd371a0b8a7341715a374d35ed620fb3b540f",
    )
